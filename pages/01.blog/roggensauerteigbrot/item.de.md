---
title: Sauerteig Roggenbrot
published: true
date: '17:34 06/23/2022'
metadata:
    og:title: 'Rezept: Sauerteig Roggenbrot'
    'og:image': https://schmankerl.space/de/blog/roggensauerteigbrot/000-titelbild.jpg
    'og:image:alt': 'Sauerteig Roggenbrot'
    keywords: 'Rezept, Sauerteig, Roggenbrot, Dinkel, Mehl, Hefe, Weizenmehl'
    description: 'Sauerteig Roggenbrot'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Backen
    tag:
        - Brot
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '8 Personen'
    -
        option: 'Vorbereitung:'
        value: '1-5 Tage'
    -
        option: 'Zubereitung:'
        value: '4-7 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Schüssel, Teigspatel, Backpapier, Küchenmaschine'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Sauerteig:'
        list:
            - '150g Weizenmehl'
            - '20-30g Mutterteig'
            - '150 ml lauwarmes Wasser'
    -
        title: 'Roggenmischbrot:'
        list:
            - '300g Roggenmehl 1150'
            - '200g Vollkorn Roggenmehl'
            - '200g Weizenmehl 450'
            - '100g Dinkelmehl 550'
            - '300g Sauerteig'
            - '500ml lauwarmes Wasser'
            - '2 1/2 El Salz'
            - '150g Kürbiskerne'
            - '150g Sonnenblumenkerne'
            - '4 Tl Brotgewürz'
            - '1/2 Würfel Hefe'
            - '1 Hand voll Hanfsamen'
            - '1 Hand voll Amaranth'
---

>Langwierig aber nicht schlimm:
<cite>Ein gutes vollmundiges Brot dauert eine ganze Weile, aber mit ein wenig Timing geht es schnell von der Hand.</cite>

## Anleitung

### Sauerteig

1. Geht zu eurem Bäcker des Vertrauens oder zur Not in die Feinkostabteilung eures Premium Supermarkts und holt euch entweder einen Mutterteig oder schon den fertigen Sauerteig aus der Packung.
2. Wenn ihr einen Mutterteig habt, dann müsst ihr ihn anfüttern. Gebt 150 ml Wasser und 150g Weizenmehl in eine Schüssel und verrüht das Ganze mit einem Löffel, bis keine Klumpen mehr vorhanden sind.
3. Gebt die Masse zum Mutterteig, verrührt ihn kräftig und füllt den angefütterten Teig in ein Schraubglas oder noch besser in ein Einweckglas und verschließt den Deckel gut.
4. Macht ihr am gleichen Tag noch das Brot, dann lasst ihn ca. 1,5 Stunden bei Raumtemperatur und aufgelegten Deckel (es muss wenig Luft rankönnen) gären.

>Tipp:
<cite>Ich füttere den Mutterteig am Vortag des Brotbackens und verschließe das Glas nachdem anfüttern und stelle es direkt in den Kühlschrank.</cite>

### Brotteig

1. Erwärmt 500ml Wasser lauwarm und gießt sie in eine große Rührschüssel für die Küchenmaschine.
2. Fügt die 300g Sauerteig (selbst gezogen ansonsten auf den Packungshinweis acht geben), den 1/2 Würfel frische Hefe und die 2 1/2 El Salz hinzu und verrührt sie mit einem Löffel kräftig.
3. Gebt alle Mehlsorten, Gewürze, Samen und Kerne hinzu.
4. Legt den Knethaken in die Küchenmaschine ein und lasst den Teig mindestens 20 Minuten, eher 30 Minuten lang kneten. Das Roggenmehl ist ziemlich fies, ihr werden den Teig immer wieder mal vom Rand mit einem Löffel abstoßen müssen, damit der Knethaken den Teig wieder aufnimmt. Bei einem reinen Weizenmehlteig passiert es nicht, dafür schmeckt das Roggenbrot um einiges vollmundiger und ist auch gesünder.
5. Bemehlt eine große Schüssel ordentlich und kratzt den Teig aus der Rührschüssel und drückt ihn in der bemehlten Schüssel in Form. Bemehlt die Oberfläche des Teigs und verteilt ihn gleichmäßig und drückt ihn leicht an, damit er in Form kommt.
6. Deckt die Schüssel zu und lasst sie bei Raumtemperatur mindestens 2 Stunden, eher 4-6 Stunden gehen.
7. Je nach Sauerteig und dessen Hefeanteil geht der Teig schneller oder langsamer. Der Teig sollte ca. 50% an Volumen gewinnen. Lässt man das Brot zu wenig gehen, wird es später matschig und bekommt keine schöne innere Struktur.
8. Heizt den Ofen mindestens 30 Minuten mit einer kleinen Schüssel Wasser auf 260-280 Grad Ober- und Unterhitze vor. Legt auf das Backblech Backpapier und bemehlt es gut. Stürzt nun das Brot aus der Schüssel auf das Backblech.
9. Schiebt das Brot auf die mittlere Schiene und lasst es ca. 15-18 Minuten anbacken.
10. Gießt ein wenig Wasser, ca. 30ml aus der Schüssel auf den Ofenboden, passt dabei auf den starken Dampf auf, der euch entgegenkommen wird. Nehmt die Schüssel mit dem Wasser aus dem Ofen.
11. Verringert die Hitze auf 210 Grad und backt das Brot ca. 40-55 Minuten je nach gewünschter Krustenstärke fertig.
12. Ist die Kruste so wie ihr sie wollt, schaltet den Ofen aus und bespritzt das Brot vorsichtig mit einer kleinen Wasserspritze (Blumen). Die Kruste trocknet damit nicht aus und bleibt länger frisch. Vorsicht! Nicht zu viel, sonst wird sie matschig.

## Rezept Fotos

![Rezept Foto Sauerteig Roggenbrot mit Kürbiskernen und Sonnenblumenkernen](roggensauerteigbrot-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Sauerteig Roggenbrot" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
