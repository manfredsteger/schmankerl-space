---
title: Hummus
published: true
date: '08:20 12/24/2020'
metadata:
    og:title: 'Rezept: Hummus'
    'og:image': https://schmankerl.space/de/blog/hummus/000-titelbild.jpg
    'og:image:alt': 'Hummus in einem Einweckglas auf dem Essenstisch mit vielen leckeren Falaffeln auf einem Teller'
    keywords: 'Rezept, vegetarisch, Hummus, Falaffel, orientalisch, Kichererbsen'
    description: 'Hummus: Der Alleskönner, als Dip, Brotaufstrich oder zur Falaffel. Hummus passt immer und überall. Das Rezept gibt es natürlich hier auf dem schmankerl.space'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Vegetarisch
    tag:
        - Beilage
        - Dip
        - Vegetarisch
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '6 Personen'
    -
        option: 'Vorbereitung:'
        value: '1 Tage'
    -
        option: 'Zubereitung:'
        value: '1 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Blender, Rührmaschine oder Mixer, Knethaken'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Hummus:'
        list:
            - '500g Kichererbsen aus der Dose'
            - '275g Tahin oder Sesammus'
            - '3 Zehen Knoblauch'
            - '1 große Zitrone'
            - '2 El Kreuzkümmel im Ganzen'
            - '1 Tl Cayenne Pfeffer gemahlen'
            - '1 Tl Harissa'
            - '2 Tl Salz'
            - 'Wasser'
---

>Gesund, lecker und macht pappsatt
<cite>Wir nehmen Hummus vor allem für den Falaffelwrap, aber als Brotaufstrich, gebratenes Gemüsetopping oder einfach zum Naschen – Hummus geht immer.</cite>

## Anleitung

1. Für das [Falaffel Rezept](https://schmankerl.space/de/blog/falaffel) nehmen wir getrocknete Kichererbsen, für den Hummus mit Absicht die eingelegten Kichererbsen aus der Dose. Die Konsistenz wird einfach besser damit. Lasst die Kichererbsen abtropfen.
2. Knoblauch schälen und in Scheiben schneiden.
3. Die Zitronen nur schälen und vierteln.
4. Die Zutaten in den Blender werfen.
5. Kreuzkümmel, Cayenne Pfeffer, Harissa und Salz dazugeben.
6. Ca. 50ml Wasser aufgießen.
7. Den Blender auf hohe Stufe stellen und mit dem Stößel gut vermengen.
8. Nun langsam weiteres Wasser hinzugeben bis ein Brei entsteht, es darf aber nicht flüssig werden.
9. Den Kichererbsen Brei aus dem Blender kratzen und in eine Rührschüssel geben und die Tahin dazugeben.
10. Rührgerät mit Knethaken ausrüsten und den Kichererbsen Brei mit dem Tahin ca. 5 Minuten verkneten. Fertig!

Wir raten dazu die Tahin nicht mit dem Blender unterzurühren, das ist ein wahnsinniger Kraftakt für das Gerät. Einen günstigen Blender haben wir damit schon geschrottet. Als schnelle Alternative haben wir die Rührmaschine hergenommen und siehe da, das Ergebnis wurde sogar viel besser.

>Jedem wie es ihm passt
<cite>Hummus kann viele Formen besitzen, die einen mögen es cremig-fluffig, die anderen fest und kompakt. Spielt mit der Wassermenge und jeder wird glücklich./cite>

## Rezept Fotos

![Rezept Foto Hummus. Die Zutaten für den Hummus liegen auf einem Schneidebrett bereit. Die Gewürze sind vermischt und abgewogen.](hummus-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Hummus. Die Kichererbsen für den Hummus wurden abgetropft in einem großen Sieb.](hummus-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Kichererbsen köcheln am Herd vor sich hin.](hummus-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Hummus. Die Zitronen liegen aufgeschnitten auf einem Küchenbrett.](hummus-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Hummus. Die Zutaten für den Hummus wurden in den Blender gefüllt und sind bereit gleich als creme herauszukommen.](hummus-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Von oben Ansicht: Die Zutaten für den Hummus wurden in den Blender gefüllt und sind bereit gleich als creme herauszukommen.](hummus-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Detailansicht: Die Zutaten für den Hummus wurden in den Blender gefüllt und sind bereit gleich als creme herauszukommen.](hummus-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Von oben Ansicht der pürierten Kichererbsen Masse für den Hummus.](hummus-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Die Kichererbsenmasse wird zusammen mit der Tahin in der Küchemaschine zum Hummus geknetet.](hummus-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Hummus" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
