---
title: Oberpfälzer Semmelknödel
published: true
date: '08:34 12/14/2019'
metadata:
    og:title: 'Rezept: Oberpfälzer Semmelknödel'
    'og:image': https://schmankerl.space/de/blog/semmelknoedel/000-teaser-semmel-semmelknoedel-rezept-gericht.jpg
    'og:image:alt': 'Die Semmelknödel sind auf einem alten bayerischen Teller angerichtet und dampfen noch, da sie gerade aus dem Wasserbad kommen.'
    keywords: 'Semmelknödel, Beilage, Braten, Semmeln, Wild, Sauce, Pilzsauce, Ammerthal, Rezept'
    description: 'Oberpfälzer Semmelknödel: Schmeckt hervorragend zu Wild und Braten mit rahmiger Sauce. Die Ammerthaler Semmelknödel können in vielen Variatenen zubereitet werden.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Beilage
        - Vegetarisch
    tag:
        - Braten
        - Fest
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: mittel
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: '2 Stunde'
    -
        option: 'Zubereitung:'
        value: '45 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Topf, Schüssel, Brotmesser'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Semmelknödel:'
        list:
            - '7-8 glatte oder Kaisersemmeln (Vortag)'
            - '250ml Milch'
            - '30g Butter'
            - '1/2 Tl Salz'
            - '3 mittelgroße Eier'
---

>Der Bratenklassiker:
<cite>Die Ammerthaler Semmelknödel schmecken hervorragend zu Wild und Braten mit rahmiger Sauce. Aber auch für Vegetarier ein Hochgenuß! Mit Pilzsauce, geröstet mit Ei ... sehr vielseitig einsetzbar. </cite>

## Anleitung

1. Die Semmeln vom Vortag kaufen oder frische Semmeln einen Tag trocknen lassen. Habt ihr keine Zeit, die frischen Semmeln 30 Minuten bei 150 Grad im Ofen backen.
2. Die Semmeln in dünne Scheiben schneiden und in eine große Schüssel geben.
3. Die Milch, das Salz und die Butter in einem Topf erhitzen und verrühren. Anschließend direkt über die geschnittenen Semmelscheiben verteilen.
4. Mit einem Kochlöffel die Brotscheiben verrühren. Nach kurzer Wartezeit mit den Händen ordentlich verkneten.
5. Die Schüssel mit einem feuchten Geschirrtuch abdecken und ca. 1-2 Stunden ziehen lassen.
6. Ca. 45 Minuten bevor die Knödel serviert werden, die Eier zu der Semmelmasse geben. Erneut den Teig kraftig durchkneten. Die Masse probieren und ggf. mit Salz abschmecken.
7. Während man mit feuchten Händen 5-6 gleichgroße Knödel formt, in einem großen Topf reichlich Salzwasser zum Kochen bringen.
8. Die Knödel für ca. 20 bis 25 Minuten in das kochende Wasser geben, einmal aufkochen lassen und dann die Hitze reduzieren. Das Wasser soll sieden und nicht mehr kochen.

>Tipp Stromsparen
<cite>Wer ein bißchen energiebewusst Kochen möchte, kann einen Kochlöffel beim Kochen der Knödel zwischen den Deckel klemmen. Das spart normalerweise 1-2 Stufen. Aber Vorsicht, ganz geschlossen kocht das Wasser gerne über.</cite>

## Rezept Fotos

![Rezept Foto selber gemachte Semmelknödel. Die alten Semmeln oder trockenen Brötchen mit einem Brotmesser in ca. 1 cm breite Streifen schneiden.](selbst-gemachte-semmelknoedel-rezept-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto selber gemachte Semmelknödel. Die aufgekochte Milch behutsam über die geschnittenen Semmeln geben.](selbst-gemachte-semmelknoedel-rezept-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto selber gemachte Semmelknödel. Die übergossenen Semmeln mit der Hand vermengen.](selbst-gemachte-semmelknoedel-rezept-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto selber gemachte Semmelknödel. Die Eier zur vermengten Semmelknödel Masse geben. ](selbst-gemachte-semmelknoedel-rezept-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto selber gemachte Semmelknödel. Erneut per Hand die Semmknödel Masse gut verkneten. ](selbst-gemachte-semmelknoedel-rezept-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto selber gemachte Semmelknödel. Beim Kochen der Knödel einen Kochlöffel zwischen den Kochtopf und Deckel klemmen.](selbst-gemachte-semmelknoedel-rezept-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto selber gemachte Semmelknödel. Die gold gelben Semmelknödel liegen in einer alten handbemalten Schüssel.](selbst-gemachte-semmelknoedel-rezept-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto selber gemachte Semmelknödel. Die gold gelben Semmelknödel liegen in einer alten handbemalten Schüssel und dampfen noch.](selbst-gemachte-semmelknoedel-rezept-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto selber gemachte Semmelknödel. Die gold gelben Semmelknödel liegen in einer alten handbemalten Schüssel und sind für das Servieren bereit. Passt am besten zu rahmigen Braten.](selbst-gemachte-semmelknoedel-rezept-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto selber gemachte Semmelknödel. Die bayerischen Semmelknödel sind ein Genuß, gleich werden Sie für einen Sauerbraten serviert.](selbst-gemachte-semmelknoedel-rezept-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Oberpfälzer Semmelknödel" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
