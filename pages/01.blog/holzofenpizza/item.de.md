---
title: Holzofen Pizza
published: true
date: '06:10 07/08/2020'
metadata:
    og:title: 'Rezept: Holzofen Pizza'
    'og:image': https://schmankerl.space/de/blog/holzofenpizza/000-teaser-pizza-teig-selbst-gemacht-rezept-holzofen.jpg
    'og:image:alt': 'Rezept Foto vom Holzofen mit brasselndem Feuer. Der ofen heizt auf, damit der Pizzateig mit Sugo bei ca. 400 Grad auf den heißen Pizzastein gelegt werden kann.'
    keywords: 'Holzofen, Pizza, Holzofen, Sugo, Schüren, Kneten, Pizzateig, Hefeteig, TIPO 00, Rezept'
    description: 'Holzofen Pizza: Klingt einfach, ist aber eine Kunst. Hier gibt es einen großen Erfahrungsbericht wie man Pizzateig für den Holzofen zubereitet und auch den Ofen richtig schürt.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Pizza
        - Italien
        - Holzofen
    tag:
        - Pizza
        - Hits4Kids
        - Holzofen
        - Original
description:
    -
        option: 'Schwierigkeitsgrad: mittel'
    -
        option: 'Reicht für:'
        value: '6 Personen'
    -
        option: 'Vorbereitung:'
        value: '24-94 Stunden'
    -
        option: 'Zubereitung:'
        value: '5 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Holzofen, Küchenmaschine, Knethaken, Schneebesen, Aufbewahrungsboxen, Passierstab'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Teig für 7 Pizzen:'
        list:
          - '900g-1kg Weizenmehl TIPO 00'
          - '100g Hartweizengries'
          - '600ml Wasser'
          - '7g frische Hefe oder 1/2 Päckchen Bierhefe'
          - '30g Meersalz fein'
    -
        title: 'Sugo:'
        list:
            - '2 Dosen gehackte Tomaten, oder 1kg überreife Tomaten am besten San Marzano'
            - '2-3El Olivenöl'
            - '1Tl Oregano'
            - 'Salz und Pfeffer'
            - 'Zucker bei Bedarf'
            -
        title: 'Belag:'
        list:
          - 'Parmaschinken'
          - 'Rucola'
          - 'Balsamicocreme'
          - 'Grana Pandano'
          - 'Gauda mittelalt'
          - 'Mozzarella'
          - 'Cheddar alt'
          -
        title: 'Zusätzlich:'
        list:
          - '50g Reismehl'
---

>Pizza mal anders:
<cite>Kein Gericht für Ungeduldige, aber es lohnt sich! Wer gerne backt, schnibbelt und mit Freunden eine sinnvolle Beschäftigung sucht, der wird seine wahre Freude mit der Holzofen Pizza finden.</cite>

## Pizzateig

> Wahl der Hefe
Je nach Vorlaufzeit nimmt man eine andere Hefesorte. Bierhefe wird bei einer sehr langen Gärzeit von 3-7 Tage verwendet, Trockenhefe bei etwa 1-3 Tage und frische Hefe über Nacht bis 2 Tage. Wir machen den Teig am liebsten 5 Tage vorher mit Bierhefe.

1. 400g Mehl, 100g Hartweizengries, Prise Zucker, 30g Meersalz, 7g Hefe (zerbröckelt) oder das 1/2 Päckchen Bierhefe und 600ml kaltes Leitungswasser in eine Rührschüssel geben.
2. Die Küchenmaschine oder den Mixer mit einem Rührbesen/Schneebesen ausrüsten und das Gemisch kurz zu einem weichen, sämig-flüssigen Teig verrühren (ca. 3 Minuten). Es dürfen keine Mehlschlieren oder Klümpchen im Teig sein. Bei meiner KitchenAid muss ich mit dem Teigschaber den Rand und den Boden einmal abschaben und erneut aufrühren.
3. Den Teig für 45min-60min an einem warmen Ort quellen lassen. Dieser Schritt ist für die Glutenbildung wichtig. Das Mehl muss in Ruhe das Wasser aufnehmen und die Hefe muss anfangen sich zu entwickeln. Dieser Schritt ist der Wichtigste, ansonsten wird der Teig wie Kaugummi.
4. Nach der Wartezeit den flüssigen Teig für weitere 5 Minuten mit dem Rührbesen verrühren.
5. Anschließend weiteres Mehl nach und nach mit dem Knethaken unterheben. Der Teig hat genug Mehl, wenn er sich zu einem Ball formt. Den Teig für weitere 5 Minuten durchnkneten lassen.
6. Der Teig muss immer noch leicht aus der Schüssel laufen – aber nicht mehr fließen wie vorher. Bemehlt ein großes Brett ordentlich und lasst den Teig auf das Brett laufen. Bemehlt die Hände faltet den Teig auf dem Brett immer wieder horizontal zusammen, drückt mit den Handballen fest darauf und dreht den Teig um 90 Grad weiter. Wiederholt diesen Schritt mit weiterem Mehl, bis ein halb-kompakter Teig entsteht. Ihr könnt zur Kontrolle einen großen Ball formen und zuschauen, wie dieser sich langsam wieder ausbreitet. Tut er das sichtbar, ist es genau richtig. Zerfließt der Teig muss mehr Mehl ran.
7. Formt den Teig zu einer langen ca. 10cm breiten Stange. Teilt den Teig mit einer Spachtel oder einem scharften Messer in Teiglinge zu ca. 220-260g auf. Legt die Teiglinge in eine Gärbox oder in einzelne Frischhalteboxen und lasst diese im Kühlschrank mindestens 12 Stunden bis 96 Stunden gären.
8. Mindestens 3 Stunden vor dem Pizzabacken die Teiglinge aus dem Kühlschrank und aus den Boxen holen und abgedeckt mit einem feuchten Geschirrtuch an einen warmen Ort stellen (mindestens 20 Grad besser 24 Grad) und weitere, bis zu 6 Stunden gären lassen.
9. Ein Schneidebrett oder die Arbeitsplatte mit viel Mehl besteuben. Anschließend den Teigling einmal im Mehl wenden und auf die Arbeitsfläche legen. Drückt nun mit den Fingerspitzen den Teig von von innen nach außen. Drückt dabei nur horizontal – auf keinen Fall ziehen oder ausrollen! Dieser Schritt ist der zweitwichtigste, damit sich der Teig nicht zurückzieht. Der Teig ist nur dann gelungen, wenn ihr kein Nudelholz benötigt!
10. Schlagt nun den Teig von links nach rechts über den Handrücken der rechten Hand (nicht umdrehen) und wieder zurück zur Ausgangsposition. Wiederholt diesen Schritt solange, bis der Teig die gewünschte Breite erreicht hat. Wichtig ist dabei, nur von Links nach rechts zu drücken, nicht von unten nach oben.
11. Abschließend den Teig am oberen Ende über beide Fäuste legen und langsam nach unten auslaufen lassen. Ihr habt nun eine runde Pizza, die sich nicht wie Kaugummi zusammenzieht, da der Teig nur geformt und nicht gezogen wurden.
12. Werft in entweder in eine stark geölte Stahlblech Pizzaform oder wieder zurück auf die gut bemehlte Arbeitsfläche. Formt mit den Fingerspitzen die Pizza und knickt bei Wunsch noch den Rand um, damit er etwas höher wird.

> Fazit: Der Vorteig muss wie ein Pfannkuchenteig sein! Bis ich diesen Kniff nicht kannte, machte Pizza nur Freude beim Essen. Jetzt freue ich mich auf das Machen. So muss er aussehen.
>
> ![Pizza Vorteig in einer Rührschüssel, der mit sehr viel Wasser und wenig Mehl zur Glutenkettenbildung in einer Rührmaschine langsam gerührt und nicht geknetet wird.](Holzofenpizza-003.jpg)

## Sugo

Die Tomaten in einem Topf leicht aufköcheln lassen und mit Olivenöl, Oregano, Salz und Pfeffer würzen. Den erhitzten Sugo am besten in einem Blender, oder mit einem Mixer pürieren, bis eine weiche, homogene Flüssigkeit entsteht. Nun kann abgeschmeckt werden, wobei man manchmal etwas Zucker zufügen muss, falls es zu sauer ist. Für den Sugo gilt eigentlich nur die Regel _weniger ist mehr_. Bitte nicht zu viel würzen. Pro Pizza werden ca. 3-4 Eßlöffel Sugo aufgetragen

## Holzofen anschüren

Vor dem Pizzabacken den Holzofen mindestens 1 Tag vorheizen. Wenn ich am Samstag Mittag Pizza backe, schüre ich den Ofen am Freitag Abend für ca. 2-4 Stunden langsam an. Erst ein winziges Feuer mit ca. 1-1,5 zerkleinerten Holzscheite (33 cm) schüren. Ich baue dafür aus den kleinen Scheiten ein Anfeuermodul. Schichtet das Holz erst längs zur Schüre an und eine zweite Schicht quer zur Schüre darauf. Legt zwei ganz  kleine Hölzer wieder längs in die Mitte des Moduls. Richtet die zwei Hölzer so aus, das noch ein Weiteres oben drauf passt. Legt darunter Holzwolle oder einen Grillanzünder und lasst das Modul von oben herab abbrennen. Folgt ihr diesem Tipp, raucht das Anzünden 2 Minuten leicht und danach entsteht kein Rauch mehr.

> Tipp: Legt die untersten Streben des Moduls soweit auseinander, dass nachdem gröbsten Abbrennen drei weitere Holzstücke von unten eingeschoben werden können. Somit verhindert ihr beim Draufschüren die Rauchentwicklung.

Es wird Zeit nachzulegen. Legt neben das Feuer weitere kleine Holzscheite und schiebt das noch brennende Feuer darauf und verschiebt somit die Branntstelle nach rechts oder nach links. Somit wird der gesamte Feuerraum erwärmt. Das erhöht die Langlebigkeit des Ofens, denn so wird nicht nur die Mitte belastet. Lasst das Feuer mit dieser Methode ca. 1,5 Stunde abbrennen und schiebt die Glut anschließend nach ganz hinten. Nun ist es an der Zeit etwas mehr Holz nachzulegen. Am Ende könnt ihr nachdem das Feuer erloschen ist die Glut verteilen und die Ofentür schließen. Über Nacht sollte der Feuerraum komplett verschlossen sein, damit kaum Wärme entweicht.

> Tipp: Wir nutzen die Hitze und Restwärme von Tag 1 für weitere Gerichte. Nehmt ihr die Glut am Ende aus dem Ofen habt ihr ca. 240 Grad im Ofen, das ist perfekt für ein Holzofen Brot. Bei 270 Grad könnt ihr bereits einen Flammkuchen backen. Sonntagsbraten für das Mittagessen ist auch kein Problem. Alle Schmorgerichte können mit einem Gusseisernen Bräter und einem Deckel sehr gut über Nacht garen. Bedenkt aber, der Ofen zieht viel Feuchtigkeit, verdoppelt die normale Flüssigkeitsmenge für das Schmoren.

Schürt am Backtag den Ofen ca. 3-4 Stunden vorher mit der oben genannten Methode an. Die ersten zwei Schüren ist für das Aufwärmen. Fängt unter dem Kamin an und verlagert das Feuer mit jeder Schüre zur Seite und nach hinten bzw nach vorne. So wird der gesamte Backraum gleichmäßig heiß. Anschließend eine mittelgroße Schüre, ca. 2,5-3 Holzscheite, am besten Buchenholz anzünden und 3/4 abbrennen lassen. Legt nun nach und nach zerkleinerte Holzzscheite nach. Je nach Bauform und Größe kann ein ganzes Scheit oder wie bei mir (Tunnelofen mit ca. 30cm Höhe) ca. 1/4 Scheit draufgelegt werden. Ca. 30-45 Minuten vor dem Backen die letzte große Schüre draufgeben, 2 Scheite bzw. bei mir ca. ein 1,5 Scheite auflegen. An diesem Schritt blase ich mit einem Blasebalg oder ganz kurz mit dem Kompressor in den Ofen. Dabei verfliegt die Asche vom Boden und das Feuer entfacht kurz. Bei diesem Schritt sehe ich auch die Glutentwicklung. Der Ofen sollte bei einer Temperaturmessung an den Rändern (Infrarotmessung) ca. 330-450 Grad aufweisen. Ich teste die Temperatur auch gerne mit der Hand. Halte ich die Hand in den Ofen – Mindestabstand zum direkten Feuer 30cm – halte ich es nach 1 Sekunden nicht mehr aus, dann ist der Ofen heiß genug.

Das noch brennende Holz muss seinen Höhepunkt bereits erreicht haben, sprich am Abbrennen sein. Räumt nun sämtliches Holz und die Glut nach hinten. Kehrt oder blast vor dem Pizza einlegen die Asche vom Boden weg.

Lasst nachdem Verschieben mindestens 15 Minuten verstreichen, bis sich an der Oberfläche eine homogene Hitze einstellt.

> Tipp: Werft eine kleine Hand voll Mehl auf die Steinoberfläche, verbrennt das Mehl direkt und wird schwarz, ist der Boden noch zu heiß. Wir das Mehr braun, seit ihr bereit für den Backvorgang.

## Pizza backen

Ist die Pizza auf einem Pizzablech, könnt ihr die Pizza sehr nah an das Feuer schieben, ist die Pizza direkt auf dem Stein, haltet einen Sicherheitsabstand von ca. 20-25cm zum direkten Feuer, ansonsten verkohlt der Rand. Im Blech muss die Pizza öfters und schneller gedreht werden. Ist der Steinboden nicht heiß genug, dass die Pizza von unten knackig wurde, könnt ihr die Pizza aus der Form heben und kurz auf den Stein geben.

> Tipp: Wenig Erfahrung? Fangt mit den Pizzablechen an und tastet euch langsam zur Kunst des Pizzabackens direkt auf dem Stein vor. Wir haben auch lange dafür gebraucht bis es geklappt hat.

Beim Belegen und in den Ofen schieben, passieren die häufigsten Fehler, die einen katastrophalen Ausgang für die Pizza haben. Zusammenschieben, kleben bleiben, zerreißen, verdrehen, zusammenklappen ... In den Pizzaformen habt ihr leichtes Spiel. Sugo, etwas Käse und Zutaten darauf und ab in den Holzofen.

Bei der klassichen Steinofenvariante kommt nun das große Finale. Die runden Pizzateiglinge müssen in der nähe des Ofens sein. Wir tragen unser großes Holzbrett in den Garten. Der Pizzaschieber wird mit **Reismehl** flächig besteubt. Das Reismehl fängt nicht so schnell zum kleben an wie das Weizenmehl, das verhindert ein schnelles Ankleben des Teigs. Die Pizzateiglinge sollten so kurz wie möglich auf den bemehlten Stellen sein, sowohl auf dem Holzbrett als auch dem Schieber. Das vorherige Bemehlen des Bretts und des Schiebers sind für den Erfolg ausschlaggebend. Macht ihr zu viel drauf, schmeckt die Pizza fürchterlich nach Mehl bzw. nach verbrannten Mehl wenn der Holzofen zu heiß ist. Fataler ist zu wenig Mehl, denn der Teig bleibt dann einfach am Schieber kleben. Dann gibt's ne Calzone Pizza.

Idealerweise habt ihr die Pizza gerade eben geformt, das Holzbrett stark bemehlt und den Teigling fertig darauf platziert. Gebt nun den Sugo dazu und achtete auf den Druck beim Verstreichen – ganz sachte. Käse drüber streuen.

Den Teigschieber stark mit Reismehl einreiben und mit einem gekonnten Doppelschub die Pizza auf den Schieber rutschen lassen. Der Doppelschub ist ein Millisekundenstopp in der Mitte, der mit einem minimalem Anheben den Teig auf den Schieber flutschen lässt. Mit einem Schub läuft man Gefahr, die Pizza zu quetschen oder sogar über den Schieber fliegen zu lassen ... Alles schon passiert! Nach 10 Pizzas in Folge, sollte man den Kniff heraus haben.

Leider gleich noch eine wichtige Sache. Weniger ist mehr. Wer eine American Pizza mit 10 Zutaten mag, wird hier keine Freude haben. Die Pizza ist viel zu schwer. Wenige, dafür ausgewählte Zutaten auf die Pizza geben. Nachdem Aufschieben ganz schnell und sachte die Zutaten auf die Pizza geben und ab in den Ofen.

Legt die Pizza lieber etwas weiter weg vom Feuer. Liegt Sie ohne Pizzablech einmal, müsst ihr diese anbacken lassen, ihr könnt sie nicht gleich verschieben! Ist die Hitze recht hoch und die Pizza zu nah am Feuer habt ihr einen verkohlten Rand innerhalb von 20 Sekunden. Ihr könnt die Pizza nicht drehen, da der Boden noch instabil ist. Bei der richtigen Temperatur und Abstand ist die Pizza nach ca. 45 Sekunden angebacken und ihr könnt Sie drehen oder verschieben. Jetzt ist Fingerspitzengefühl und PizzabäckerInnen Gen gefragt. Verschiebt nach Lust und Laune bis die gewünschte Bräune erreicht wurde. Ich drehe insgesamt zwei Mal. Je nach Größe des Ofens dicke des Teigs und eingependelter Hitze, braucht eine Pizza ca. 2-4 Minuten bis sie fertig ist. Dauert der Vorgang länger, wird es Zeit nachzuschüren.

## Holzofen Nachschüren

Bei meinem Pizzabackofen mit ca. Innenraumfläche 90 cm lang, 80 cm breit und 27 cm hoch muss ich nach etwa 6-8 Pizzas nachschüren. Ich kann immer 2 Pizzas gleichzeitig backen und schaffe demnach 2-3 Durchgänge.

Sobald man nachschürt hat man ca. 30-45 Minuten keine Möglichkeit eine weitere Pizza zu backen. Dann ist erstmal Pause.

Das Problem ist nicht die Oberhitze des Ofens, sondern die Bodenplatte, die recht schnell auskühlt. Je nach Modell und Material variiert Ober- und Unterhitze Abnahme. Ich kann in diesem Fall nur für meinen Miniofen sprechen.

Ich schiebe die Glut in die Mitte des Ofens, damit der Kamin mehr Wirkung zeigt. Danach kommen, am besten klein gespaltene, Buchenholzscheite auf die Glut. Ich nehme 8 kleine Scheite 33 cm und verteile sie der Breite nach in der Mitte des Ofens. Bei noch gut 300 Grad im Ofen fangen diese sofort Feuer. Nach ca. 10 Minuten abbrennen, verteile ich die gesamte Glut und die brennenden Holzscheite auf der primären Backfläche des Ofens.

![Pizza Holzofen Luigi der gerade mit Buchenholz nachgeschürt wurde und flammt und lodert.](Holzofenpizza-007.jpg)

Ich versuche also die Hauptbackstelle als erstes zu erhitzen. Mache ich das am Schluss, wird die erste Pizza einen schwarzen Boden bekommen. Oder ich warte noch 20 Minuten nachdem Schürvorgang. Diese Zeit kann man sich sparen, wenn hungrige Gäste schon auf die nächste Pizza warten.

Achtet darauf die Seiten des Ofens auch mit zu heizen, nicht nur die Mitte. Generell gilt, ähnlich wie beim ersten Anschüren, verteilt die Hitze gleichmäßig. Das Backergebnis wird besser und die Langlebigkeit des Ofens wird erhöht.

>Variante
<cite>Wir backen die Pizzen wie beschrieben erst fertig und belegen sie anschließend mit frischen Zutaten, wie z.B.: Parma-, Serranoschinken, Rucola, Feigen, Balsamicocreme.</cite>

## Rezept Fotos

![Rezept Foto Holzofen Pizza Komplettanleitung. Verschiedene Mehle und eine italienische Bierhefe stehen auf dem Holzbrett bereit für das Pizzabacken bzw. den Teig.](holzofenpizza-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Holzofen Pizza Komplettanleitung. Der Vorteig der Pizza wird gerade in der Küchenmaschine zu einem flüssigen Teig verrührt.](holzofenpizza-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Holzofen Pizza Komplettanleitung. Foto in Schwarz-Weiss. Zwei Pizzableche stehen nebeneinander, sie sind mit dem fertigen Pizzateig ausgelegt und warten darauf mit Sugo bestrichen zu werden.](holzofenpizza-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Holzofen Pizza Komplettanleitung. Foto in Farbe. Zwei Pizzableche stehen nebeneinander, sie sind mit dem fertigen Pizzateig ausgelegt und warten darauf mit Sugo bestrichen zu werden.](holzofenpizza-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Holzofen Pizza Komplettanleitung. Brasselndes Feuer im Holzofen sorgt beim Nachschüren für ordentlich Hitze.](holzofenpizza-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Holzofen Pizza Komplettanleitung. Beim Anschüren und nachschüren des Holfofens werden die Scheite und das Feuer von links nach rechts und später von vorne nach hinten verschoben. Das Bild zeigt das linksseitige Feuer, das richtig kräftig abbrennt.](holzofenpizza-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Holzofen Pizza Komplettanleitung. Die Pizza bruzzelt gerade im kräftig brennenden Holzofen.](holzofenpizza-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Holzofen Pizza Komplettanleitung. Foto in Schwarz Weiss. Die Pizza nähert sich dem Höhepunkt im Ofen, gleich ist sie fertig.](holzofenpizza-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Holzofen Pizza Komplettanleitung. Fertig gebackene Pizza mit Salami und frischem Gartengemüse.](holzofenpizza-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Holzofen Pizza Komplettanleitung. Fertig gebackene Pizza mit Salami und frischem Gartengemüse auf einem Holzbrett angerichtet.](holzofenpizza-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Holzofen Pizza Komplettanleitung. Detailaufnahme. Fertig gebackene Pizza mit Salami und frischem Gartengemüse auf einem Holzbrett angerichtet.](holzofenpizza-014.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Holzofen Pizza Komplettanleitung von A-Z" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
