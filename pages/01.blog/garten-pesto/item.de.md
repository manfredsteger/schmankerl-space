---
title: Grünes Gartenpesto
published: true
date: '10:34 08/27/2019'
metadata:
    og:title: 'Rezept: Grünes Gartenpesto'
    'og:image': https://schmankerl.space/de/blog/garten-pesto/000-teaser-garten-pesto-gruen-basilikum-petersilie.jpg
    'og:image:alt': 'Rezept Foto Kräuterpesto. Das Foto zeigt viele Zutaten des Pestos. Im Vordergrund steht Parmesanreibe und ein Stück Grana Pandano und ein Messbecher voll mit frischen Kräutern.'
    keywords: 'Pesto, italienisch, Kräuter, Garten, Basilikum, Nüsse, Kerne, Petersilie, Rezept'
    description: 'Grünes Gartenpesto: Gartenkräuter über? Kein Problem! Hier werden ihr fast alles los. Bei den Nüssen und Kernen ebenso, einfach mal ausprobieren. Geht nicht, gibt es nicht!'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Pasta
        - Pesto
        - Vegetarisch
    tag:
        - Dip
        - Pasta
        - Pesto
        - schnell
        - Kräuter
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Reicht für:'
        value: '6 Personen'
    -
        option: 'Vorbereitung:'
        value: 'keine'
    -
        option: 'Zubereitung:'
        value: '30 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Topf, Stabmixer oder Blender, Käsereibe'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Gartenpesto:'
        list:
            - '60g Basilikum'
            - '25g Petersilie'
            - '25g Bärlauch'
            - '30g Cashewkerne'
            - '15g Pinienkerne'
            - '15g Erdnüsse'
            - '80g Parmesan im Stück'
            - '200ml Olivenöl'
            - '1 Knoblauchzehe'
            - 'Salz & Pfeffer'
---

>Aufgepasst:
<cite>Das Grüne Gartenpesto kann in vielen Variationen zubereitet werden. Je nach Kräuter im Garten kann variiert werden. Bei den Nüssen und Kernen ebenso, einfach mal ausprobieren!.</cite>

## Anleitung

1. Die Kerne und Nüsse, wenn nicht bereits so gekauft, in einer Pfanne leicht anrösten.
2. Basilikum, Petersilie und Bärlauch waschen und von den Stielen befreien.
3. Den Knoblauch in Scheiben schneiden.
3. Die Kräuter, Nüsse, Kerne, Knoblauch und Olivenöl in einen Messbecher geben und kräftig durchmixen. Das Rohpesto sollte von einem Eßlöffel gleiten, ohne große Rückstände dabei zu hinterlassen. Ist die Konsistenz zu grob und fest, weiteres Olivenöl hinzugeben und weitermixen. Wer einen leistungsstarken Blender hat (ca. 2000 Watt und ab 10000 Umdrehungen pro Minute) kann das Pesto in zwei Phasen erstellen. Eine Hälfte auf höchster Stufe zu einem sämigen Brei verarbeiten und die andere Hälfte nur grob hacken. Am Ende wieder zusammenführen und man hat eine traumhafte Konsistenz. Funktioniert leider nicht mit handelsüblichen Stabmixern.
4. Den Parmesan fein hobeln und mit einem Eßlöffel unter das Pesto heben.
5. Wieder auf die Konsistenz achten, das Grüne Pesto sollte geschmeidig und nicht zu fest sein. Bei Bedarf weiteres Olivenöl hinzugeben und seicht unter die Masse heben.
6. Zum Schluß noch ein wenig mit Salz und Pfeffer abschmecken.

>Und wie servieren?
<cite>Das Grüne Pesto passt hervorragend zu Tagliatelle, frischem Holzofenbrot oder angegrilltem Ciabatta. </cite>

## Rezept Fotos

![Rezept Foto Kräuter und Garten Pesto. Salatschüssel mit frischen Kräutern darin.](rezept-garten-pesto-basilikum-petersilie-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Messbecher mit den verschiedenen Zutaten darin.](rezept-garten-pesto-basilikum-petersilie-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Schneidebrett mit Olivenöl Flasche einer Parmesan Reibe und den Kräutern darauf.](rezept-garten-pesto-basilikum-petersilie-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Seitenansicht Schneidebrett mit Olivenöl Flasche einer Parmesan Reibe und den Kräutern darauf.](rezept-garten-pesto-basilikum-petersilie-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Messbecher mit Kräutern und Nüssen und Kernen darin.](rezept-garten-pesto-basilikum-petersilie-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Katharina pflückt gerade frisches Basilikum aus dem Garten.](rezept-garten-pesto-basilikum-petersilie-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Frisches Garten Basilikum liegt zusammen mit mehreren Knoblauchzehen auf dem Schneidebrett.](rezept-garten-pesto-basilikum-petersilie-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Die Kräuter werden in den Messbecher gegeben.](rezept-garten-pesto-basilikum-petersilie-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Messbecher mit den Zutaten und dahinter steht eine Flasche natives Olivenöl.](rezept-garten-pesto-basilikum-petersilie-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Die restlichen Kräuter werden ordentlich in einem großen Sieb gewaschen.](rezept-garten-pesto-basilikum-petersilie-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Nahaufnahme des Knoblauchs der bereits geschält wurde.](rezept-garten-pesto-basilikum-petersilie-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. In der Bratpfanne rösten die Pinienkerne langsam vor sich hin.](rezept-garten-pesto-basilikum-petersilie-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Ein Teil des Pestos wurde bereits mit dem Pürierstab fein gemacht und zu einer glatten Masse verarbeitet.](rezept-garten-pesto-basilikum-petersilie-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. ](rezept-garten-pesto-basilikum-petersilie-014.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Ansicht des Blenders mit den Kräutern, dem Knoblauch und den Gewürzen.](rezept-garten-pesto-basilikum-petersilie-015.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kräuter und Garten Pesto. Panorama Foto des Haus Gartens aus dem die ganzen Zutaten für das Kräuter Pesto kommen.](rezept-garten-pesto-basilikum-petersilie-016.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Grünes Gartenpesto" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
