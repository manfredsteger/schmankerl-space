---
title: 'Maronenrisotto'
published: true
date: '13:00 05/08/2020'
metadata:
    'og:title': 'Rezept: Maronenrisotto'
    'og:image': https://schmankerl.space/de/blog/Maronenrisotto/000-teaser-maronen-risotto-rezept-gericht.jpg
    'og:image:alt': 'Rezept Foto von Maronenrisotto zeigt eine getöpferte Tonschale mit frischen Maronen darin.'
    keywords: 'Risotto, Maronen, Vegetarisch, Reis, Gorgonzola, Rezept'
    description: 'Maronenrisotto: Mit wenigen Zutaten, aber etwas Geduld zur wahren Geschmacksexplosion.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Reis
        - Risotto
        - Italien
        - Vegetarisch
    tag:
        - Italienisch
        - Gorgonzola
        - Brühe
description:
    -
        option: 'Schwierigkeitsgrad: mittel'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Zubereitung:'
        value: '45 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Mittelgroßer Topf, Messbecher'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Maronenrisotto:'
        list:
            - '350g Risotto'
            - '500g Maronen'
            - '100g Gorgonzola'
            - '1,2l Gemüsebrühe'
            - '150ml Weißwein oder 40ml Weißweinessig'
            - '1 Schalotte'
            - '30g Butter'
            - '10g Walnüsse'
            - 'Olivenöl'
            - 'Salz & Pfeffer'

---

>Mit ständigem Rühren und etwas Geduld zum wahren Geschmackserlebnis.
<cite>Hier ist man praktisch an den Ofen gefesselt und sollte das Risotto nicht allzu lange aus den Augen lassen. Aber die Mühe zahlt sich immer aus.</cite>

## Anleitung

1. Die Maronen an der spitzen Seite kreuzförmig mit einem Messer einritzen und etwa 40 Minuten in reichlich Salzwasser kochen. Die Schale an der eingeritzten Seite sollte sich danach leicht nach außen wölben.
2. Gießt das Wasser ab und lasst die Maronen auskühlen. Anschließend sollten sie recht leicht zu schälen sein.
3. Schneidet, oder brecht die geschälten Maronen in kleinere Stücke.
4. Den Gorgonzola so gut es geht ebenfalls in Stücke schneiden, die Walnüsse hacken.
5. Die Schalotte klein würfeln und in einem mittelgroßen Topf mit etwas Olivenöl goldgelb anschwitzen.
6. Gebt den Risottoreis hinzu und lasst ihn unter ständigem Rühren etwas mit anrösten.
7. Nun wird je nach Belieben entweder mit Weißwein, oder Weißweinessig abgelöscht. Lasst die Flüssigkeit ruhig bei gleicher Hitze unter gleichmäßigem Rühren, eine gute Minute verdampfen. Das nimmt dem Essig, oder Wein die Intensität.
8. Reduziert die Hitze nun etwas.
9. Jetzt wird mit soviel Gemüsebrühe aufgegossen, dass das Risotto gerade vollständig mit Flüssigkeit bedeckt ist. Rührt beim Zugießen gut um, damit sich die Brühe richtig verteilt. Dann wird gewartet und nur gelegentlich umgerührt, bis die Flüssigkeit fast vollständig aufgesogen wurde. Hier liegt die einzige Schwierigkeit beim Risotto kochen, da man nicht zu früh, oder zu viel Brühe zugeben sollte und zeitgleich aufpassen muss, dass der Reis nicht anbrennt.
10. Wiederholt Schritt 9. solange, bis die Brühe vollständig aufgebraucht ist.
11. Ist das Risotto noch zu bissig, gebt weitere Flüssigkeit dazu. Jede Risottosorte ist hier etwas anders.
12. Reduziert nun weiter die Hitze und gebt die Maronen, Walnüsse und den Gorgonzola dazu. Lasst alles unter gelegentlichem Rühren etwa 3-5 Minuten schwach köcheln.
13. Der Herd kann nun ausgeschaltet werden. Gebt zum Schluss die Butter dazu, sie kann bei geschlossenem Deckel schmelzen. Mit Salz und Pfeffer abschmecken.

Das Risotto sollte nun "cremig-schlonzig" sein. Unterschätzt nicht, dass der Reis in den letzten Schritten noch ordentlich nachgart. Lasst ihn nach der letzten Zugabe der Brühe also lieber etwas bissig, sonst wird er am Ende zu weich.

>Tipp
<cite>Habt ihr eine Kräuterecke in der Küche oder einen Garten? Am besten wird das Risotto mit selbstgemachter Gemüsebrühe aus Petersilie, Liebstöckel, Petersilienwurzel und Salz.</cite>

## Rezept Fotos

![Rezept Foto Maronenrisotto. Katharina ritzt mit einem scharfen Messer kreuzweise die Maronen ein.](maronen-risotto-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Maronenrisotto. Detailaufnahme Katharina ritzt mit einem scharfen Messer kreuzweise die Maronen ein.](maronen-risotto-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Maronenrisotto. Schwarzweiss Foto Schneidebrett, Messer und ein Haufen Maronen.](maronen-risotto-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Maronenrisotto. Geschälte und zerteilte Maronen in einer Rührschüssel auf dem Küchentisch.](maronen-risotto-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Maronenrisotto. Selbstgemachte Gemüsebrühe wird in den Risotto Reis eingerührt.](maronen-risotto-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Maronenrisotto. Unter ständigem Rühren wird das Risotto langsam schlonzig und fertig.](maronen-risotto-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Maronenrisotto. Der Risotto Reis köchelt langsam vor sich hin und nimmt die perfekte Konsistenz an.](maronen-risotto-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Maronenrisotto. Der heiße Topf mit Risotto steht auf dem Esstisch bereit und mit einer Kelle wurde gerade der erste Schöpfer genommen.](maronen-risotto-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Maronenrisotto. Maronenrisotto angerichtet auf einem spitzen Teller mit einer Walnuss garniert.](maronen-risotto-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Maronenrisotto" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
