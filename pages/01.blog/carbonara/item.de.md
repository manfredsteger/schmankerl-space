---
title: 'Spaghetti Carbonara'
published: true
date: '13:00 12/18/2019'
metadata:
    'og:title': 'Rezept: Spaghetti Carbonara'
    'og:image': https://schmankerl.space/de/blog/carbonara/000-teaser-carbonara-schinken-parma-nudelgericht.jpg
    'og:image:alt': 'Rezept Foto von der Spaghetti Carbonara. Das Foto zeigt dieses Mal kein Essen, sondern einen alten italienischen Laden, vor dem der Parma Schinken für das Rezept hängt.'
    keywords: 'Spaghetti, Carbonara, Italien, Pasta, Nudeln, Fettucine, Tagliatelle, Schinken, Pecorino, Parma, Rezept'
    description: 'Spaghetti Carbonara: Italien kann so einfach aber gleichzeitg so lecker sein. Massimiliano aus Anguillara zeigte uns die perfekte Carbonara.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Pasta
        - Italien
        - Nudeln
    tag:
        - Italienisch
        - Eier
        - Schinken
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Zubereitung:'
        value: '45 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Edelstahlpfanne, Käsereibe'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Spaghetti Carbonara:'
        list:
            - '600g Spaghetti'
            - '5 Eier'
            - '40g Pecorino'
            - '20g Parmesan'
            - '50g Pancetta, Parma oder am besten luftgetrockneten Schweinehalsspeck (original)'
            - '20ml Nudelwasser'
            - Sonnenblumenöl
            - 'Salz & Pfeffer'
---

>Italien kann so einfach und so lecker sein.
<cite>In Deutschland auch vom Italiener meist falsch zubereitet, berichtet unsere Verwandtschaft aus Anguillara. Massimiliano zeigte uns die perfekte Carbonara.</cite>

## Anleitung

1. In einer Pfanne ein wenig Öl auf niedrige Hitze bringen und den Schinken darin langsam anrösten.
2. Derweilen Salzwasser zum Kochen bringen und die Spaghetti al dente kochen.
3. Ein ganzes Ei in eine Schüssel schlagen und 4 Eigelbe trennen und dazugeben. Kurz miteinander verrühren. Die Käsesorten fein reiben.
4. Den Speck aus der Pfanne nehmen und beiseite stellen. Den Herd ausschalten.
5. Die Nudeln absieben und das Nudelwasser auffangen.
6. Die Nudeln in die Pfanne geben und die Bratrückstände beim ständigen Wenden der Nudeln aufsaugen. Anschließend die Eier dazugeben und 30 Sekunden unterheben.
7. Den geriebenen Pecorino und Parmesan 1 Minute lang unterheben.
8. Mit etwas Nudelwasser vermengen bis die gewünschte Konsistenz erreicht wird. Perfekt ist es, wenn keine Flussigkeit auf dem Teller vorhanden ist und die Eier nicht zum Stocken begonnen haben.
9. Speck wieder hinzugeben, Pfannrühren und genießen.

>Tipp
<cite>Es kommt nicht auf die Menge Schinken, sondern auf den Reifegrad an. Das Fett eines gut angehangenen Schinkens ist der Hauptgeschmacksträger dieses Rezeptes.</cite>

## Rezept Fotos

![Rezept Foto Spaghetti Carbonara. Italienischer Metzger in einer kleinen Gasse in Anguillara. Die Parma Schinken hängen vor der Ladentür.](rezept-spaghetti-carbonara-anguillara-italien-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Spaghetti Carbonara. Der Parma Schinken wird in einer Edelstahlpfanne ganz langsam kross gebraten.](rezept-spaghetti-carbonara-anguillara-italien-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Spaghetti Carbonara. Die Nudeln kommen zum Speck und werden darin gerührt.](rezept-spaghetti-carbonara-anguillara-italien-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Spaghetti Carbonara. Die Nudeln werden auf einem großen Teller angerichtet und mit Pecorino verfeinert.](rezept-spaghetti-carbonara-anguillara-italien-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Spaghetti Carbonara" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
