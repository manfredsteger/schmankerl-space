---
title: Winterliche Barbarie Entenbrust
published: true
date: '11:20 12/31/2020'
metadata:
    og:title: 'Rezept: Winterliche Barbarie Entenbrust'
    'og:image': https://schmankerl.space/de/blog/barbarieentenbrust/000-titelbild.jpg
    'og:image:alt': 'Rezept Foto der angerichteten Barbarie Entenbrust mit traditionellem Blaukraut und einem Kartoffelknödel.'
    keywords: 'Rezept, winterlich, Ente, Entenbrust, Barbarie Ente, Weihnachtsessen, Geflügel'
    description: 'Rezept barbarieentenbrust mit winterlichem Rotkohl an einer Blaukraut Glühwein Sauce. Ein Traum für das Weihnachtsfest.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Geflügel
    tag:
        - Entenbrust
        - Ente
        - Feierlich
description:
    -
        option: 'Schwierigkeitsgrad: schwer'
    -
        option: 'Reicht für:'
        value: '3 Personen'
    -
        option: 'Vorbereitung:'
        value: '2 Stunden'
    -
        option: 'Zubereitung:'
        value: '1 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Herd, Ofen, Edelstahlpfanne, Topf, Pürierstab oder Blender, Bratenthermometer'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Barbarie Ente:'
        list:
            - '600g Barbarie Entenbrustfilets'
    -
        title: 'Glühwein Rotkohlsauce:'
        list:
            - '2 mittelgroße Zwiebeln'
            - '4 Scheiben Ingwer'
            - '1 El Honig'
            - '2 El Balsamicoessig'
            - '8 El Glühwein'
            - '100ml Gemüsebrühe oder Entenfond'
            - '4 El Blaukraut zubereitet'
            - '1-2 Tl Speisestärke'
            - '2 Nelken'
            - 'Zimt'
            - 'Kerbel'
            - 'Salz & Pfeffer'
---

>Ente gut alles Gut.
<cite>Festlich kann auch richtig schnell gehen und selbstverständlich mega lecker sein.</cite>

## Anleitung

1. Die frische Barbarie Entenbrust ca. 2 Stunden vor dem geplanten Festessen aus dem Kühlschrank holen und bei Zimmertemperatur abgedeckt 1 Stunde akklimatisieren lassen. Habt ihr eine tiefkühl Entenbrust, dann lasst diese einen Tag vorher im Kühlschrank langsam auftauen.
2. In der Zwischenzeit die Zwiebel schälen und in kleine Würfel stückeln.
3. Die Ingwer Scheiben von der Ingwerwurzel schneiden und beiseite legen.
4. Etwa 1 Stunde vor dem Essen die Entenbrüste abtupfen und die Fettseite kräftig einsalzen. Lasst das Salz 20 Minuten die obere Fettschicht entwassern und tupft anschließend mit einer Küchenrolle vorsichtig das austretende Wasser ab – jedoch nicht das Salz wegwischen. Bei Bedarf nochmals nachsalzen.
5. Den Ofen auf 120 Grad vorheizen.
6. Legt die Entenbrüste mit der Fettseite nach unten in eine kalte Edelstahlpfanne und dreht den Herd auf mittlere Stufe auf. Die Entenbrüste sollen langsam und gleichmäßig erhitzt werden. Ihr braucht kein Fett, das hat eine gute Ente mehr als genug.
7. Verteilt auf der Fleischseite die Ingwerscheiben.
8. Bratet die Entenbrüste langsam und gleimäßig an, bis eine hellbraune Haut sichtbar wird. Das dauert je nach Fettschicht und Größe der Entenbrust ca. 10 Minuten.
9. Dreht die Entenbrust auf die Fleischseite und gebt die Zwiebelstückchen hinzu. Lasst die Ente noch etwa 3-4 Minuten auf dieser Seite mit in der Pfanne. Der Ingwer wird nun mit angebraten und bleibt in der Pfanne.
10. Nehmt die Entenbrust aus der Pfanne und legt sie in den Ofen zum garen. Je nach Dicke der Entenbrust dauert dies 20 Minuten bis eine Kerntemperatur zwischen 65-70 Grad erreicht wird. Der Gargrad hängt vom Geschmack ab, wir bevorzugen 68 Kerntemperatur.
11. Ab jetzt muss es mit der Sauce schnell gehen. Die Zwiebeln zum Entenfett in die Pfanne geben und hellbraun anschwitzen.
12. Mit dem Glühwein ablöschen und die Hitze erhöhen.
13. Blaukraut, Balsamicoessig, Gemüsebrühe oder Entenfond, Nelken, und einen kleinen Zweig Kerbel zugeben und ca. 10 Minuten köcheln lassen.
14. Währenddessen die Speisestärke mit einem Schuss kaltem Leitungswasser anmischen und beiseite stellen.
15. Werft immer wieder einen Blick auf die Kerntemperatur der Entenbrust. Reguliert bei Bedarf die Hitze nach oben oder nach unten.
16. Fischt mit einer Gabel den Kerbel und die Ingwerscheiben aus der Sauce. Püriert nun die Sauce inkl. der Blaukraut Einlage. Es sollte ein kompaktes Püree entstehen.
17. Streicht ca. die Hälfte des Pürees durch ein feines Sieb und fangt die Sauce auf.
18. Mit Salz, Pfeffer, Glühwein und Zimt abschmecken und zum Schluss bei Bedarf mit der Speisestärkemischung abbinden.
19. Die fertige Entenbrust aus dem Ofen holen und in Küchenrolle einwickeln und auf einem Teller zurück in den Ofen legen.
20. Der abgegangene Bratensaft kommt in die Sauce. Einmal das Ganze verrühren und nochmals final abschmecken. Ist die Sauce zu bitter, kommt noch etwas Honig hinzu, ist sie zu süß ein Schuss Glühwein.
21. Die Entenbrust aufschneiden und den Saft erneut zur Entensauce geben.

>Ein Eigenkreation
<cite>Dieses Gericht ist ein eigenes Schmankerl, dass seit Jahren von uns gemacht wird.</cite>

## Rezept Fotos

![Rezept Foto der Zutaten die für die Entebrust und die Glühweinsauce benötigt werden.](barbarie-entenbrust-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto der Entenbrust in der Edelstahlpfanne.](barbarie-entenbrust-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>![Rezept Foto der Entenbrüste die scharf angebraten in der Pfanne schmoren.](barbarie-entenbrust-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>![Rezept Foto der Barbarie Ente im Ofen mit Bratenthermometer.](barbarie-entenbrust-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>![Rezept Foto der Glühweinsauce die langsam einreduziert.](barbarie-entenbrust-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>![Rezept Foto der fertig pürierten und abgebundenen Glühweinsauce.](barbarie-entenbrust-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>![Katharina schneidet mit einem Küchenmesser schöne Entenbrust Streifen.](barbarie-entenbrust-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>![Rezept Foto der angerichteten Barbarie Entenbrust mit traditionellem Blaukraut und einem Kartoffelknödel.](barbarie-entenbrust-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>![Detailansicht Rezept Foto der angerichteten Barbarie Entenbrust mit traditionellem Blaukraut und einem Kartoffelknödel.](barbarie-entenbrust-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Winterliche Barbarie Entenbrust" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
