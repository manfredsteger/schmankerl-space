---
title: Rotes Pesto
published: true
date: '17:34 03/10/2018'
metadata:
    og:title: 'Rezept: Rotes Pesto'
    'og:image': https://schmankerl.space/de/blog/rotes-pesto/000-teaser-rotes-pesto-rosso-tomaten-getrocknet-rezept.jpg
    'og:image:alt': 'Rezept Foto vom Roten Pesto. Das Foto zeigt wie Katharina gerade Öl in den Mixer gibt, damit anschließend alle Zutaten zum Roten Pesto werden.'
    keywords: 'Pesto, italienisch, Rotes Pesto, Tomaten, getrocknete Tomaten, Chili, Olivenöl, Pinienkerne, Rezept'
    description: 'Rotes Pesto: Bei diesem Rezept kommen wieder die selbst eingelegten, getrockneten Tomaten zum Einsatz. Schnell gemacht und immer ein grandioses "fast food".'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Pasta
        - Pesto
        - Vegetarisch
    tag:
        - Dip
        - Pasta
        - Pesto
        - schnell
        - lang haltbar
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: 'keine'
    -
        option: 'Zubereitung:'
        value: '30 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Topf, Stabmixer'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Rotes Pesto:'
        list:
            - '200g eingelegte, getrocknete Tomaten'
            - '80g Cashewkerne'
            - '50g Pinienkerne'
            - '100g Walnüsse'
            - '80g Parmesan im Stück'
            - '150ml Olivenöl'
            - '100g Feta-Käse'
            - '600g Spaghetti'
---

>Aufgepasst:
<cite>Für dieses Rezept kommen wieder unsere selbst eingelegten, [getrockneten Tomaten](https://schmankerl.space/de/blog/eingelegte-getrocknete-tomaten) zum Einsatz.</cite>

## Anleitung

1. Die Pinienkerne in einer Pfanne bei geringer Hitze goldbraun anrösten. Walnüsse und Cashewkerne grob hacken.
2. Eingelegte, getrocknete Tomaten klein schneiden.
3. Die Nüsse und Tomaten in den Mixer geben. Wir verwenden die Chilli, den Knoblauch und das Olivenöl aus den selbst eingelegten, getrockneten Tomatengläschen gleich mit. Die Zutaten ausgiebig durchmixen.
4. Anschließend den Parmesan reiben, zur Pestomasse geben und erneut mixen. Die Konsistenz sollte leicht krümelig und sämig werden.
5. Bis zum Servieren in den Kühlschrank stellen.
6. Die Spaghetti im Salzwasser al dente kochen, abgießen und mit Olivenöl benetzen.
7. Zum Servieren den Feta-Käse grob zerkleinern und mit dem Pesto auf die Nudeln geben.
8. Noch etwas frischen Parmesan darüber reiben und viel davon genießen.

## Rezept Fotos

![Rezept Foto Rotes Pesto Rosso. Das Foto zeigt viel verschiedene Nüsse und Kerne. Walnüsse, Cashew Kerne, Haselnüsse und Erdnüsse. Die Gläser zum Abfüllen für das Pesto stehen auf einem Schneidebrett.](rezept-rotes-pesto-rosso-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Rotes Pesto Rosso. Nahaufnahme verschiedener Nüsse und Kerne. Walnüsse, Cashew Kerne, Haselnüsse und Erdnüsse. Die Gläser zum Abfüllen für das Pesto stehen auf einem Schneidebrett.](rezept-rotes-pesto-rosso-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Rotes Pesto Rosso. ](rezept-rotes-pesto-rosso-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Rotes Pesto Rosso. Ein paar Einweckgläser stehen auf dem Schneidebrett und werden mit Oliveöl befüllt.](rezept-rotes-pesto-rosso-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Rotes Pesto Rosso. Die Zutaten kommen in einen Multihacker und das Olivenöl wird gerade hinzugefügt. Gleich wird das Pesto zerkleinert.](rezept-rotes-pesto-rosso-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Rotes Pesto Rosso. Das Rezept angerichtet auf einem Teller. Ein kleiner Haufen Sphaghetti und ein Klecks Rotes Pesto darauf. Als Krönung frischer Parmesan.](rezept-rotes-pesto-rosso-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Rotes Pesto Rosso. Seitenaufnahme des Rezepts, angerichtet auf einem Teller. Ein kleiner Haufen Sphaghetti und ein Klecks Rotes Pesto darauf. Als Krönung frischer Parmesan.](rezept-rotes-pesto-rosso-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Rotes Pesto Rosso. Nahaufname des Rezepts, angerichtet auf einem Teller. Ein kleiner Haufen Sphaghetti und ein Klecks Rotes Pesto darauf. Als Krönung frischer Parmesan.](rezept-rotes-pesto-rosso-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Rotes Pesto" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
