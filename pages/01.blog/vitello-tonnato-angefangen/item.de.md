---
title: Vitello Tonnato
published: false
date: '14:00 09/13/2019'
metadata:
    og:title: 'Rezept: Vitello Tonnato'
    'og:image': https://schmankerl.space/de/blog/vitello-tonnato/000-titelbild.jpg
    'og:image:alt': 'xxx'
    keywords: 'Vitello Tonnato, Fisch, Fischsauce, Dipp, Fisch Paste, Italien, italienisch, Rezept'
    author: 'Chefköche Katharina und Manfred Steger'
    description: 'Vitello Tonnato: '
taxonomy:
    category:
        - Fisch
        - Dip
        - Vorspeise
    tag:
        - Dip
        - Fisch Paste
        - Snack
        - Italien
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Zubereitung:'
        value: '1 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Stabmixer oder Blender'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Vitello Tonnato:'
        list:
            - 'xxx g Thunfisch oder Makrelenfilets eingelegt in Öl'
            - 'xxx ml Sonnenblumen Öl'
            - '3 Kapern in Salz eingelegt'
            - 'Pfeffer'
            - 'xxx g Kalbfleisch'
---

## Anleitung

1. Das Kalbfleisch kochen.
2. Die Fischfilets aus dem Öl nehmen und zusammen mit dem Öl, den Kapern und dem Pfeffer im Blender oder mit dem Stabmixer zu einer sämigen Creme zerkleinern.
3. Das Fleisch in dünne Scheiben schneiden und mit der Fisch Creme bestreichen.

>Geschichte zum Rezept.
<cite>Der Barsch ...</cite>

## Rezept Fotos

[flickr-photoset id=72157693277826571]

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Wiener Schnitzel mit Kartoffelsalat" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
