---
title: Currywurst Sauce
published: true
date: '17:34 08/10/2022'
metadata:
    og:title: 'Rezept: Currywurst Sauce'
    'og:image': https://schmankerl.space/de/blog/currywurstsauce/000-titelbild.jpg
    'og:image:alt': 'Currywurst Sauce im Glas mit echtem Thai Curry'
    keywords: 'Rezept, Berlin, deftig, Tomaten, Curry, Currywurst, Kinderessen'
    description: 'Currywurst Sauce: Keinen Bock auf Ketchup Pampe? Dann macht mit frischen Tomaten die eigene Currywurst Soße mit hochwertigen Zutaten.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Sauce
    tag:
        - Currywurst
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '30 Portionen'
    -
        option: 'Vorbereitung:'
        value: '60 Minuten'
    -
        option: 'Zubereitung:'
        value: '4 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Topf, Blender oder Stabmixer'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Currywurst Soße:'
        list:
            - '2kg frische Tomaten (überreif)'
            - '1kg geschälte Tomaten in der Dose'
            - '300 ml Apfelessig'
            - '10El Speiseöl'
            - '10 Tropfen Liquid Smoke'
            - '2El Paprika Pulver'
            - '1El Scharfes Paprika Pulver'
            - '1El Geräuchertes Paprika Pulver'
            - '1El Chipotle Chili Pulver'
            - '1Tl Kreuzkümmel'
            - '2El Red Thai Curry Pulver'
            - '7El Indisches Curry Pulver'
            - '400g Brauner Zucker'
            - '5 Zehen frischer Knoblauch'
            - '2 El Knoblauch Pulver'
            - '5 mittelgroße Zwiebeln'
            - 'Optional: Chili für die Schärfe'
            - '2 1/2El Salz'
---

>Bombastisch
<cite>Wer genauso wenig wie ich Ketchup mag, wird diese Sauce lieben! Echter Tomatengeschmack, wenig Säure und volle Gewürzpower machen deine Wurst nicht nur zum Leckerbissen, sondern zu einer Gourmetvariante! Deine Pommes und du werden es lieben!</cite>

## Anleitung

1. Die überreifen frischen Tomaten waschen, entstielen und falls kein Blender oder Stabmixer im Haushalt ist, die Schale abziehen.
2. Knoblauch und Zwiebeln schälen und hacken.
3. Das Öl in einem sehr großen Topf auf mittlerer bis kleiner Stufe erhitzen und den Braunen Zucker hinzugeben.
4. Den Zucker langsam zu Krokant kross braten und kurz bevor er klumpt die Zwiebeln und den Knoblauch kurz dazugeben. Beides sollte nicht rösten, nur gar werden.
5. Anschließend mit dem Essig ablöschen und 2-3 Minuten gelegentlich rühren. Der Zucker sollte sich langsam wieder auflösen.
6. In der Zwischenzeit die Tomaten pürieren und anschließend in den Topf schütten.
7. Alles muss langsam erhitzt werden bis die Sauce köchelt.
8. In der Zwischenzeit können alle Gewürze hinzugefügt werden.
9. Ab jetzt heißt es immer wieder rühren und nach ca. 1 Stunde langsamen köcheln geht es ans Abschmecken.
10. Die Sauce sollte ca. 2-3 Stunden leicht simmern und jede Stunde sollte die Sauce auch probiert und mit den Gewürzen abgestimmt werden.

## Tipps

> Süße Säure Verhältnis

Je länger ihr die Sauce köcheln lasst, desto mehr verliert sie an Säure. Eine ausgewogene Mischung an Zucker-Essig-Köchelzeit führt zum individuellen besten Ergebnis. Einfach viel ausprobieren!

---

> Geschmack perfekt aber nicht die Konsistenz

Schmeckt eure Soße perfekt ist aber zu flüssig, dann nehmt einfach 1-2El Speisestärke, rührt diese mit 2-3El kaltem Wasser an, bis keine Stärke mehr zu sehen ist. Rührt sie nach und nach und schnell unter die Sauce.

---

> Nachdem Kochen noch Klumpen vorhanden?

Holt den Pürrierstab oder den Blender heraus und passiert die Sauce erneut. Das kann man nicht oft genug tun. Danach erneut kurz aufkochen und ca. 10min simmern lassen.

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Currywurst Sauce" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
