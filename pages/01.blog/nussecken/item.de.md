---
title: 'Altammerthaler Nussecken'
published: true
date: '17:20 12/14/2019'
metadata:
    og:title: 'Rezept: Altammerthaler Nussecken'
    'og:image': https://schmankerl.space/de/blog/nussecken/000-teaser-nussecke-nussecken-rezept-weihnachten-schoko.jpg
    'og:image:alt': 'Rezept Foto vom Altammerthaler Nussecken. Das Foto zeigt die frisch bestrichenen Nussecken, die auf einem Silbertablett darauf warten, gegessen zu werden.'
    keywords: 'Nussecken, Rezept, Backen, Plätzchen, Weihnachten, Gebäck, selber backen, Weihnachtsrezept, Rezept'
    description: 'Die Ammerthaler Nussecken sind ein traditionalles Gebäck aus der Weihnachtszeit. Knusprige Mandeln und leckere Schokolade machen diese Plätzchen besonders lecker.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Backen
    tag:
        - Kuchen
        - Weihnachten
        - Gebäck
        - Plätzchen
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Reicht für:'
        value: 'ca. 25 Ecken'
    -
        option: 'Zubereitung:'
        value: '1 Stunde 30 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Springform rechteckig (ca. 35cm x 25cm), Rührgerät, Rührschüssel, Knethaken, kleiner Topf, Pinsel'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Boden:'
        list:
            - '300g Mehl'
            - '130g Zucker'
            - '130g Butter'
            - '2 Eier'
            - '2 Päckchen Vanillezucker'
            - '1 Tl Backpulver'
    -
        title: 'Belag:'
        list:
            - '4 El Aprikosenmarmelade'
            - '200g Zucker'
            - '200g Butter'
            - '2 Päckchen Vanillezucker'
            - '4 El Wasser'
            - '200g gemahlene Haselnüsse'
            - '200g gestiftete Mandeln'
            - '200g Zartbitterkuvertüre'
            - '1 El Speiseöl'
---

>Suchtpontential extrem hoch!
<cite>Einmal genascht, kommt man schwer von den Nussecken wieder los. Kann schnell zum Ganzjahres Leckerli werden :)</cite>

---

## Anleitung

1. Für den Boden alle Zutaten in eine Rührschüssel geben und mit dem Knethaken zu einem glatten Teig verrühren.
2. Den Teig zu einer Kugel formen und mit Frischhaltefolie einwickeln. Mindestens eine halbe Stunde im Kühlschrank ruhen lassen.
3. Den Teig mit den Händen gleichmäßig auf dem Springformboden verteilen und mehrmals mit einer Gabel einstechen.
4. Die Aprikosenmarmelade auf dem Boden verstreichen.
5. Den Backofen anschließend auf 175°C Ober- und Unterhitze vorheizen.
6. Für den Belag Butter, Zucker, Vanillezucker und Wasser in einem Topf aufkochen und anschließend die Hitze auf eine geringe Stufe reduzieren.
7. Haselnüsse und Mandelsplitter unterrühren.
8. Die warme Masse auf dem bestrichenen Boden verteilen.
9. Die Form für ca. 25 Minuten in den Ofen geben. Die Nussecken sind fertig, wenn die Ränder leicht braun werden und die gesamte Oberfläche golden karamelisiert ist.
10. Aus dem Ofen nehmen und auskühlen lassen. Anschließend mit einem Messer am Rand der Springform entlangfahren, damit die Springform abgenommen werden kann.
11. Schneidet erst mehrere Reihen horizontal und dann vertikal. Passt dabei auf, dass in etwa gleichgroße Quadrate entstehen. Nun können diese diagonal in Ecken geschnitten werden.
13. Die Kuvertüre erwärmen und einen El Speiseöl unterrühren.
14. Je nach Wunsch die Nussecken mit der Kuvertüre bestreichen. Wir bepinseln am liebsten die Kanten.

## Rezept Fotos

![Rezept Foto Ammerthal Nussecken. Brauner Zucker und weißer Zucker in einer Schüssel für den Teig der Nussecken.](rezept-nussecken-backen-weihnachten-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Der geschmeidige Teig liegt als Kugel auf einer Frischhaltefolie.](rezept-nussecken-backen-weihnachten-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Die gebackenen Nussecken liegen noch im ganzen auf der Backform.](rezept-nussecken-backen-weihnachten-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Detailaufnahme der gebackenen Nussecken auf der Backform.](rezept-nussecken-backen-weihnachten-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Die Nussecken kommen gerade frisch aus dem Ofen und duften herrlich nach Mandeln und kandiertem Zucker.](rezept-nussecken-backen-weihnachten-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Detailaufnahme der Nussecken die gerade frisch aus dem Ofen kommen.](rezept-nussecken-backen-weihnachten-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Katharina schneidet die Nussecken gerade in Form.](rezept-nussecken-backen-weihnachten-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Einmal längs geschnitten und einmal quer geschnitten, werden die Nussecken zu Quadraten portioniert.](rezept-nussecken-backen-weihnachten-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Detailaufnahme wie die Nussecken Quadrate mit einem Diagonalschnitt zu Nussecken werden.](rezept-nussecken-backen-weihnachten-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Detailaufnahme des Portionierens der Nussecken.](rezept-nussecken-backen-weihnachten-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Ansicht von oben, wie die Nussecken im Raster geteilt wurden.](rezept-nussecken-backen-weihnachten-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Detailansicht von der Seite, wie die Nussecken im Raster geteilt wurden.](rezept-nussecken-backen-weihnachten-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Der Pinsel wird gerade in die Zartbitterkuvertüre getaucht und gleich auf die Nussecken gestrichen.](rezept-nussecken-backen-weihnachten-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Ansicht der fertig lasierten Nussecken auf einem Silbertablett.](rezept-nussecken-backen-weihnachten-014.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Ammerthal Nussecken. Draufsicht der fertig lasierten Nussecken auf einem Silbertablett.](rezept-nussecken-backen-weihnachten-015.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Altammerthaler Nussecken" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
