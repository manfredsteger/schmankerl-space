---
title: Lauchzwiebel-Kuchen
published: true
date: '16:34 02/21/2020'
metadata:
    og:title: 'Rezept: Lauchzwiebelkuchen'
    'og:image': https://schmankerl.space/de/blog/lauchzwiebelkuchen/000-teaser-lauchzwiebelkuchen-zwiebelkuchen-rezept.jpg
    'og:image:alt': 'Rezept Foto vom Lauchzwiebelkuchen. Auflaufform gefüllt mit der Quiche Füllung, die gleich im Ofen gebacken wird.'
    keywords: 'Lauch, Zwiebel, Quiche, Vegetarisch, Teig, Speck, Rezept'
    description: 'Lauchzwiebelkuchen: Eine wunderbare Quichevariation, die einen in kalten, ungemütlichen Wintertagen von Innen heraus wärmt.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Backen
    tag:
        - Quiche
        - Zwiebel
        - Lauch
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: '1 1/2 Stunden'
    -
        option: 'Zubereitung:'
        value: '1/2 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Quicheform oder Springform, große Schüssel, Topf, Reibe oder Küchenmaschine, Backofen, Nudelholz'


ingredients_title: Zutaten
ingredients:
    -
        title: 'Teig:'
        list:
            - '250g Weizenmehl (Type 405)'
            - '100g Frischkäse'
            - '100g Butter'
            - '1 Eigelb'
            - 'Salz'
            - 'Olivenöl'
    -
        title: 'Füllung:'
        list:
            - '1 Stange Lauch'
            - '2 mittelgroße Zwiebeln'
            - '125g Speck'
            - '200g saure Sahne'
            - '50g Schmand'
            - '3 Eier'
            - '120g geriebener Käse (z.B. Gouda, Emmentaler, o.ä.)'
            - 'Muskatnuss'
            - 'Salz & Pfeffer'

---

>Ein Kuchen darf auch mal deftig sein.
<cite>Wer gerne Kuchen backt, der wird dieses Gericht lieben. Sowohl kalt auf der Party oder zum Mittag frisch aus dem Ofen, der Lauchzwiebelkuchen schmeckt immer.</cite>

## Anleitung

1. Für den Teig die Butter in kleine Flocken oder Stücke schneiden und das Ei trennen. Nun kann das Mehl mit Frischkäse, Butter, Eigelb und einer Prise Salz zu einem glatten Teig verknetet werden. Diesen in Frischhaltefolie eine Stunde im Kühlschrank ruhen lassen.
2. Die Quiche-, oder Springform mit einem Schuss Olivenöl einfetten. Die klassische Kuchenspringform lässt den Mürbeteig knackig werden. In den klassischen Keramikformen wird der Teig oftmals etwas latschig.
3. Den Teig grob mit einem Nudelholz ausrollen und in die Form geben. Der Rand muss nicht komplett nach oben gezogen werden. Hier reicht ein kurzes Stück.
4. Mehrmals mit einer Gabel den Teig einstechen.
5. Zwiebel in größere Würfel schneiden. Käse reiben.
6. Entfernt beim Lauch etwa die Hälfte des Grün und schneidet den Rest in dünne Ringe.
7. Den Ofen auf 200°C Ober-/Unterhitze vorheizen.
8. Den Speck in einem Topf kross anbraten und anschließend die Zwiebel zugeben. Sie sollten leicht anbräunen. Zum Schluss den Lauch nur kurz mit anschwitzen, sonst zerfällt er zu stark. Nun könnt ihr die geschmackstragenden Bratrückstände am Topfboden mit einem kleinen Schluck Wasser lösen.
9. Die saure Sahne und den Schmand mit den Eiern in einer großen Schüssel verquirlen und mit Salz, Pfeffer und Muskatnuss würzen. Anschließend den geriebenen Käse, angebratenen Speck, Zwiebeln und Lauch dazugeben und alles gut miteinander vermengen.
9. Nun kann die Füllung hineingegeben und alles für etwa eine Stunde im vorgeheizten Backofen gebacken werden. Der Kuchen ist fertig, wenn er leicht gebräunt ist. Ansonsten könnt ihr einfach mit einem Holzstäbchen in die Masse stechen. Bleibt nichts mehr kleben, kann der Kuchen serviert werden.

>Auf Wunsch auch vegetarisch:
<cite>Auch wenn er ein hervoragender Geschmacksträger ist, kann der Speck jederzeit weggelassen werden.</cite>

>Teigvariation:
<cite>Wer verschiedene Mehlsorten zu Hause hat, kann diese einfach 1:1 mischen. Das Weizenmehl verträgt sich auch gut mit Vollkorn-, oder Dinkelmehl.</cite>

## Rezept Fotos

![Rezept Foto Lauchzwiebelkuchen backen und genießen. Ein mit Mehl besteubtes Brett und der Mürbeteig des Lauchzwiebelkuchens liegt in einem Ball geformt darauf.](lauch-zwiebel-kuchen-000.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. Auf einem Schneidebrett liegt der gewaschne Lauch und ein scharfes Küchenmesser.](lauch-zwiebel-kuchen-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. Speck wird in einem kleinen Topf auf dem Ofen scharf angebraten.](lauch-zwiebel-kuchen-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. In einer Springform ist der Mürbeteig für den Lauchzwiebelkuchen ausgelegt und mit einer Gabel eingestochen worden.](lauch-zwiebel-kuchen-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. Eier, saure Sahne und Schmand sind in der Rührschüssel und warten darauf gemixed zu werden.](lauch-zwiebel-kuchen-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. Der Lauch wurde in der Zwischenzeit in dünne Ringe geschnitten.](lauch-zwiebel-kuchen-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. Die geschnittenen Lauchringe werden zum angebratenen Speck und Zwiebeln gegeben und miteinander vermengt.](lauch-zwiebel-kuchen-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. Die angebratenen Zutaten werden zusammen mit der verquirlten Zutaten in die Springform gegeben.](lauch-zwiebel-kuchen-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. Detailaufnahme der angebratenen Zutaten die zusammen mit den verquirlten Zutaten in die Springform gegeben werden.](lauch-zwiebel-kuchen-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. ](lauch-zwiebel-kuchen-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. Der fertige, dampfende Lauchzwiebelkuchen kommt gerade aus dem Ofen und ist goldbraun gebacken.](lauch-zwiebel-kuchen-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. Der Lauchzwiebelkuchen wurde aus der Form genommen und mit einem Messer in Häppchen geschnitten.](lauch-zwiebel-kuchen-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lauchzwiebelkuchen backen und genießen. Ein Stück Lauchzwiebelkuchen wurde auf einem kleinen blauen Tonteller angerichtet und mit Salz und Pfeffer bestreut.](lauch-zwiebel-kuchen-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>


## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Lauchzwiebelkuchen" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
