---
title: Tröpfchenkuchen
published: true
date: '10:34 06/03/2018'
metadata:
    og:title: 'Rezept: Tröpfchenkuchen'
    'og:image': https://schmankerl.space/de/blog/troepfchenkuchen/000-teaser-troepfchenkuchen-kuchen-eischnee-pudding-rezept.jpg
    'og:image:alt': 'Eine Detailaufnahme vom Wiener Schnitzel mit Kartoffelsalat. Das Schnitzel ist goldbraun gebraten und raucht noch, da es gerade frisch aus der Pfanne gekommen ist.'
    keywords: 'Tröpfchenkuchen, Nachtisch, Süßspeise, Sahnepudding, Kuchen, Backen, Gebäck, Rezept'
    description: 'Tröpfchenkuchen: Innen weich und cremig und außen verführerisch knackig, ein Alltagskuchen, der von Tag zu Tag leckerer wird.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Backen
    tag:
        - Kuchen
        - Lieblingskuchen
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Reicht für:'
        value: '10 Personen'
    -
        option: 'Zubereitung:'
        value: '2 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Kuchenform (26cm Durchmesser), Rührgerät, Rührschüssel, Knet- und Rührhaken'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Boden:'
        list:
            - '75g Butter'
            - '75g Zucker'
            - '1 Ei'
            - '200g Mehl (Type 1050)'
            - '1 Tl Backpulver'
    -
        title: 'Belag:'
        list:
            - '500g Quark'
            - '200g Zucker'
            - '1/2 Tasse Sonnenblumenöl'
            - '400ml Milch'
            - '1 Sahnepuddingpulver'
            - '3 Eier'
---

>Wenn Kakao auf Vanille trifft:....
<cite>Außen knackig innen weich und cremig, ein verführerischer Kuchen, der von Tag zu Tag leckerer wird.</cite>

---

## Anleitung

1. Die Kuchenform einfetten oder den Teig direkt auf die beschichtete Form geben. Praktisch ist auch, ein Backpapier auf den Boden der Form zu legen und den Ring fest darauf spannen. Somit lässt sich der gebackene Teigboden problemlos lösen und zum Anrichten auf einen Servierteller schieben.
2. Für den Boden einen Mürbteig aus den oben genannten Zutaten zusammenkneten. Damit die Kuchenform ausfüllen und einen leichten Rand nach oben ziehen.
3. Zwischenzeitlich den Backofen auf 175 Grad Ober-, Unterhitze vorheizen.
4. Für den Belag Quark und 100g Zucker verrühren und langsam das Sonnenblumenöl und die Milch hinzugießen. Die Eier trennen und die Eigelb direkt zur Masse geben. Das Eiweiß separat in einer Tasse oder Rührschüssel aufbewahren. Abschließend noch das Sahnepuddingpulver zugeben und zu einer homogenen Masse verrühren.
5. Die Quarkmasse in die Kuchenform geben und für 45 Min. in den aufgeheizten Backofen stellen.
6. Nach etwa 40 Min. das ausgehobene Eiweiß zu einer Créme schlagen und nach und nach die restilichen 100g Zucker hineinrieseln lassen. Der Eischnee sollte nicht zu fest und lange geschlagen werden, ansonsten wird er nicht so voluminös und _fluffig_.
7. Die Kuchenform kurz aus dem Ofen holen und den Eischnee auf die Quarkmasse streichen. Er muss nicht bis an den Rand der Form reichen, da er im Ofen automatisch etwas auseinandergeht. Nun erneut für weitere 15 Min. bei gleicher Temperatur backen.

>"Special effect":
<cite>Spätestens am Folgetag sollten auf dem gebackenen Eischnee zahlreiche kleine und große goldgelbe Tröpfchen erscheinen. Wie ihr seht, lohnt sich auch hier das Warten :)</cite>

## Rezept Fotos

![Rezept Foto Tröpfchenkuchen. Butter wird in die Kitchen Aid Küchenmaschinen gegeben und der Knethaken für den ersten Schritt eingespannt.](troepfchenkuchen-rezept-anleitung-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Die Springform wurde mit einem Fettpapier ausgelegt und auf den Boden der Form gespannt.](troepfchenkuchen-rezept-anleitung-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Die Zutaten für den Kuchen Boden werden mit dem Knethaken verrührt.](troepfchenkuchen-rezept-anleitung-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Nachdem Kneten mit der Maschine wird der Teig noch mit der Hand nachgeknetet.](troepfchenkuchen-rezept-anleitung-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Und nochmal nachgeknetet.](troepfchenkuchen-rezept-anleitung-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Der fertige Kuchen Teig wird auf die Springform dünn aufgetragen und mit den Fingern ausgelegt.](troepfchenkuchen-rezept-anleitung-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Die Zutaten für die Füllung des Kuchens kommt in die Rührschüssel.](troepfchenkuchen-rezept-anleitung-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Nachdem die Masse gut verrührt wurde kommt alles in die Form und ab in den Ofen.](troepfchenkuchen-rezept-anleitung-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Nach kurzer Zeit im Ofen sieht man die goldgelbe Oberfläche des Kuchens langsam gestalt annehmen.](troepfchenkuchen-rezept-anleitung-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Der geschlagene Eischnee kommt nachdem ersten Backvorgang auf den Kuchen.](troepfchenkuchen-rezept-anleitung-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Der geschlagene Eischnee kommt nachdem ersten Backvorgang auf den Kuchen. Die Reste werden mit dem Schneelöffel aus der Form gekratzt.](troepfchenkuchen-rezept-anleitung-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Wirklich auch der letzte Rest wird aus der Rührschüssel herausgekratzt.](troepfchenkuchen-rezept-anleitung-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. ](troepfchenkuchen-rezept-anleitung-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Der Eischnee wird mit einem Schaumlöffel auf der Oberfläche vorsichtig verteilt.](troepfchenkuchen-rezept-anleitung-014.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Der Kuchen steht noch im Ofen und ist nach der Backzeit fertig. Mit einem Schaschlik Stäbchen noch schnell testen ob der Kuchen durch ist.](troepfchenkuchen-rezept-anleitung-017.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Nachdem letzten Backvorgang sieht der Kuchen goldgelb mit einer leichten Eischneekruste saftig und lecker aus.](troepfchenkuchen-rezept-anleitung-016.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Tröpfchenkuchen. Der fertige Kuchen wurde bereits angeschnitten und auf Tellern verteilt.](troepfchenkuchen-rezept-anleitung-015.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Tröpfchenkuchen" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
