---
title: Eingelegte, getrocknete Tomaten
published: true
date: '17:34 03/09/2018'
metadata:
    og:title: 'Rezept: Eingelegte, getrocknete Tomaten'
    'og:image': https://schmankerl.space/de/blog/eingelegte-getrocknete-tomaten/000-teaser-eingelegte-getrocknete-tomaten-olivenoel.jpg
    'og:image:alt': 'Rezept Foto von den eingelegten getrockneten Tomaten. Das Foto zeigt ein Einweckglas voller getrockneter Tomaten und eine Flasch Olivenöl.'
    keywords: 'Tomaten, Italien, getrocknet, Glas, Einwecken, Einlegen, Chili, Pesto, Olivenöl, Rezept'
    description: 'Eingelegte getrocknete Tomaten: Schnell gemacht für das eigene Pesto oder in Verbindung mit dem italienischem Nudelsalat das perfekte Rezept.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Grundzutat
        - Eingemachtes
    tag:
        - Tomaten
        - In Öl
        - italienisch
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Vorbereitung:'
        value: '1 Stunde'
    -
        option: 'Zubereitung:'
        value: '2 Wochen'
    -
        option: 'Ausstattung:'
        value: 'Einmachgläser'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Eingelegte getrocknete Tomaten:'
        list:
            - '500g getrocknete Tomaten ohne Flüssigkeit'
            - '5 getrocknete kleine Chillis'
            - '500ml Olivenöl'
            - '5 Zehen Knoblauch'
---

>Grundzutat:
<cite>Dieses Rezept verwenden wir für das [rote Pesto](../rotes-pesto/item.de.md) und den [italienischen Nudelsalat](../italienischer-nudelsalat/item.de.md).
Die Menge der Zutaten variiert je nach Größe der Gläser.</cite>

## Anleitung

1. Die Gläser vorab mit heißem Wasser ausspülen.
2. Pro Glas nehmt ihr je 1 Chilli und Knoblauchzehe. Den Knoblauch gerne zerteilen, wenn er zu groß ist.
3. Drückt die getrockneten Tomaten ins Glas, bis es randvoll ist.
4. Mit Olivenöl auffüllen, bis alle Tomaten bedeckt sind. Deckel drauf und fertig.
5. Die eingelegten Tomaten sollten mindestens 2 Wochen lagern. Ab 1 Monat haben sie ihr volles Aroma angenommen.

## Rezept Fotos

folgen

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Eingelegte getrocknete Tomaten" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
