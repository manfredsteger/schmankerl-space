---
title: Dotsch aka Reiberdatschi
published: true
date: '10:34 01/05/2021'
metadata:
    og:title: 'Rezept: Dotsch aka Reiberdatschi'
    'og:image': https://schmankerl.space/de/blog/dotsch/000-titelbild.jpg
    'og:image:alt': 'Rezept Foto vom Bayerischen Dotsch auch Reiberdatischi genannt. Goldbraun gebraten liegt er mit einem Klecks Apfelmuß angerichtet auf einem Teller.'
    keywords: 'Rezept, bayerisch, Oberpfalz, Karftoffelpuffer, Dotsch, Reiberdatischi'
    description: 'Dotsch aka Reiberdatschi: Ein Gericht und wahnsinnig viele Namen. Kartoffelpuffer, Reibekuchen, Dotsch, Reiberdatschi und noch viele mehr. Alles haben Sie eins gemeinsam, wenig Zutaten und der Hit für Jung und Alt.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Vegetarisch
    tag:
        - Kartoffeln
        - Karftoffelpuffer
        - Dotsch
        - Reiberdatischi
description:
    -
        option: 'Schwierigkeitsgrad:'
    -
        option: 'Reicht für:'
        value: '3 Personen'
    -
        option: 'Vorbereitung:'
        value: 'keine'
    -
        option: 'Zubereitung:'
        value: '2 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Pfanne, Kartoffelreibe, Kloßsäckchen, Schüssel'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Dotsch:'
        list:
            - '2kg Kartoffeln vorwiegend festkochend'
            - '2 mittelgroße Eier'
            - 'Butterschmalz'
            - 'Salz'
---

> Der Dotsch ist schnell gemacht und lässt sich auch super einfrieren.
<cite>Kinder lieben dieses Gericht genauso wie Erwachsene und man darf nicht vergessen, der Dotsch ist nicht nur mit Apfelmuß klasse, sondern auch als Beilage zu vielen anderen Rezepten.</cite>

## Anleitung

1. Die Kartoffeln schälen und abwaschen.
2. Je nach Kartoffelreibe, die ganzen Kartoffeln per Hand oder Maschine in eine große Schüssel reiben. Die Konsistenz ist bei einer Kartoffelreibe einzigartig, versucht es erst gar nicht mit einer anderen.
3. Die Kartoffeln mit einem Kartoffelsäckchen auspressen. Wie viel Wasser noch in der Masse bleiben soll, ist je nach Kartoffel abzuwägen. Probiert es einfach aus. Ist die Masse zu wässrig hält sie beim Braten nicht und zerfließt. Ist sie zu trocken bröckelt sie beim Brutzeln. Ich drücke solang bis nur noch ein feiner Strahl Stärkewasser austritt. Ihr könnt ein wenig Stärkewasser auffangen, falls die Masse zu trocken wird, könnt ihr wieder etwas hinzugeben.
4. Die Eier zur ausgedrückten Kartoffelmasse geben, genügend Salz dazu und gut vermengen.
5. Probiert den Rohteig und entscheidet ob er noch weiter abgeschmeckt werden muss oder ggf. noch ein Ei für die Konistenz hinzugefügt werden soll.
6. Erhitzt eine Pfanne auf mittlerer Hitze und gebt immer ein großes Stück Butterschmalz dazu. Bitte nicht sparen!
7. Heizt den Ofen auf ca. 120 Grad vor und legt auf ein Blech mehrere Schichten Küchenrolle zum Abtropfen der fertigen Reiberdatischis.
8. Gebt nun mit einer Kelle immer einen guten Schwupps Rohmasse in die Pfanne. Passt dabei auf, dass nicht mehr als 2/3 der Pfanne befüllt ist, ansonsten verwässert die Pfanne und der Anbrateffekt verschwindet.
9. Bratet solange bis sie von beiden Seiten goldbraun sind.
10. Die fertigen Dotschis ab zum Entfetten in den Ofen schieben.

>Dotsch Klassiker mit Apfelmuß
<cite>Zum Reiberdatschi passt perfekt selbstgemachtes Apfelmuß, das kaum gezuckert ist.</cite>

## Rezept Fotos

![Rezept Foto Kartoffelpuffer. Katharina reibt die Kartoffeln für den Puffer mit einer Kartoffelreibe in der Küchemaschine.](dotsch-reiberdatschi-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Der fertig geriebebe Kartoffelteig liegt in einer Schüssel mit den Zutaten bereit.](dotsch-reiberdatschi-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Manfred drückt die Kartoffelmasse durch ein Kloßsäckchen, damit das Stärkewasser entweicht.](dotsch-reiberdatschi-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Dotsch. Die Kartoffelmasse wurde entwässert und liegt mit den übrigen Zutaten zum Vermengen bereit.](dotsch-reiberdatschi-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Reiberdatschi der gerade in der Pfanne mit viel Butterschmalz vor sich hinbruzzelt. Bald wird er fertig sein.](dotsch-reiberdatschi-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)


## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Dotsch aka Reiberdatschi" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
