---
title: 'Wiener Schnitzel mit Kartoffelsalat'
published: true
date: '17:34 03/04/2018'
metadata:
    og:title: 'Rezept: Wiener Schnitzel mit Kartoffelsalat'
    'og:image': https://schmankerl.space/de/blog/wiener-schnitzel/000-teaser-wiener-schnitzel-kalb-kalbfleisch-rezept-gericht.jpg
    'og:image:alt': 'Eine Detailaufnahme vom Wiener Schnitzel mit Kartoffelsalat. Das Schnitzel ist goldbraun gebraten und raucht noch, da es gerade frisch aus der Pfanne gekommen ist.'
    keywords: 'Wiener Schnitzel, Kartoffelsalat, Schnitzel, österreichisch, Bayern, Salat, Kartoffelgratin, Kartoffeln, Beilage, Rezept'
    description: 'Wiener Schnitzel mit Kartoffelsalat: In Bayern genauso lecker wie in Graz oder Wien. Zartes Kalbfleisch und knusprige Panade machen dieses Rezept unwiderstehlich.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Schmankerl
    tag:
        - Salat
        - Fleisch
        - Schmankerl
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht-mittel
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: '1 Tag'
    -
        option: 'Zubereitung:'
        value: '1 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Edelstahlpfanne, große Schüssel, Fleischklopfer'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Wiener Schnitzel:'
        list:
            - '4 Kalbschnitzel'
            - 'Weizenmehl Type 405'
            - Semmelbrösel
            - '3 Eier'
            - '1 Biozitrone oder -limette'
            - 'Salz & Pfeffer'
            - 'Butterschmalz oder Butter'
    -
        title: 'Bayrischer Kartoffelsalat:'
        list:
            - '1,2kg festkochende Kartoffeln'
            - 'je 1 rote und weiße Zwiebel'
            - '175g Cornichons (Abtropfgewicht)'
            - '10 EL Olivenöl'
            - '6 EL Apfelessig'
            - '4 EL Gurkenwasser'
            - '1 EL Zucker'
            - 'Salz & Pfeffer'
---

>Beilage nach Lust und Laune:
<cite>Statt dem Salat passt auch ein [Kartoffelgratin](../gebutterter-kartoffelgratin/item.de.md) sehr gut zum Schnitzel.</cite>

## Anleitung

1. Die Kartoffeln am Vortag gar kochen.
2. Kartoffeln schälen und in nicht zu dünne Scheiben schneiden. Mit Öl, Essig, Gurkenwasser, Zucker, Salz und Pfeffer würzen. In der Zwischenzeit die Zwiebeln in Würfel oder Ringe schneiden und mit heißem Wasser blanchieren. Essiggurken in dünne Scheiben schneiden und mit den Zwiebeln zu den Kartoffeln geben. Den Salat mindestens 2 Stunden ziehen lassen und vor dem Servieren nach Belieben abschmecken.
3. Beim Einkaufen ist darauf zu achten, dass die Kalbsschnitzel zart rosa sind. Die Farbe ist ein Hinweis für die Frische und zeitgerechte Schlachtung. Ist das Kalb zu alt, wird das Fleisch zäh.
4. Die Schnitzel trocken tupfen und mit dem Fleischklopfer flach klopfen, damit es zarter wird. Im Anschluss salzen und pfeffern.
5. Die Eier in eine Schüssel schlagen, salzen, pfeffern und Zitronen-, oder Limettenschale hinein reiben. Alles gut vermischen und leicht schaumig schlagen.
6. Die Schnitzel zuerst ausgiebig in Mehl wenden, bis sie vollständig bedeckt sind. Hier sollte das Fleisch noch leicht durchschimmern. Anschließend ins Ei geben und zum Schluss in Semmelbrösel wenden. Eine gute Panade braucht seine Zeit.
7. Gebt ein großes Stück Butterschmalz oder Butter in die kalten Edelstahlpfannen. Beim Braten gilt der Grundsatz – mindestens 30% Fläche bleibt ohne Bratgut. Erhitzt die Pfannen auf niedriger Stufe bis der Inhalt geschmolzen ist und langsam anfängt zu knistern. Dreht nun auf mittlere Hitze und gebt nach kurzem Warten die Schnitzel hinzu. Das Bratfett darf nie vollständig aufgenommen werden, da das Bratgut sonst am Boden der Edelstahlpfanne festbraten wird. Wendet das Schnitzel nach ca. 5 Minuten Bratzeit – auch hier gilt ein weiterer Grundsatz des Bratens, es wird nur einmal gewendet. Lässt sich das Schnitzel nicht anheben, wartet noch kurz, sobald die Kruste braun geworden ist, lässt sich das Bratgut lösen. Hier ist ein wenig Fingerspitzengefühl und Erfahrung nötig. Wer sich unsicher ist, kann auch eine Teflonpfanne verwenden.

>Panade mal anders:
<cite>Zum Ei kann man auch geriebenen Parmesan oder als Semmelbröselersatz ungezuckerte, zerstoßene Cornflakes nehmen.</cite>

## Rezept Fotos

![Wiener Schnitzel bereits mit Semmelbrösel paniert in einer Edelstahlpfanne mit viel Butter. Das Schnitzel wird gerade auf mittlerer Temperatur herausgebraten.](wiener-schnitzel-mit-kartoffelsalat-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Nahaufnahme in Schwarz/Weiss. Wiener Schnitzel bereits mit Semmelbrösel paniert in einer Edelstahlpfanne mit viel Butter. Das Schnitzel wird gerade auf mittlerer Temperatur herausgebraten.](wiener-schnitzel-mit-kartoffelsalat-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Nahaufnahme vom Kartoffelsalat in einer kleinen Schüssel, garniert mit roten Zwiebeln und ein paar geschnittenen Essiggurken.](wiener-schnitzel-mit-kartoffelsalat-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Wiener Schnitzel gold gelb herausgebraten auf einem Teller serviert mit einer Limette angerichtet.](wiener-schnitzel-mit-kartoffelsalat-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Wiener Schnitzel gold gelb herausgebraten auf einem Teller serviert und mit dem Kartoffelsalat daneben, bereit zum Essen.](wiener-schnitzel-mit-kartoffelsalat-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Wiener Schnitzel mit Kartoffelsalat" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
