---
title: Bier-Barbecue-Sauce mit Schmackes
published: true
date: '11:34 06/03/2018'
metadata:
    og:title: 'Rezept: Bier-Barbecue-Sauce mit Schmackes'
    'og:image': https://schmankerl.space/de/blog/barbecuesauce/000-teaser-barbecue-sauce-bier-american-style.jpg
    'og:image:alt': 'Rezept Foto von der rauchigen Barbecue Sauce. Das Foto zeigt ein Schneidebrett und darauf eine Variaten aus den Gewürzen für die BBQ Soße. Im Hintergrund steht eine Flasche Schwarzbier die als Grundlage für die Sauce benötigt wird.'
    keywords: 'Barbecue, BBQ, Sauce, Dipp, Rippchen, Ketchup, feurig, Rezept'
    description: 'Bier-Barbecue-Sauce: Barbecue-Sauce ganz ohne Ketchup, dafür richtig feurig! Wer viel Zeit hat und auf puren Geschmack steht, ist bei diesem Rezept rauchig dabei. Schmeißt schon mal den Grill an.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Sauce
        - Dips
        - Grillen
    tag:
        - Steaks
        - Sauce
        - Grillen
        - Barbecue
        - Einwecken
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '10 Personen'
    -
        option: 'Zubereitung:'
        value: '5 Stunden 30 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Grill, Passierstab'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Bier-Barbecue-Sauce:'
        list:
            - '4 Dosen eingelegte Cherrytomaten'
            - '5 El Tomatenmark'
            - '3 mittelgroße Zwiebeln'
            - '2 Knoblauchzehen'
            - '2 Schwarzbier oder Guinness Ale'
            - '1 El Dijon Senf'
            - '8 El Honig'
            - '3 Tl Chipotle Chillipulver'
            - '2 Tl Rauchpaprikapulver'
            - '3 Tl Knoblauchpulver'
            - '4 Tl Zwiebelpulver'
            - '2 Tl Kreuzkümmel gemahlen'
            - '15 El Brauner Zucker'
            - '0,5 Liter Apfelessig'
            - '10 El Worchestersauce'
            - '3 El Olivenöl'
            - 'Liquid Smoke'
            - 'Salz & Pfeffer'
---

>Zum snacken, dippen und rubben:
<cite>Eine Barbecue-Sauce ganz ohne Ketchup, dafür richtig feurig!</cite>

## Anleitung

1. Zwiebeln und Knoblauch fein hacken und mit den 3 El Oliven Öl in einem großen Topf bei mittlerer Hitze anschwitzen.
2. Sobald Zwiebeln und Knoblauch eine goldene Farbe annehmen, das Tomatenmark und den braunen Zucker hinzugeben. Gut verrühren und leicht anbraten bzw. karamelisieren lassen.
3. Währenddessen die Gewürze in einer Schale vermengen und eine Minute unter ständigem Rühren mit in dem Topf anbraten.
4. Nun sind die Cherrytomaten an der Reihe. Die Dosen öffnen und samt Flüssigkeit hinzugeben.
5. 30 Minuten köcheln lassen und anschließend den Inhalt mit einem Passierstab superfein pürieren.
6. Die Worchestersauce, das Bier (100 ml Bier aufheben), den Honig und den Apfelessig hinzufügen.
7. 30 Minuten bei schwacher Hitze weiterköcheln lassen und dabei alle 10 Minuten umrühren.
8. Bei schwacher Hitze weitere 2 Stunden die Sauce köcheln lassen und dabei alle 20 Minuten umrühren. Der Inhalt sollte am Boden leicht ansetzen, ist das nicht der Fall, die Hitze geringfügig erhöhen.
9. Nach insgesamt 3 Stunden die Sauce, mit den bereits verwendeten Gewürzen und tröpfchenweise mit Liquid Smoke weiter abschmecken. Bei Bedarf auch ein wenig vom aufgehobenem Bier hinzufügen und weitere 1-2 Stunden köcheln lassen.
10. Ist die BBQ Sauce nach eurem Geschmack, muss sie einmal abkühlen und am besten über Nacht ruhen.
11. Am nächsten Tag die Sauce erwärmen und mit den Gewürzen und dem Liquid Smoke weiter abschmecken.

>Marinieren oder dippen
<cite>Am Grilltag muss die Sauce erneut aufgeheizt, abgekühlt und abgeschmeckt werden, danach ist sie als Marinade für z.B. Spare Ribs oder direkt zum Verzehr bereit.</cite>

## Rezept Fotos

![Rezept Foto BBQ Barbecue Bier Sauce. Die Zwiebel Stücke werden mit dem Tomatenmark und den Gewürzen in einem Topf scharf angebraten.](rezept-bbq-barbecue-sauce-ohne-ketchup-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto BBQ Barbecue Bier Sauce. Nachdem die Tomaten glasiert sind kommen die restlichen Gewürze dazu und die Paste wird angebruzzelt.](rezept-bbq-barbecue-sauce-ohne-ketchup-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto BBQ Barbecue Bier Sauce. Mit eingelegten Cherrytomaten wird die Gewürzpaste abgelöscht.](rezept-bbq-barbecue-sauce-ohne-ketchup-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto BBQ Barbecue Bier Sauce. Nach längerer Zeit des Köchels wurden die ganzen geschälten Tomaten zu einer homogenen Sauce.](rezept-bbq-barbecue-sauce-ohne-ketchup-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto BBQ Barbecue Bier Sauce. Die BBQ Sauce abgefüllt in einem kleinen Einweckglas mit Schildchen neben einem Blumengesteck.](rezept-bbq-barbecue-sauce-ohne-ketchup-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto BBQ Barbecue Bier Sauce. Übersicht aller verwendeter Zutaten. Insgesamt beläuft es sich auf über 20 Zutaten, die für diese wahnsinnig gute Barbecue Sauce gebraucht werden.](rezept-bbq-barbecue-sauce-ohne-ketchup-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto BBQ Barbecue Bier Sauce. Die Zwiebelchen werden in einem alten Gusseisernen Topf angeschwitzt.](rezept-bbq-barbecue-sauce-ohne-ketchup-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto BBQ Barbecue Bier Sauce. Die Gewürzmischung in einem kleinen Schälchen neben einem Schwarzbier gestellt.](rezept-bbq-barbecue-sauce-ohne-ketchup-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Bier-Barbecue-Sauce mit Schmackes" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
