---
title: Flammkuchen
published: true
date: '17:34 06/22/2022'
metadata:
    'og:title': 'Rezept: Elsässer Flammkuchen'
    'og:image': 'https://schmankerl.space/de/blog/flammkuchen/000-titelbild.jpg'
    'og:image:alt': 'Elsässer Flammkuchen'
    keywords: 'Rezept, Französisch, Speck, Creme Fraiche, Zwiebeln, Wein'
    description: 'Elsässer Flammkuchen'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Backen
    tag:
        - Teig
        - Französisch
description:
    -
        option: 'Schwierigkeitsgrad: mittel'
    -
        option: 'Reicht für:'
        value: '3-4 Personen'
    -
        option: 'Vorbereitung:'
        value: '2-3 Stunden'
    -
        option: 'Zubereitung:'
        value: '1 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Pizzaofen, Form, Pizzaschieber, Teigspachtel'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Flammkuchen Teig:'
        list:
            - '280g Weizenmehl'
            - '1/3 Hefewürfel'
            - '200 ml warmes Wasser'
            - '1 Tl Salz'
    -
        title: 'Belag:'
        list:
            - '4 mittelgroße Zwiebeln'
            - '1 Becher Creme Fraiche'
            - '2 Eigelb'
            - Salz
            - Pfeffer
            - Kümmel
            - '125g Speck'
---

> Unten knusprig und obend deftig cremig
<cite>Der Flammkuchen ist eine super Alternative zur Pizza und ist richtig deftig.</cite>

## Anleitung

1. Das lauwarme Wasser in eine Schüssel geben, die Hefe zerbröckeln und mit einem Löffel gut verrühren. Salz hinzugeben.
2. Das Mehl auf das Wasser sieben und anschließend mit dem Knethaken in der Küchenmaschine ca. 15 Minuten durchkneten.
3. Eine große Fläche dick mit Mehl bedecken und den gesamten Teig darauf stürzen.
4. Bedeckt den Teig mit Mehl und versucht ihn von unten anzuheben und werft ihn immer wieder auf die Mehldecke. Wenn der Teig nicht mehr an den Händen und an der Unterfläche kleben bleibt, versucht ihn nach und nach zu straffen, das sog. Ziehen und Falten (stretch and fold). Macht das solange bis der Teig langsam eine Form annimmt und kurz behält, wenn man ihn ablegt.
5. Schneidet den Teig in zwei Hälften, bemehlt die Fläche erneut und formt zwei gleichmäßige Kugeln. Wiederholt mit den kleineren Teigbällchen erneut das Ziehen und Falten. Geht mit den Händen unter den Teig und und zieht ihn nach hinten, damit die Oberfläche der Kugel gestrafft wird. Formt so schöne gleichmäßige Ballen.
6. Wenn ihr auf der Arbeitsfläche genug Platz habt, setzt die Teiglinge ca. 30-60 cm auseinander auf die stark bemehlte Fläche und setzt jeweils eine hohe Schüssel über die Teiglinge.
7. Lasst den Teig nun mindestens 1,5 Stunden bis 3 Stunden gehen und ruhen, bevor ihr ihn mit den Fingern austippelt.
8. Benutzt kein Nudelholz! Wie es der Name schon sagt, rollt man damit den Nudelteig aber keinen Flammkuchen oder Pizzateig. Wenn der Teig gut geruht hat, lässt er sich von innen nach außen mit den Fingern leicht ausdehnen. Steckt den Teig von innen nach außen und tippelt mit den Fingern die dickeren Stellen nach außen. Dreht dabei den Teig immer wieder und zieht ihn leicht mit den Handflächen nach außen. Ihr könnt den Teig auch zwei mal komplett wenden. Wenn ihr den Teig umdreht und wieder dehnt und tippelt verliert er weitere Spannung und er bleibt in einer runden Form.
9. Heizt den Backofen auf 260 Grad mindestens 20 Minuten zusammen mit einem Backblech auf.
10. Schneidet in der Zwischenzeit die Zwiebeln und den Speck in Würfelchen.
11. Bratet erst den Speck scharf an, bis er Farbe nimmt und gebt dann die Zwiebeln dazu bis sie glasig werden. Stellt das Gemenge zur Seite.
12. Rührt in einer Schüssel die Creme Fraiche und Eigelbe an und würzt sie mit Kümmel, Salz und Pfeffer.
13. Wenn der Ofen heiß genug ist und das Backblech auch ordentlich Hitze hat, könnt ihr die Flammkuchenteiglinge auf das Backpapier legen und mit dem Speck Zwiebel Topping bestreichen. Ich verwende einen Pizzaschieber um die Teiglinge von der bemehlten Fläche auf das Backpapier zu legen.
14. Holt das heiße Backblech vorsichtig aus dem Ofen und legt Backpapier und Teiglinge auf das Blech. Schiebt die Flammkuchen auf eine hohe Schiene, damit viel Hitze die Oberfläche schnell bräunt und das heiße Backblech tut dies gleichzeitig von unten für einen braunen Boden.
15. Backzeit variiert je nach Ofen, Aufheizphase und Teigdicke bei ca. 2-5 Minuten.

> Tipp
<cite>Ich mache den Flammkuchen im Holzofen bzw. in meinem Gasgrill, dort braucht der Flammkuchen ca. 2 Minuten bei 340-380 Grad ohne direkte Flamme.</cite>

## Rezept Fotos

![Rezept Foto Flammkuchen Teig Gärung](flammkuchen-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Flammkuchen Teig Kugeln](flammkuchen-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Flammkuchen Teig ausrollen](flammkuchen-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Flammkuchen Teig Belag](flammkuchen-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Flammkuchen Teig im Holzofen](flammkuchen-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Elsässer Flammkuchen" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
