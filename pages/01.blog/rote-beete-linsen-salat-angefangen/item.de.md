---
title: Rote Beete Linsensalat
published: false
date: '17:34 12/29/2020'
metadata:
    og:title: 'Rezept: Rote Beete Linsensalat'
    'og:image': https://schmankerl.space/de/blog/rote-beete-linsen-salat/000-titelbild.jpg
    'og:image:alt': 'Rezept Foto: Rote Beete Linsensalat'
    keywords: 'Rezept, gesund, Rote Beete, Salat, Vegan, Vegetarisch'
    description: 'Rote Beete Linsensalat: Ne Beilage die immer passt? Rote Beete und Linsen in einem leicht saueren Salat geht überall und auch ganz alleine.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Beilage
        - Vegetarisch
    tag:
        - Rote Beete
        - Salat
        - Linsen
description:
    -
        option: 'Schwierigkeitsgrad:'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: 'keine'
    -
        option: 'Zubereitung:'
        value: '2 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Kochtopf, Schüssel, Sparschäler, Sieb'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Roto Beete Linsen Salat:'
        list:
            - '400g frische Rote Beete'
            - '200g Rote Linsen'
            - '350ml Gemüsebrühe'
            - '1Tl Kreuzkümmel im Ganzen'
            - '1 1/2 Tl Currypulver'
            - '40g Ruccola'
            - '1Tl Dijon Senf'
            - '1El Honig'
            - '3El Olivenöl'
            - '4El Apfelbalsamico oder 3El Apfelessig und 1El Balsamico'
            - 'Petersilie'
            - 'Salz & Pfeffer'
---

> Nicht nur für Vegetarier
<cite>Der Rote Beete Linsensalat ist eine geile Beilage zu einem schönem Stück Steak!</cite>

## Anleitung

1. Die rote Beete schälen und in mundgerechte Würfel schneiden.
2. Wasser in einem Topf zum Kochen bringen und die rote Beete darin 40-45 Minuten gar kochen.
3. In der Zwischenzeit die Linsen waschen und in einem Topf mit Curry und Kreuzkümmel bei mittlerer Hitze für etwa 1 Minute anrösten.
4. Mit der Brühe ablöschen und 8-10 Minuten köcheln lassen. Die Flüssigkeit sollte vollständig aufgesogen werden und die Linsen noch leicht bissfest sein.
5. Für das Dressing in einer Schüssel Senf, Honig, Essig, Salz und Pfeffer vermischen. Der Honig löst sich immer gut, wenn man noch einen kleinen Schluck heißes Wasser zugibt. Zum Schluss das Olivenöl unterrühren.
6. Die Linsen zum Dressing geben.
7. Sind die roten Beete gar gekocht, das Wasser abgießen und die Beete unter die Linsen heben. Alles gut vermischen und abkühlen lassen.
8. Petersilie und Rucola waschen und grob hacken.
9. Vor dem Servieren den Salat nochmal abschmecken und Petersilie und Rucola unterheben.

## Rezept Fotos

![Rezept Foto xxx](xxx.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Rote Beete Linsensalat" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
