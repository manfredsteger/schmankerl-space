---
title: Pfannkuchen
published: true
date: '17:34 08/10/2022'
metadata:
    og:title: 'Rezept: Pfannkuchen'
    'og:image': https://schmankerl.space/de/blog/pfannkuchen/000-titelbild.jpg
    'og:image:alt': 'Pfannkuchen auf dem Teller mit Schokosauce.'
    keywords: 'Rezept, klassisch, Teig, Eier, Pfannkuchen, Rührkuchen, Kinderessen'
    description: 'Pfannkuchen: Ganz einfach aber einen schönen Teig zu bekommen ist nicht leicht. Ein Gericht für Kinder und auch für die ganz großen Kinder ein Genuss.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Mehlspeise
    tag:
        - Pfannkuchen
        - Schokosauce
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '3 Personen'
    -
        option: 'Vorbereitung:'
        value: '30 Minuten'
    -
        option: 'Zubereitung:'
        value: '45 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Ofen, beschichtete Pfanne'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Pfannkuchen:'
        list:
            - '100g Weizenmehl 550'
            - '130g Wiener Grießerl'
            - '20g Dinkel- oder Roggenmehl'
            - '500ml Milch'
            - '6 Eier'
            - '1 1/2 El Zucker'
            - '2 Päckchen Vanillinzucker'
            - 'Butterschmalz'
            - 'Butter'
            - 'Salz'
---

>Hits für Kids
<cite>Wer gerne Mehlspeisen mag, wird ihn lieben. Unsere kleine Leni isst den Pfannkuchen morgens, mittags und abends gerne.</cite>

## Anleitung

1. Schlagt die Eier in eine Rührschüssel und gebt die 1 1/2 El Zucker und die zwei Päckchen Vanillinzucker und ne Prise Salz hinzu.
2. Schlagt die Eier und Zucker zu einer leicht schaumigen Masse. Aber nicht zu viel, ansonsten wird der Teig später nicht homogen.
3. Fügt unter ständigem, langsamen Rühren die Mehlsorten hinzu. Wir verwenden den Schneebesen der Küchenmaschine dafür.
4. Die Masse kurz kräftig aufrühren lassen und ca. 5 Minuten quellen lassen. So verbindet sich die Masse sehr gut und wird schön gleichmäßig. Nur nicht zu lange rühren, sonst wir das Ergebnis eher ein Biskuitt als ein Pfannkuchen.
5. Ein wenig Butterschmalz und Butter in einer beschichteten Pfanne auf mittlerer Stufe erhitzen. Dann ca. 1-1,5 Suppenkellen hinzugeben und gleichmäßig verteilen.
6. Backofen vorheizen
7. Die Pfannkuchen auf beiden Seiten schön anbräunen und mit Küchenpapier abtupfen und auf einem Teller in den Ofen stellen.
8. Schichtet die Pfannkuchen im Ofen mit jeweils einem Küchenpapier dazwischen, dann nehmt ihr noch überschüssiges Fett vom Pfannkuchen.

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Pfannkuchen" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
