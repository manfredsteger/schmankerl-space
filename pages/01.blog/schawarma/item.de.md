---
title: Hähnchen Schawarma Wrap
published: true
date: '17:34 05/08/2024'
metadata:
    og:title: 'Rezept: Hähnchen Schawarma Wrap mit Tahin Joghurt Sauce'
    'og:image': https://schmankerl.space/de/blog/schawarma/000-titelbild.jpg
    'og:image:alt': 'Hähnchen Schawarma mit Joghurt Tahinsauce. Das Foto zeigt einen Wrap mit Hähnchen Schawarma Streifen und viele frische Zutaten wie Salat, Zwiebeln, Gurken und anderem Gartengemüse.'
    keywords: 'Rezept, arabisch, Tahin, Hähnchen, Gesund, Gemüse, Salat'
    description: 'Ein leckeres, gesundes und vor allem kalorienarmes Essen das schnell zubereitet, abgeändert und angepasst werden kann. Viele frische Zutaten und eine leckere Joghurt-Tahin-Sauce mit Granatapfel Kerne machen das Gericht besonders lecker.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Wrap
    tag:
        - Wrap
        - Hähnchen
        - Gesund
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: '8 Stunden'
    -
        option: 'Zubereitung:'
        value: '45 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Pfanne'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Hähnchen Schawarma:'
        list:
            - 'Hähnchenbrust 600g'
            - '6 EL Speiseöl'
            - '2 Knoblauchzehen'
            - '1 EL Apfelessig'
            - '1/2 Zitrone ausgepresst'
            - '1/2 TL Nelken fein zerstoßen'
            - '1 TL Kreuzkümmel im Ganzen'
            - '1/2 TL Kardamompulver'
            - '1/2 TL Zimt gemahlen'
            - '1/2 TL Muskatnuss gemahlen'
            - '1/2 TL Paprik süß'
            - '1/2 TL Paprika rauchig oder Chipotle Chili'
            - '1 TL Harissa Pulver'
            - '2 TL Salz'
    -
        title: 'Wrap:'
        list:
            - '30 cm Wraps oder Dürum Fladen'
            - '200g Hummus'
            - '15g Tahin Sesam Paste'
            - '5 EL Joghurt'
            - 'Kopfsalat Blätter'
            - 'Gurke'
            - 'Granatapfel Kerne'
            - '1 Karotte geraspelt'
            - 'Blaukraut/Rotkohl Kopf'
            - 'Kohlrabi'
            - 'Aubergine Scheiben gegrillt (optional)'
            - 'Sesam geröstet'
            - '1 EL Apfelessig'
            - '2 EL Olivenöl'
            - 'Salz & Pfeffer'
---

<cite>Etwas auf die Linie achten? Perfektes Gericht! Wenig Kohlehydrate, viel Frisches und Gesundes und traumhaft abwandelbar, damit man diesen Wrap auch mehrere Tage hintereinander essen kann.</cite>

## Anleitung

1. Am Vortag wird die Marinade vorbereitet und das Hähnchen in dünne, lange Streifen geschnitten.
2. Bereitet in einer großen Schüssel die Gewürze vor: 1/2 TL Nelken fein zerstoßen, 1 TL Kreuzkümmel ganz, 1/2 TL gemahlenes Kardamom, 1/2 TL gemahlener Zimt, 1/2 TL gemahlene Muskatnuss, 1/2 TL süßes Paprikapulver, 1/2 TL geräuchertes Paprikapulver oder Chipotle-Chilipulver, 1 TL Harissapulver und 2 TL Salz.
3. Fügt nun der Gewürzmischung 6 EL Speiseöl, 1 EL Apfelessig und den ausgepressten Zitronensaft hinzu und verrührt alles gründlich, bis keine Klümpchen mehr vorhanden sind.
4. Hackt die Knoblauchzehen fein oder drückt sie durch eine Knoblauchpresse.
5. Fügt den frischen Knoblauch und die Hähnchenbruststreifen zu der Gewürzpaste hinzu und vermengt alles sehr gut. Bedeckt die Schüssel mit einem Deckel oder Frischhaltefolie und stellt sie mindestens 12 Stunden in den Kühlschrank.
6. Holt die Hähnchenstreifen ca. 1 Stunde vor der Zubereitung aus dem Kühlschrank und lasst sie Raumtemperatur annehmen. Das marinierte Hähnchen kann mehrere Tage im Kühlschrank aufbewahrt werden. Ich nehme immer nur so viel heraus, wie ich gerade brauche, und bewahre den Rest für die folgenden Tage auf. Es ist auch vakuumiert bis zu einer Woche haltbar im Kühlschrank und kann eingefroren werden; dabei bleibt es bis zu einem Jahr frisch.
7. Bereitet in der Zwischenzeit die frischen Zutaten vor. Reißt den Kopfsalat in grobe Stücke und waswascht ihn, schneidet die Gurke in kleine Stücke, hobelt das restliche Gemüse fein und entkernt einen Teil des Granatapfels.
8. Gebt alle Gemüsesorten in eine ausreichend große Schüssel, fügt 1 EL Apfelessig, 2 EL Olivenöl, gerösteten Sesam, Granatapfelkerne sowie etwas Salz und Pfeffer hinzu. Zum Schluss gebt 5 EL Joghurt und einen guten Schuss Tahini dazu. Vermengt alles gut zu einem festen Salat mit Dressing. Das erleichtert das Zusammenlegen des Wraps.
9. Lass den Salat kurz marinieren oder, wenn es eilig ist, in der Zwischenzeit das Hähnchen in einer Pfanne scharf von allen Seiten anbraten. Achte darauf, die Pfanne nicht zu heiß zu machen, da die Marinade sonst regelrecht verbrennt. Ich persönlich mag Röstaromen, aber bei diesem Gericht sind sie nicht so wichtig wie die Süße und die Nuancen von Säure. Daher würde ich empfehlen, das Hähnchen etwas weniger braun anzubraten, um es gesünder und schmackhafter zu machen
10. Lege nach dem Anbraten einen Wrap aus, bestreiche ihn mit Hummus und lege die marinierte Salatmischung in das untere Drittel. Lege die Hähnchenstreifen oben auf. Jetzt wird es ein wenig fummelig, und ab und zu kann es auch zu einer kleinen Sauerei kommen. Tipp: Lege unter das untere Drittel des Wraps etwas Alufolie und lasse etwa 5-7 cm davon unten herausschauen, dann bleiben die Hände später sauber. Drücke das Hähnchen- und Salatgemisch fest mit beiden Händen und stülpe das untere Drittel des Wraps fest darüber. Drehe dann den Wrap vorsichtig und falte dabei entweder das linke oder rechte Ende des Wraps ein, damit er an einer Seite geschlossen ist. Drehe den Wrap ein Stückchen weiter und schließe ihn ein, indem du die obere Seite einschlägst. Wenn du eine Alufolie daruntergelegt hast, kannst du den Wrap nun von beiden Seiten damit einrollen, und der Wrap hält zusammen.

>Lecker Schmecker Tipp:
<cite>Macht den Hummus selbst oder holt ihn vom Araber/Syrer eures Vertrauens! Supermarkt Hummus ist meiner Meinung nach ungenießbar. Wenn ihr richtig Lust zu Kochen habt, macht ihn mit diesem Rezept selbst [Rezept: Hummus](https://schmankerl.space/de/blog/hummus) und ihr habt ein 300% Upgrade.</cite>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Hähnchen Schawarma Wrap mit Tahin Joghurt Sauce" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
