---
title: Lasagne
published: true
date: '17:34 04/26/2018'
metadata:
    og:title: 'Rezept: Lasagne'
    'og:image': https://schmankerl.space/de/blog/lasagne/000-teaser-lasagne-teller-angerichtet-rezept-bolognese.jpg
    'og:image:alt': 'Rezept Foto der italienischen Lasagne. Das Foto zeigt die dampfende frisch aus der Form gestochene Lasagne. Angerichtet auf einem eierschalen farbenen runden Teller.'
    keywords: 'Lasagne, italienisch, Kochschinken, Sahne, Schmand, Hackfleisch, Kinderessen, Rezept'
    description: 'Lasagne: Die Lasagne gehört schön fast zum bayerischen Kulturgut! Ein Gericht für Kinder und auch für die ganz großen Kinder ein Genuss.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Pasta
    tag:
        - Lasagne
        - Hits4Kids
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Zubereitung:'
        value: '2,5 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Auflaufform, Pfanne'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Lasagne:'
        list:
            - 'Lasagne Platten'
            - '3-4 El Tomatenmark'
            - '100g Kochschinken'
            - '1 Tl italienische Kräuter'
            - '150ml Gemüsebrühe'
            - '80g Erbsen (TK)'
            - '2 mittelgroße Zwiebel'
            - '1 Knoblauch'
            - '1 große Karotten'
            - '500g Hackfleisch gemischt'
            - '1 Tl Chillipulver oder 1/2 frische Chilli'
            - '200ml Sahne'
            - '100ml Schmand'
            - '1/2 El Mehl (optional)'
            - '50g alter Gauda'
            - '50g Parmesan'
            - '100g Mozzarella'
            - '3 Tl Paprikapulver edelsüß'
            - '2 El Pflanzenöl'
            - 'Salz & Pfeffer'

---

>Bella Bavaria.
<cite>Ähnlich wie Spaghetti Bolognese, gehört auch die Lasagne schon fast zum bayrischen Kulturgut. Hier macht sich die geografische Nähe zu Italien stark bemerkbar.</cite>

## Anleitung

1. Das Essen sollte etwa 1 Stunde bevor es in den Ofen kommt zubereitet werden. So können die rohen Lasagneplatten noch gut Flüssigkeit aufnehmen.
2. Karotte, Zwiebeln, Knoblauch und Schinken in kleine Würfel schneiden. Falls man eine frische Chilli zur Verfügung hat, diese klein hacken.
3. Käsesorten fein reiben und mischen.
4. Das Hackfleisch in Pflanzenöl scharf anbraten, bis es eine dunkle Farbe annimmt. Beinahe zeitgleich den Schinken mit in die Pfanne, um ihn kross zu braten. Die Hitze leicht reduzieren, Hack und Schinken beiseite schieben und Zwiebel, Knoblauch und Chilli mit in der Pfanne anbraten. Die Masse ebenfalls zur Seite schieben und das Tomatenmark anrösten, das gibt ein gutes Aroma. Karotten und Erbsen dazugeben und kurz mit anbraten. Anschließend mit Sahne und Gemüsebrühe ablöschen und ca. 5 Minuten köcheln lassen, bis eine homogene Masse entstanden ist. Sie sollte nicht zu flüssig, aber auch nicht zu fest sein, damit die Lasagneplatten noch Flüssigkeit aufsaugen können. Sollte es zu flüssig sein, kann man die Masse gut mit etwa 1/2 El Mehl bestäuben, das bindet gut. Mit Salz & Pfeffer, den italienischen Kräutern und Paprikapulver abschmecken.
5. Die Hitze abschalten und nach etwa 10 Minuten Abkühlzeit den Schmand unterrühren.
6. Die Auflaufform zuerst mit einer dünnen Schicht Hackfleisch füllen, darauf die Lasagneplatten verteilen und hierauf wiederum eine dünne Lage Hack auftragen, die mit Käse bestreut wird. Den Vorgang so lange wiederholen, bis die  Auflaufform gefüllt ist. Die oberste Schicht wird abgeschlossen mit einer Lage Hackfleisch und Käse.
7. 45 Min bei 180 Grad im Ofen backen.
8. Zum Schluß noch einmal den Grill hinzuschalten und dabei zugucken wie das gold rote Glück langsam knusprig braun wird.

>Gibts auch aus dem Tiefkühlschrank...nur selbst gemacht.
<cite>Die Lasagne kann ohne Probleme zubereitet werden und anstatt in den Ofen, wandert die Auflaufform in den Tiefkühler. Einfach am Vortag über Nacht im Kühlschrank auftauen lassen und dann ab in den Ofen.</cite>

## Rezept Fotos

![Rezept Foto Lasagne selber machen. Schneidebrett auf dem die Zwiebeln für die Lasagne fein gehackt wurden.](rezept-lasagne-hackfleisch-italienisch-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Frischer Schinken vom Metzger, klein geschnitten mit einem scharfen Ausbeinmesser.](rezept-lasagne-hackfleisch-italienisch-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. In einer großen Edelstahlpfanne werden erst das Hackfleisch und dann der Schinken angebräunt.](rezept-lasagne-hackfleisch-italienisch-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Die Lasagne Platten werden in die Backofenform gelegt.](rezept-lasagne-hackfleisch-italienisch-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Die Hackfleischmasse wird kurz vor dem fertigen Braten mit den Gewürzen abgeschmeckt und noch kurz weiter angebraten.](rezept-lasagne-hackfleisch-italienisch-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Die Hackfleischmasse wird am Schluß noch in der Pfanne mit Mehl bestäubt, damit die Masse später sämiger wird und nicht zu sehr wassert.](rezept-lasagne-hackfleisch-italienisch-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Das Hackfleisch wird gerade mit Sahne und Schmand abgelöscht und behutsam umgerührt.](rezept-lasagne-hackfleisch-italienisch-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Katharina legt auf die Lasagne Platten jeweils eine Schicht Hachfleisch mit Soße und danach Käse auf.](rezept-lasagne-hackfleisch-italienisch-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Mama Siglinde legt eine neue Schicht Lasagne Nudeln auf die Hackfleisch Schicht der Lasagne auf.](rezept-lasagne-hackfleisch-italienisch-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Schwarz Weiss Foto der Lasagne Form und der Edestahlpfanne mit dem Brät.](rezept-lasagne-hackfleisch-italienisch-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Einsicht in den Ofen, in dem die Lasagne langsam gold braun wird.](rezept-lasagne-hackfleisch-italienisch-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Frischer Gartensalat ist eine tolle Beilage zur Lasagne. Frische Garten Gurken und leckerer Feldsalat sind auf einem runden Teller mit Goldrand garniert worden.](rezept-lasagne-hackfleisch-italienisch-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Draufsicht auf den frischen Gartensalat. Frische Garten Gurken und leckerer Feldsalat sind auf einem runden Teller mit Goldrand garniert.](rezept-lasagne-hackfleisch-italienisch-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Drei Teller mit Gartensalat stehen für das anstehende Familientreffen bereit.](rezept-lasagne-hackfleisch-italienisch-014.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Gartensalt angerichtet auf dem Familientisch.](rezept-lasagne-hackfleisch-italienisch-015.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Lasagne selber machen. Detailaufnahme der Lasagne auf einem runden china bone Teller.](rezept-lasagne-hackfleisch-italienisch-016.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Lasagne Mama" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
