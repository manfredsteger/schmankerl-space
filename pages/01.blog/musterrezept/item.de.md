---
title: Musterrezept
published: false
date: '17:34 03/23/2018'
metadata:
    og:title: 'Rezept: Musterrezept'
    'og:image': https://schmankerl.space/de/blog/musterrezept/000-titelbild.jpg
    'og:image:alt': 'xxx'
    keywords: 'Rezept, italienisch, Kochschinken, Sahne, Schmand, Hackfleisch, Kinderessen'
    description: 'Lasagne: Die Lasagne gehört schön fast zum bayerischen Kulturgut! Ein Gericht für Kinder und auch für die ganz großen Kinder ein Genuss.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Steaks
    tag:
        - Steaks
        - Bohnen
description:
    -
        option: 'Schwierigkeitsgrad:'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: '2 Tage'
    -
        option: 'Zubereitung:'
        value: '1 Stunde 30 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Edelstahlpfanne, ...'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Steaks:'
        list:
            - 'Rinderfilet 600g'
            - 'xxx'
    -
        title: 'Nudelsalat:'
        list:
            - '500g Penne'
            - '250g Mozzarella'
            - '160g Parmaschinken'
            - '50g Pinienkerne'
            - '50g Cashewkerne'
            - '1 Bund Rucola'
            - '1 Knoblauchzehe'
            - '60g Parmesan'
            - '8 EL Balsamico'
            - 'Balsamico Creme'
            - '10 EL Olivenöl'
            - '1 TL scharfer Senf'
            - '1 TL Honig'
            - '1 TL rotes Pesto (optional)'
            - 'Salz & Pfeffer'
---

>Geschichte zum Rezept.
<cite>Der Barsch ...</cite>

## Anleitung

1. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
2. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
3. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.
4. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.

>Geschichte zum Rezept.
<cite>Der Barsch ...</cite>

## Rezept Fotos

![Rezept Foto xxx](xxx.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Wiener Schnitzel mit Kartoffelsalat" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
