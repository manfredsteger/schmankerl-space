---
title: Enchilladas Tex-Mex
published: true
date: '08:34 07/17/2018'
metadata:
    og:title: 'Rezept: Enchilladas Tex-Mex'
    'og:image': https://schmankerl.space/de/blog/enchilladas/000-teaser-enchilladas-ofen-gratin-rezept.jpg
    'og:image:alt': 'Rezept Foto von Enchilladas. Das Foto zeigt einen Blick in den Backofen. Die Enchilladas sind in der Edelstahlform fast fertig gebacken. Gold gelb färbt sich der Käse. Gleich ist es soweit.'
    keywords: 'Enchillada, Tex-Mex, Mexikanisch, Mexico, Liquid Smoke, Grillen, Feurig, Scharf, Rezept'
    description: 'Enchilladas Tex-Mex: Scharf und rauchig lecker, entweder im Ofen oder auf dem Grill! Ein tolles Gericht für lauwarme Sommerabende oder das Grillfest.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Mexikanisch
        - Fleisch
    tag:
        - Tomatensauce
        - Scharf
        - Feurig
        - Hackfleisch
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Zubereitung:'
        value: '2 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Auflaufform, Topf, Herd, Ofen, Grill, Käsereibe'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Enchilladas:'
        list:
            - '10 Maismehltortillas'
            - '500g Rinderhack'
            - '2 frische Chilis'
            - '2 Knoblauchzehen'
            - '2 rote Pakrika'
            - '2 große Zwiebel'
            - '500g schwarze Bohnen'
            - '100g alter Gouda'
            - '100g mittelalter Gouda'
            - '50g Mozzarella'
            - '50g Parmesan'
            - 'Liquid Smoke'
            - 'Olivenöl'
            - 'Salz & Pfeffer'
    -
        title: 'Tomatensauce:'
        list:
          - '2 Dosen eingelegte Cherrytomaten'
          - '2 El Mehl'
          - '1 Tl Paprikapulver'
          - '1 Tl Knoblauchpulver'
          - '1/2 Tl Kreuzkümmelpulver'
          - '1 Tl getrockneter Oregano'
          - '1 Tl Salz'
          - '1 Tl Pfeffer'
          - '1/2 Tl Cayenne'
          - '5 Tl brauner Zucker'
---

>Lasagne auf mexikanischer Art
<cite>Feurig scharf und rauchig lecker, im Ofen oder auf dem Grill!</cite>

## Anleitung

1. Für die Tomatensauce 2 El Öl in einem Topf leicht erhitzen und die 2 El Mehl darin anschwitzen.
2. 1 Tl Paprikapulver, 1 Tl Knoblauchpulver, 1/2 Tl Kreuzkümmel, 1 Tl Oregano, 1 Tl Salz & Pfeffer, 1/2 Tl Cayenne und die 5 Tl brauner Zucker zugeben und kurz mit anbraten.
3. Die eingeleten Cherrytomaten hinzugeben und ca. 30 Minuten bei geringer Hitze köcheln lassen. Währenddessen kann die Tex-Mex-Hackfleisch Füllung zubereitet werden.
4. Die Zwiebeln und die Paprikas in kleine Stücke hacken. Den Knoblauch und die Chillischote sehr fein hacken.
5. Die Pfanne oder besser den Grill mit Grillpfanne kräftig aufheizen und das Hackfleisch als Rolle im Ganzen auf allen Seiten scharf anbraten. Das Bratgut nicht voreilig zerteilen, da sonst zu viel Wasser den Röstvorgang unterbindet und das Fleisch mehr kocht als dass es anbrät.
6. Währenddessen die Käsesorten reiben und in einer Schüssel kühl stellen.
7. Ist das Hackfleisch rundum kross angebraten, flach drücken und 1-2 Minuten weiter anbraten. Anschließend in mehrere Stücke zerteilen, auf die rohe Seite wenden und weitere 1-2 Minuten anbraten. Das Fleisch nun auf eine Seite der Pfanne schieben und auf die andere Seite die Zwiebeln ausbreiten und anbraten, bis sie gold braun sind.
8. Sind die Zwiebeln und das Fleisch rösch angebraten, werden beide Lager miteinander vermengt. Das Hackfleisch beim Vermengen zerstoßen und weiter anrösten lassen. Nach weiteren 1-2 Minuten die Paprikastückchen und die gehackte Chillischote hinzugeben.
9. Zum Schluß mit Salz & Pfeffer sowie wenige Tropfen Liquid Smoke abschmecken.
10. Zum Befüllen die Tortillas auf einem Teller auslegen, mit der Tomatensauce bestreichen, Tex-Mex, schwarze Bohnen, und ein wenig Käse geben. Anschließend zu einer festen Rolle drehen und in eine Auflaufform geben.
11. Die übrige Tomatensauce über den Enchilladas verteilen und mit Käse bestreuen.
12. Bei 200 Grad etwa 35 Minuten im Ofen backen.

## Rezept Fotos

![Rezept Foto Mexikansiche Enchillada. Alle Zutaten liegen vor dem Grill bereit. Das frische Hackfleisch wird gleich kross gegrillt.](rezept-mexikanisch-enchilladas-grill-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Enchillada. Erst werden die Zwiebel und die Chillis auf der Grillpfanne auf dem Gasgrill angebruzzelt.](rezept-mexikanisch-enchilladas-grill-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Enchillada. Das Hackfleisch kommt im Ganzen als Braten geformt in die Grillpfanne, damit es rundherum schön braun angegrillt werden kann.](rezept-mexikanisch-enchilladas-grill-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Enchillada. Das scharf angegrillte Hackfleisch ist schön braun geworden, nun kommen die zerkleinerten Paprikas dazu.](rezept-mexikanisch-enchilladas-grill-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Enchillada. Das fertige Hackfleisch wird in einem Topf beiseite gestellt.](rezept-mexikanisch-enchilladas-grill-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Enchillada. Hackfleisch und Soße stehen bereit um in die Toritallas gefüllt zu werden.](rezept-mexikanisch-enchilladas-grill-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Enchillada. Katharina streicht die Tomatensauce hauch dünn auf die Maismehltortillas.](rezept-mexikanisch-enchilladas-grill-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Enchillada. Auf die bestrichenen Maismehltortillas kommt nun eine Reihe gegrilltes Hackfleisch, schwarze Bohnen und etwas Käse.](rezept-mexikanisch-enchilladas-grill-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Enchillada. Katharina streut den Käse langsam über die Tortillas.](rezept-mexikanisch-enchilladas-grill-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Enchillada. Die Enchilladas werden gerollt und nebeneinander in einen Bräter gelegt und mit der restlichen Tomatensauce übergossen. Am Schluss nicht den Käse zum Gratinieren vergessen.](rezept-mexikanisch-enchilladas-grill-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Enchillada. Ein Blick in den Ofen, die Enchilladas werden gerade gold gelb gebacken.](rezept-mexikanisch-enchilladas-grill-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Enchillada. Die Enchilladas angerichtet auf einem china bone Teller mit einem Klecks Tomatensauce.](rezept-mexikanisch-enchilladas-grill-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Enchilladas Tex-Mex" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
