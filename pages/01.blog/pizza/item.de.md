---
title: Grill- und Ofenpizza
published: true
date: '17:34 05/16/2018'
metadata:
    og:title: 'Rezept: Grill- und Ofenpizza'
    'og:image': https://schmankerl.space/de/blog/pizza/000-teaser-grillpizza-pizzateig-selbst-gemacht-rezept.jpg
    'og:image:alt': 'Rezept Foto vom Grillpizza und Ofenpizza. Das Foto zeigt die frisch belegten Pizzen bevor sie gleich in den Ofen kommen.'
    keywords: 'Pizza, Ofenpizza, Grillpizza, Steinofen, Steinofenpizza, Teig, Pizzateig, Tomatensauce, Rezept'
    description: 'Grill- und Ofenpizza: Außen knusprig, mittig pikant, oben frisch belegt und jeder wie es mag. Genial für einen ausgiebigen Kochabend.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Pizza
        - Italien
    tag:
        - Pizza
        - Hits4Kids
        - italienisch
        - Original
description:
    -
        option: 'Schwierigkeitsgrad: mittel'
    -
        option: 'Reicht für:'
        value: '7 Personen'
    -
        option: 'Vorbereitung:'
        value: '24 Stunden'
    -
        option: 'Zubereitung:'
        value: '1 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Grill, Pizzastein, Ofen, Backpapier, Küchenmaschine, Nudelholz, Knethaken, Frischhaltefolie, Passierstab'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Pizzateig:'
        list:
            - '500g Mehl (Pizzamehl Type 00)'
            - '200g Vollkornmehl'
            - '75g Hartweizengrieß'
            - '390ml Wasser'
            - '7g Bierhefe / 1 Würfel frische Hefe'
            - '1 El Meersalz'
            - '1 Tl Zucker'
    -
        title: 'Tomatensauce:'
        list:
            - '1 Dose eingelegte Cherrytomaten'
            - '2 El Tomatenmark'
            - '1 kleine Zwiebel'
            - '1/2 Knoblauchzehe'
            - 'Prise Zucker'
            - 'Salz&Pfeffer'
            - '1 Tl Oregano (getrocknet, gerebelt)'
            - '1/2 Tl Thymian (getrocknet)'
            - '1 El Basamico Essig (dunkel)'
            - 'Olivenöl'
---

>Pizza mal anders:
<cite>Unten knusprig, mittig pikant, oben frisch belegt und jeder wie es mag.</cite>

## Anleitung

1. Das praktische an diesem Pizzateigrezept ist, dass er schon einen Tag zuvor zubereitet werden kann. Löst die Hefe in lauwarmen Wasser auf. Mischt in einer großen Schüssel das Mehl mit Grieß und Salz. Das Mehlgemisch mit einem Löffel vermengen und in der Mitte eine Mulde bilden. Hierein das _Hefewasser_ schütten und 10 Minuten quellen lassen. Anschließend kann der Teig für gute 10-15 Minuten geknetet werden, je länger, desto besser für den Teig. Während des Knetens sollte man immer wieder die Konsistenz begutachten und gegebenenfalls etwas Wasser nachgießen. Der Teig muss homogen, fest sein und darf nicht kleben bleiben.
2. Nach dem Kneten die Schüssel mit Frischhaltefolie abdecken, ein feuchtes Tuch drüber legen und an einem kalten Ort, wie einem Keller oder Kühlschrank für mindestens 8 Stunden, besser über Nacht gehen lassen.
3. Den Teig aus der Schüssel nehmen und je nach Wunschgröße portionieren.
4. Vor dem Ausrollen sollte der Teig etwa 2 Stunden bei Zimmertemperatur aklimatisieren und noch etwas gehen. Formt dafür Kugeln, legt diese auf ein Holzbrett und bedeckt die Teiglinge mit einem feuchten Tuch.
5. Währenddessen könnt ihr für die Tomatensauce Zwiebel und Knoblauch klein hacken, in Olivenöl anbraten und sobald sie eine goldgelbe Farbe annehmen, mit der Prise Zucker karamelisieren lassen. Das Tomatenmark zufügen und kurz mit anbraten. Anschließend mit den Tomaten aus der Dose inkl. Tomatensaft ablöschen, aufköcheln lassen und mit dem Passierstab fein mixen.
6. Nun können die Gewürze (Oregano, Thymian, Salz&Pfeffer, Balsamico) hinzugegeben werden. Die Sauce etwa eine halbe Stunde bei geringer Hitze köcheln lassen.
7. Möchte man die Pizzen auf dem Grill zubereiten, muss der Pizzastein ca. eine halbe Stunde bei höchster Temperatur vorgeheizt werden. Für den Ofen gilt gleiches.
8. Die ausgerollten Pizzen mit der Tomatensauce bestreichen, Käse darüberstreuen und ab auf den Grill oder in den Ofen.

>Tip zum Rezept:
<cite>Wir grillen die Pizzen wie beschrieben erst fertig und belegen sie anschließend mit frischen Zutaten, wie z.B.: Parma-, Serranoschinken, Rucola, Feigen, Balsamicocreme.</cite>

## Rezept Fotos

![Rezept Foto Grillpizza und Ofenpizza Italien. Ansicht der Zutaten für die Grillpizza.](rezept-grill-und-ofenpizza-handgemacht-italien-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Mehl in einer Rührschüssel auf der Kitechen Aid Küchenmaschine.](rezept-grill-und-ofenpizza-handgemacht-italien-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Das Wasser wird zum Mehl in die Rührschüssel gekippt.](rezept-grill-und-ofenpizza-handgemacht-italien-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Das Öl wird zum Mehl und dem Wasser in die Rührschüssel gegeben.](rezept-grill-und-ofenpizza-handgemacht-italien-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Schneidebrett mit den gewürfelten Zwiebeln und im Hintergrund stehen die eingelegten Cherrytomaten.](rezept-grill-und-ofenpizza-handgemacht-italien-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Die Zwiebeln schwitzen im Olivenöl auf dem Herd langsam an.](rezept-grill-und-ofenpizza-handgemacht-italien-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Das Tomatenmark wurde zu den glasierten Zwiebeln in den Topf gegeben.](rezept-grill-und-ofenpizza-handgemacht-italien-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Schwarz Weiß Foto vom Tomatenmark und den glasierten Zwiebeln im Topf.](rezept-grill-und-ofenpizza-handgemacht-italien-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Nun kommen die Cherrytomaten zu den angeschwitzen Zwiebeln in den Topf.](rezept-grill-und-ofenpizza-handgemacht-italien-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Die Gewürze wie Salz und Pfeffer kommen zum Sugo in den Topf.](rezept-grill-und-ofenpizza-handgemacht-italien-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Katharina bestreut das Küchenbrett für das Ausrollen des Teigs mit Mehl.](rezept-grill-und-ofenpizza-handgemacht-italien-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Der Teig gehrt in der Rührschüssel an einem warmen Ort vor sich hin.](rezept-grill-und-ofenpizza-handgemacht-italien-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Katharina knetet und formt den Teig mit Ihren Händen.](rezept-grill-und-ofenpizza-handgemacht-italien-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Katharina drückt den Teig mit Ihren Händen vorsichtig zusammen.](rezept-grill-und-ofenpizza-handgemacht-italien-014.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Katharina rollt den Teig zum Schluss auf dem Schneidebrett aus.](rezept-grill-und-ofenpizza-handgemacht-italien-015.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Katharina rollt den Teig zum Schluss auf dem Schneidebrett aus. Der Teig ist lange und dünn.](rezept-grill-und-ofenpizza-handgemacht-italien-016.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Das Grillgemüse wurde in einer Schüssel bereitgestellt und mit Öl beträufelt.](rezept-grill-und-ofenpizza-handgemacht-italien-017.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Das Grillgemüse liegt nun auf dem Grill und wird scharf angebraten.](rezept-grill-und-ofenpizza-handgemacht-italien-018.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Die Pizzas werden nun nach belieben belegt.](rezept-grill-und-ofenpizza-handgemacht-italien-019.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Vogelperspektive über die belegten Pizzas und Zutaten.](rezept-grill-und-ofenpizza-handgemacht-italien-020.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Der Esstisch ist voll mit belegten Pizzas und vielen verschiedenen Zutaten.](rezept-grill-und-ofenpizza-handgemacht-italien-021.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Frischer Ruccola wird gewachen und kommt nach dem Backen auf die Pizzen.](rezept-grill-und-ofenpizza-handgemacht-italien-022.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Frischer Gartensalat wird in einer Schüssel bereitgestellt.](rezept-grill-und-ofenpizza-handgemacht-italien-023.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Der Parmaschinken wird bereitgelegt und kommt nachdem Backen auf die Pizzen.](rezept-grill-und-ofenpizza-handgemacht-italien-024.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Der Gartensalat wird auf einen runden Teller mit Goldrand garniert. Gänseblümchen aus unserer Wiese schmücken ihn zusätzlich.](rezept-grill-und-ofenpizza-handgemacht-italien-025.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grillpizza und Ofenpizza Italien. Foto vom Grill und einer krossen Pizza die noch auf dem Backstein liegt.](rezept-grill-und-ofenpizza-handgemacht-italien-026.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Grill- und Ofenpizza" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
