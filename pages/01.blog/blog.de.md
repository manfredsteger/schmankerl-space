---
title: 'Rezepte und Traditionelle Küche'
metadata:
    keywords: 'Rezepte, Kochen, Backen, Blog, Nussecken, Braten, Schmankerl'
    description: 'Rezepte und Schmankerl findet Ihr auf dem schmankerl.space eurem Koch Blog für Genießer und Köche. Traditionelle Gerichte und pfiffige internationale Rezepte zum Ausprobieren und Verbessern.'
    author: 'Chefköche Katharina und Manfred Steger'
sitemap:
    changefreq: monthly
body_classes: 'header-image fullwidth'
blog_url: blog
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
    limit: 6
    pagination: true
feed:
    description: 'Der abgespacte Blog mit raketenscharfen Rezepten zum Nachkochen und Genießen.'
    limit: 10
pagination: true
---
