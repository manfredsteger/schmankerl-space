---
title: 'Oberpfälzer Milchrahmstrudel'
published: true
date: '08:34 07/30/2018'
metadata:
    'og:title': 'Rezept: Oberpfälzer Milchrahmstrudel'
    'og:image': https://schmankerl.space/de/blog/apfelstrudel/000-teaser-apfelstrudel-milch-rosinen-selbst-gemacht.jpg
    'og:image:alt': 'Rezept Foto Ammerthaler Milchrahmstrudel. Das Foto zeigt einen Teller voll mit Vanillesoße und dem noch dampfenden Apfelstrudel. Mir läuft beim Schreiben schon das Wasser im Mund zusammen!'
    keywords: 'Strudel, Apfelstrudel, Milchrahmstrudel, Schmankerl, Bayerisch, Kornapfel, Backen, Rezept'
    description: 'Oberpfälzer Milchrahmstrudel: Ein Stück Heimat, das man überall genießen kann und das zu jeder Tageszeit. Rezept, Anleitung und viele Bilder hier im post.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Vegetarisch
        - Süßspeise
        - Hits4Kids
    tag:
        - Äpfel
        - Kornapfel
        - Teig
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Zubereitung:'
        value: '2 Stunden 30 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Auflaufform oder Ofenpfanne (Reindl), Mixer, Knethaken, Pinsel, Rührschüssel, Nudelholz'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Strudelteig:'
        list:
            - '500g Weizenmehl Type 405'
            - '1 Tl Salz'
            - '250ml lauwarmes Wasser'
            - '80g flüssige Butter'
    -
        title: 'Strudelfüllung:'
        list:
            - '2kg Kornäpfel (säuerlich)'
            - '2 El Semmelbrößel'
            - '1-3 El Zucker'
            - '2 El Rosinen'
            - '1 Prise Zimt'
            - '6 El Schlagsahne'
            - '8 El flüssige Butter'
    -
        title: 'Strudeltopping:'
        list:
            - '250ml Vollmilch'
            - '1 Ei'
            - '1 El Zucker'
            - '2-3 El flüssige Butter'
            - '3 El Vollmilch'
---

>Da schwelgt man gerne wieder in der Kinderheit
<cite>Knackig und süß-säuerlich, warm und luftig, ein Stück Heimat, das man überall genießen kann und das zu jeder Tageszeit.</cite>

## Anleitung

1. Für den Studelteig Mehl, Salz, flüssige Butter und das lauwarme Wasser mit dem Knethaken des Rührgeräts etwa 5 Minuten kneten, bis ein geschmeidiger Teig entsteht.
2. Den Teig in der Rührschüssel mit einem feucht-warmen Geschirrtuch abdecken und bei Zimmtertemperatur ca. 30 Minuten ruhen lassen.
3. In der Zwischenzeit vorzugsweise Kornäpfel oder alternativ säuerliche Äpfel schälen, von den Kernhäusern befreien und in Spalten schneiden.
4. Den Teig nach der Ruhezeit halbieren und mit einem Nudelholz so dünn wie möglich ausrollen. Sobald die Unterlage langsam durchscheint ist der Teig dünn genung.
5. Die zwei ausgerollten Teiglinge mit jeweils 1 El Butter und 1 El Semmelbrößel benetzen.
6. Danach die Apfelspalten auf die Teiglinge ausgewogen verteilen und diese mit der Sahne betreufeln.
7. Zucker und Zimt vermengen und je nach Süße des Apfels und persönlichem Geschmack versüßen.
8. Nach dem Belegen und Verfeinern wird der Strudel eingeschlagen und gerollt. Alle vier Seiten werden zunächst um etwa ein Viertel der Länge eingeschlagen und anschließend vorsichtig zusammengerollt.
9. Den Backofen auf 200 Grad Ober- und Unterhitze vorheizen.
10. Die Backform mit der übrigen flüssigen Butter bestreichen und je nach Formgröße ein bis zwei Teigrollen hineingeben.
11. Den Rohrstrudel mit 250ml Milch übergießen und im Ofen bei 200 Grad etwa 1 Stunde backen, bis er eine gesunde Bräune erreicht hat.
12. In der Zwischenzeit für das Topping 1 Ei mit 1 El Zucker und  3 El Vollmilch verquirlen.
13. Nach der Bräunungsphase des Strudels die Eiermilch über den Strudel gießen und die Form mit Alufolie abdecken. Bei ca. 150 Grad weitere 15 Minuten das Topping stocken lassen. Danach kann der Strudel heiß und saftig direkt serviert werden.

>Wer es noch ein wenig süßer mag
<cite>Wie man es aus Österreich kennt, passt zum Strudel eine Kugel Vanille Eis oder eine cremige Vanillesoße hervorragend dazu.</cite>

## Rezept Fotos

![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Katharina schält die Kornäpfel für den Apfelstrudel.](rezept-apfelstrudel-milchrahmstrudel-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Katharina schält die Kornäpfel für den Milchrahmstrudel.](rezept-apfelstrudel-milchrahmstrudel-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Die Kornäpfel liegen zerkleinert in einer Schüssel.](rezept-apfelstrudel-milchrahmstrudel-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Die Butter wird bei Raumtemperatur langsam zum schmelzen gebracht.](rezept-apfelstrudel-milchrahmstrudel-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Das Mehl liegt in der Rührschüssel bereit.](rezept-apfelstrudel-milchrahmstrudel-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Die Butter wurde über das Mehl in der Rührschüssel gegeben.](rezept-apfelstrudel-milchrahmstrudel-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Die Kitchen Aid Küchenmaschine rührt die Zutaten für den Strudel zusammen.](rezept-apfelstrudel-milchrahmstrudel-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde knetet den Teig nachdem rühren mit den Händen.](rezept-apfelstrudel-milchrahmstrudel-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde knetet den Teig nachdem rühren mit den Händen zu einer Kugel.](rezept-apfelstrudel-milchrahmstrudel-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde knetet den Teig nachdem rühren mit den Händen und wirft ihn immer wieder auf das Schneidebrett.](rezept-apfelstrudel-milchrahmstrudel-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Nahaufnahme Mama Siglinde knetet den Teig nachdem rühren mit den Händen und wirft ihn immer wieder auf das Schneidebrett.](rezept-apfelstrudel-milchrahmstrudel-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Katharina bepinselt eine Backform mit der zerlassenen Butter.](rezept-apfelstrudel-milchrahmstrudel-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde rollt den Teig mit einem Nudelholz aus.](rezept-apfelstrudel-milchrahmstrudel-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde rollt den Teig mit einem Nudelholz ganz dünn aus.](rezept-apfelstrudel-milchrahmstrudel-014.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde nimmt den ausgerollten Teig auf die Hände und zeigt ihn direkt in die Kamera.](rezept-apfelstrudel-milchrahmstrudel-015.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde breitet den Teig mit den Händen aus, damit er gleich befüllt werden kann.](rezept-apfelstrudel-milchrahmstrudel-016.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Der Teig wird mit Ei und Butter bestrichen.](rezept-apfelstrudel-milchrahmstrudel-017.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Die Kornäpfel kommen nun in den Teig des Strudels.](rezept-apfelstrudel-milchrahmstrudel-018.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Die Äpfel kommen ganz frisch aus der Schüssel in den Teig.](rezept-apfelstrudel-milchrahmstrudel-019.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Der ausgerollte Studelteig füllt sich langsam nach und nach mit Apfelstückchen.](rezept-apfelstrudel-milchrahmstrudel-020.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde streut auf den zweiten Studelteig die Kornäpfel.](rezept-apfelstrudel-milchrahmstrudel-021.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Seitenansicht des befüllten Studels.](rezept-apfelstrudel-milchrahmstrudel-022.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Kornapfel und Edelstahlschüssel auf dem Esstisch.](rezept-apfelstrudel-milchrahmstrudel-023.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde streut Rosinen auf den Teig.](rezept-apfelstrudel-milchrahmstrudel-024.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde faltet den Teig an den Ecken ein.](rezept-apfelstrudel-milchrahmstrudel-025.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde schlägt den Teig an den Ecken ein.](rezept-apfelstrudel-milchrahmstrudel-026.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde faltet den Teig an den Ecken ein und stülpt alles übereinander.](rezept-apfelstrudel-milchrahmstrudel-027.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde faltet den Teig an den Ecken ein und macht ein Quadrat daraus.](rezept-apfelstrudel-milchrahmstrudel-028.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde faltet den Teig an den Ecken ein und formt es langsam zu einem langen Schlauch.](rezept-apfelstrudel-milchrahmstrudel-029.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde rollt den Teig in einem Schlauch zusammen.](rezept-apfelstrudel-milchrahmstrudel-030.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde packt den Teig in den bepinselten Bräter.](rezept-apfelstrudel-milchrahmstrudel-031.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Der zweite Strudelteig wird gerade mit Zucker bestreut.](rezept-apfelstrudel-milchrahmstrudel-032.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde rollt den Teig wieder behutsam zusammen.](rezept-apfelstrudel-milchrahmstrudel-033.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Fast geschafft, der Teig ist beinahe zusammengerollt.](rezept-apfelstrudel-milchrahmstrudel-034.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Nahaufname: der Teig ist beinahe zusammengerollt.](rezept-apfelstrudel-milchrahmstrudel-035.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Seitenaufnahme: der Teig ist beinahe zusammengerollt.](rezept-apfelstrudel-milchrahmstrudel-036.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Beide Strudel liegen im Bräter nebeneinander und warten daruf in den Ofen geschoben zu werden.](rezept-apfelstrudel-milchrahmstrudel-037.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde schüttet das gequirlte Ei langsam über den ersten Strudel.](rezept-apfelstrudel-milchrahmstrudel-038.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde schüttet das gequirlte Ei langsam über den zweiten Strudel.](rezept-apfelstrudel-milchrahmstrudel-039.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Nahaufnahme Mama Siglinde schüttet das gequirlte Ei langsam über den ersten Strudel.](rezept-apfelstrudel-milchrahmstrudel-040.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Nahaufnahme Mama Siglinde schüttet das gequirlte Ei langsam über den zweiten Strudel.](rezept-apfelstrudel-milchrahmstrudel-041.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Seitenaufnahme Mama Siglinde schüttet das gequirlte Ei langsam über den ersten Strudel.](rezept-apfelstrudel-milchrahmstrudel-042.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Seitenaufnahme Mama Siglinde schüttet das gequirlte Ei langsam über den zweiten Strudel.](rezept-apfelstrudel-milchrahmstrudel-043.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Der Strudel backt im Ofen seit kurzer Zeit, das Ei stockt langsam und die Oberfläche wird gold braun.](rezept-apfelstrudel-milchrahmstrudel-044.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Mama Siglinde stülpt eine Alufolie über den fast fertigen Studel, damit die Oberfläche nicht verbrennt.](rezept-apfelstrudel-milchrahmstrudel-045.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Der fertige Strudel dampft in der Form vor sich hin und wird gleich angeschnitten.](rezept-apfelstrudel-milchrahmstrudel-046.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Katharina schneidet ein Stück Milchrahmstrudel heraus.](rezept-apfelstrudel-milchrahmstrudel-047.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Der Oberpfälzer Milchrahmstrudel angerichtet auf einem china bone Teller mit Vanillesoße.](rezept-apfelstrudel-milchrahmstrudel-048.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Draufsicht: Oberpfälzer Milchrahmstrudel angerichtet auf einem china bone Teller mit Vanillesoße.](rezept-apfelstrudel-milchrahmstrudel-049.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Oberpfälzer Milchrahm Apfelstrudel. Seitenansicht: Oberpfälzer Milchrahmstrudel angerichtet auf einem china bone Teller mit Vanillesoße.](rezept-apfelstrudel-milchrahmstrudel-050.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>


## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Oberpfälzer Milchrahmstrudel" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
