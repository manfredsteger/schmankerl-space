---
title: Sauerbraten mit Serviettenknödel
published: true
date: '17:34 03/31/2018'
metadata:
    og:title: 'Rezept: Sauerbraten mit Serviettenknödel'
    'og:image': https://schmankerl.space/de/blog/sauerbraten/000-teaser-sauerbraten-hessisch-bayerisch-sauce-bratensauce.jpg
    'og:image:alt': 'Eine Draufsicht auf den Sauerbraten, angerichtet auf einem großen weißen, geschwungenen Teller mit einem Semmelknödel und viel rahmiger Soße.'
    keywords: 'Sauerbraten, Serviettenknödel, Braten, Festtagsbraten, Schmankerl, Rinderfond, Suppengemüse, Tradition, Rezept'
    description: 'Sauerbraten mit Serviettenknödel: Sauerbraten ist bei uns zartes Stück Tradition. Dieser Braten wird seit Generationen zubereitet, weitergegeben und verfeinert. Lasst es euch schmecken!'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Braten
    tag:
        - Braten
        - Festlich
        - Knödel
        - Sonntagsessen
        - Fleisch
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: mittel
    -
        option: 'Reicht für:'
        value: '6 Personen'
    -
        option: 'Vorbereitung:'
        value: '4 Tage'
    -
        option: 'Zubereitung:'
        value: '6 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Herd, großer Topf, mittlerer Topf, kleiner Topf, Bräter, Sieb, Stabmixer, Geschirrtuch, Küchengarn, Schneebesen'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Sauerbraten:'
        list:
            - '2kg Tafelspitz oder Bürgermeisterstück vom Rind'
            - 'Sauerbratengewürz (Senfsaat, Lorbeerblätter, Wachholderbeeren, Nelken, Zimt, Zwiebeln, Pfeffer, Thymian, Piment, Ingwer, Koriander)'
            - '2,5 - 3 Liter Wasser'
            - '0,7 Liter Apfelessig'
            - '1kg Zwiebeln'
            - '100g Bauchspeck'
            - 'Suppengemüse (Kartotten, Sellerie, Lauch, Blumenkohl, Petersilienwurzel)'
            - 'Rinderfond'
            - '200g Crème fraîche'
            - 'Butterschmalz'
            - 'Zucker'
            - 'Salz & Pfeffer'
    -
        title: 'Serviettenknödel:'
        list:
            - '6 Weizenbrötchen'
            - '60g Butter'
            - 'Petersilie'
            - '1 große Zwiebel'
            - '2 Eier'
            - '130ml Milch'
            - 'Muskatnuss'
            - 'Salz & Pfeffer'
---

>Was lange währt, wird sicher butterweich!
<cite>4 Tage Vorbereitung, dafür aber wenig zu tun.</cite>

Der Sauerbraten ist in unserer Familie ein zartes Stück Tradition – an Geburtstagen, Weihnachten oder abgewandelt zu Ostern mit dem Osterhasen, wird er mit Knödeln gerne gereicht.

## Anleitung

### Fleisch beizen

Der Sauerbraten braucht viel Vorbereitungszeit, mindestens 3 Tage sollte das Rindfleisch oder der Hase im Essigsud ruhen. Der Essigsud hat nur wenig Zutaten und ist in 10 Minuten bereitgestellt.

1. 2,5 - 3 Liter Wasser (je nach Fleischstück) zum Kochen bringen. Währenddessen ca. 10 Esslöffel Sauerbratengewürz, 2 Esslöffel Zucker und ca. 2 Teelöffel Salz in das Wasser geben.
2. Sobald das Wasser kocht, das Kochfeld ausschalten und ca. 0,5 Liter Apfelessig hinzugeben. Abschmecken. Der Sud sollte süß-sauer sein und ähnlich wie bei Sauerne Bratwürste einen runden Geschmack haben. Wenn es einem das Gesicht verzieht, ist der Sud zu sauer, schmeckt man kaum Säure muss noch mit Essig nachgeholfen werden.
3. Anschließend das Fleisch ohne Weiteres in den heißen Sud legen. Einen Deckel auf den Topf legen und bei kühler Außentemperatur (ca. 2 - 12 Grad) oder nach einer 2 stündigen Abkühlphase in den Kühlschrank stellen.
4. Das Fleisch muss nun mindestens 1 Tag bis idealerweise 4 Tage beizen. Ist das Fleisch vollständig mit Flüssigkeit bedeckt, muss es nicht gewendet werden, ansonsten einaml pro Tag wenden.

>Tipp:
<cite>Umso länger die Beizzeit, desto zarter und geschmackvoller wird der Braten. Wird ein etwas zäheres Stück Rindfleisch wie z.B. Bug (falsches Filet) hergenommen, muss es länger mürbe gebeizt werden.</cite>

### Am Vortag braten

Festtage und großer Besuch können stressig genug werden, beim Sauerbraten ist die Hauptarbeit einen Tag davor zu erledigen.

1. Holt das Fleisch aus der Beize und lasst es auf mehreren Streifen Küchenrolle abtropfen. Stellt den Sud bei Seite, dieser wird später zur Schmorflüssigkeit.
2. Im Bräter 2 El Butterschmalz erhitzen und das abgetropfte Fleisch scharf von allen Seiten anbraten. Achtet darauf, dass sich Fleischreste am Boden des Bräters ansetzen. Das Fleisch lässt sich leicht vom Boden lösen, wenn eine gute Bratkruste entstanden ist.
3. Während des Bratens kann das Suppengemüse gewaschen, geschält und geschnitten werden. Das Gemüse bis auf die Zwiebeln in Scheiben schneiden, diese werden geachtelt.
4. Das Fleisch nach dem Anbraten beiseitestellen und das Gemüse hinzufügen. Gebt noch ein wenig Butterschmalz dazu und bei stark angesetzten Bratrückständen einen Schuss Wasser zum Lösen.
5. Bratet nun das Gemüse etwa 10 Minuten bei mittlerer Hitze an, bis es goldbraun wird. Heizt währenddessen den Ofen auf 160 Grad Ober-, Unterhitze vor.
6. Fügt nun das Stück Fleisch hinzu und gebt etwa 10 Schöpflöffel vom Essigsud dazu, bis das Fleisch etwa zu 1/3 bis 1/2 mit Flüssigkeit bedeckt ist.
7. Bei geschlossenem Deckel muss der Braten nun ca. 3 Studen schmoren. Jede Stunde sollte das Bratgut einmal gewendet werden.
8. Nach dem Schmorvorgang kann die Soße bereits zubereitet werden. Nehmt das Fleisch aus dem Bräter und püriert feinsäuberlich den gesamten Inhalt mit dem Pürierstab. Gebt nun einen Esslöffel Salz dazu, und gießt etwas Rinderfond an. Legt das Fleisch zurück, lasst den Bräter abkühlen. Wendet das Fleisch und stellt den Bräten dann wieder an einen kühlen Ort.


### Finalisieren und Serviettenknödel vorbereiten

Etwa 3 Stunden vor dem Festessen geht es wieder mit den Vorbereitungen los. Das Schöne dabei ist, wir brauchen nur etwas Zeit und wenig Handgriffe.

1. Heizt den Ofen erneut auf 160 Grad Ober-, Unterhitze vor und stellt den Bräter samt Inhalt in den Ofen. Nach etwa 1,5 Stunden sollte der Inhalt wieder schmoren.
2. Während der Aufwärmphase könnt ihr die Brötchen in kleine Würfel schneiden und auf ein Backblech ausbreiten. Wenn ihr damit fertig seit, schiebt die Brotwürfelchen auf der untersten Schiene mit in den Ofen und lasst diese vorsichtig trocken werden. Nach etwa 10 Minuten einmal wenden und nochmals 5 Minuten trocken werden lassen.
3. Inzwischen könnt ihr die Eier, die Milch und etwas Salz & Pfeffer in einer Schüssel verquirlen.
4. Wenn die Brotwürfelchen trocken sind, gebt diese in einen großen Behälter, lasst sie kurz abkühlen und gebt Eier und Milch Mix hinzu. Vermengt das Ganze gut und stellt alles beiseite.
5. Erhitzt die Butter in einem kleinen Topf, hackt die Zwiebel schön fein und wascht eine Hand voll Petersilie. Schwitzt die Zwiebeln goldgelb mit der Butter an und hackt zwischenzeitlich die Petersilie. Sind die Zwiebeln fertig, gebt die Petersilie noch 1 - 2 Minuten mit hinzu.
6. Gebt anschließend den Topfinhalt zur Brotmasse, mit Salz, Pfeffer und Muskat abschmecken. Vermengt alle Zutaten sehr gut. Stellt die Masse eine weitere halbe Stunde beiseite, damit der Inhalt quellen kann. Am besten knetet man die Masse ausgiebig mit der Hand durch. Sind die Semmelstückchen dann immer noch zu fest, kann man einfach noch etwas Milch angießen.

### Endspurt

Es ist fast alles geschafft, wieder ein Pluspunkt bei diesem Gericht, ihr habt keinen Zeitdruck am Ende. Viele Gerichte müssen mit den Beilagen auf den Punkt serviert werden – dem Braten ist es egal ob er noch 30 Minuten oder 2 Stunden länger schmort. 45 Minuten vor dem Essen müssen wir nur noch die Serviettenknödel formen und ins heiße Wasser legen.

1. Legt ein Küchentuch ausgebreitet bereit und gebt die Knödelmasse darauf. Formt eine lange ca. 6-7 cm im Durchmesser breite Rolle. Achtet dabei bereits auf den Kochtopf – es klingt trivial, kann aber problematisch werden. Die Rolle sollte relativ genauso lang, wie der Topf breit werden. Es ist kein Problem zwei Rollen anstatt einer zu formen. Wickelt die Masse fest mit dem Tuch ein und bindet die Enden mit einem Küchengarn ab. Legt die Rolle in den Topf und übergießt mit kochendem Wasser solange, bis die Rolle ca. zur Hälfte mit Wasser bedeckt ist. Bringt das Wasser zum köcheln und lasst die Knödel auf dieser Seite 20 Minuten garen. Anschließend dreht ihr die Rolle und lasst sie weitere 20 Minuten fertiggaren.
2. Holt den Braten zwischenzeitlich aus dem Ofen und schmeckt die Soße ab. Gebt Rinderfond, etwas Zucker, Salz & Pfeffer, je nach Geschmack hinzu. Falls die Soße zu sauer ist, etwas mehr Zucker hinzugeben. Achtet darauf, dass eine ausgewogene Restsäure vorhanden ist, im letzten Schritt wird diese nochmals deutlich reduziert. Wenn ihr den Bräter auf dem Kochfeld erhitzen könnt, tut dies. Achtet dabei darauf, dass die Soße nicht kocht, sondern leicht simmert (ähnlich wie beim Schmoren).
3. Sobald die Knödel fertiggegart sind, kommen die letzten beiden parallelen Schritte. Dreht die Hitze für den Braten ab und holt die Serviettenknödel aus ihrem Tuchmantel. Holt das Fleisch aus dem Bräter und legt es zum Schneiden auf ein Küchenbrett bereit. An dieser Stelle ist die Hilfe von 1 - 2 Personen ratsam. Am besten schneidet eine Person das Fleisch in etwa 1,5 cm dicke Scheiben, die Andere die Serviettenknödel in etwa 2 cm dicke Scheiben und der oder die Köchin gibt 200g Crème fraîche in die Soße. Die Soße darf unter gar keinen Umständen mehr kochen, da sonst die Crème fraîche zu Stocken beginnt. Mit einem Schneebesen zügig vermengen und fertig ist das Festgericht.
4. Richtet nun 1 - 2 Stücke Fleisch und 2 - 3 Stücke Serviettenknödel auf einem Teller an und übergießt mit ordentlich viel Soße den Braten. Noch ein paar frische Blätter Petersilie dazu und _wohl bekomm`s_!

## Rezept Fotos

![Rezept Foto Sauerbraten mit Serviettenknödel. Draufsicht auf den angerichteten Teller mit zwei Stücken Sauerbraten, Serviettenknödel und ganz viel Soße.](rezept-traditioneller-sauerbraten-schmankerl-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Das Suppengemüse ist auf dem Schneidebrett aufgelegt. Bereit zum Zerkleinern.](rezept-traditioneller-sauerbraten-schmankerl-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Zwiebeln, Karotten, Lauch und Kohl sind geschält und liegen zum Schnibbseln bereit.](rezept-traditioneller-sauerbraten-schmankerl-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Schwarz Weiss Fotografie des Sauerbraten Suds mit Loorbeerblättern, Essig Sud, Zwiebelringen und Senfsaat.](rezept-traditioneller-sauerbraten-schmankerl-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Der Sauerbraten Sud mit Loorbeerblättern, Essig Sud, Zwiebelringen und Senfsaat wird gleich aufgekocht,da mit der Tafelspitz hineinkommen kann.](rezept-traditioneller-sauerbraten-schmankerl-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Der 5 Tage lang gebeizte Sauerbraten sieht von außen grau aus und von innen ist das Fleisch dunkelrot. Der Braten fühlt sich jetzt schon weich an.](rezept-traditioneller-sauerbraten-schmankerl-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Seitenansicht des 5 Tage lang gebeizten Sauerbratens. Er sieht von außen grau aus und von innen ist das Fleisch dunkelrot.](rezept-traditioneller-sauerbraten-schmankerl-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Der Sauerbraten wird in einem Bräter mit Butterschmalz rundherum scharf angebraten. Die Röststoffe werden eine Grundlange für die schmackhafte Soße.](rezept-traditioneller-sauerbraten-schmankerl-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Schwarz Weiss Foto des Sauerbratens im Bräter. Die Kruste des Bratens nimmt langsam Form an.](rezept-traditioneller-sauerbraten-schmankerl-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Seitenansicht des Sauerbratens im Bräter. Die Kruste des Bratens nimmt langsam Form an.](rezept-traditioneller-sauerbraten-schmankerl-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Ansicht des Bräterbodens nach dem Entfernen des Tafelspitzs. Der Boden hat viel Röststoffe angesetzt und wird gleich mit Brühe abgelöscht.](rezept-traditioneller-sauerbraten-schmankerl-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. In den Bräter kommt nun das zerkleinerte Gemüse und wird rundherum scharf angebraten.](rezept-traditioneller-sauerbraten-schmankerl-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Nahaufnahme des scharf angebratenem Gemüse.](rezept-traditioneller-sauerbraten-schmankerl-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Ist das Gemüse rösch angebraten wird der Tafelspitz bzw. eingelegte Sauerbraten in den Bräter gegeben und oben aufgelegt.](rezept-traditioneller-sauerbraten-schmankerl-014.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Der Braten wird gerade mit Gemüsebrühe aufgegossen bis er mindestens ein viertel der Höhe bedeckt wird. Der Braten wird gleich in der Röhre schmoren.](rezept-traditioneller-sauerbraten-schmankerl-015.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Nahaufname vom Braten wie er im Sud langsam vor sich hin schmort.](rezept-traditioneller-sauerbraten-schmankerl-016.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. In der Zwischenzeit werden die alten Weizenbrötchen mit einem Brotmesser in ca. 1 cm breite Streifen geschnitten. Die Serviettenknödel sind gerade in der Vorbereitung.](rezept-traditioneller-sauerbraten-schmankerl-017.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Nachdem die Soße gemacht wurde kommt diese zusammen mit dem Sauerbraten zurück in den Bräter. Der Braten ruht in der Soße und nimmt den Geschmack bereits auf.](rezept-traditioneller-sauerbraten-schmankerl-018.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Sauerbraten mit Serviettenknödel. Das Rezept wird gerade auf einen Teller serviert. Zwei leckere Serviettenknödel und zwei Scheiben Sauerbraten mit ganz viel rahmiger Sauce warten darauf verzehrt zu werden.](rezept-traditioneller-sauerbraten-schmankerl-019.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Sauerbraten mit Serviettenknödel" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
