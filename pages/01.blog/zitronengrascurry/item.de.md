---
title: Zitronengras Thai Curry
published: true
date: '10:15 01/04/2021'
metadata:
    og:title: 'Rezept: Zitronengras Thai Curry'
    'og:image': https://schmankerl.space/de/blog/zitronengrascurry/000-titelbild.jpg
    'og:image:alt': 'Rezept und Zutaten Foto von der Thai Curry Paste auf dem schmankerl.space. Ein super leichtes und schmackhaftes Rezept zum Nachkochen.'
    keywords: 'Rezept, asiatisch, Zitronengras, Thai-Curry, Curry, Kokosmilch, Asia Küche'
    description: 'Rezept Thai Curry und Thai Curry Paste schnell selbst zubereitet: Was hat Thai Curry mit Bayern zu tun? Fast alles kommt aus dem heimischen Garten, bis auf die Kokosmilch und der Ingwer. Gesund, schnell, lecker, also ran an die Messer.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Asia
        - Thai
    tag:
        - Gemüse
        - frisch
        - asiatisch
        - Curry
        - Thai
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: 'keine'
    -
        option: 'Zubereitung:'
        value: '2 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Blender, Rührgerät, Pfanne oder Wok.'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Zitronengrascurry Paste:'
        list:
            - '4 Stengel frisches Zitronengras'
            - '40g Ingwer'
            - '4 frische Chillis mittelscharf'
            - '1 Limette'
            - '4 Schalotten'
            - '3 Zehen Knoblauch'
            - '2 EL Speiseöl'
    -
        title: 'Zitronengras Thai Curry:'
        list:
            - '2 Paprikas'
            - '1 mittelgroße Zwiebel'
            - '2 Karotten'
            - '100g Zuckerschoten'
            - '200g Kichererbsen aus der Dose'
            - '50g Cashewkerne'
            - '1 Zehe Knoblauch'
            - '500ml Kokosmilch'
            - '1-2 El Zitronengras Thai Curry Paste'
            - 'Kokosfett'
            - 'Salz & Pfeffer'
---

>Curry geht immer mit allem!
<cite>Das coole am Curry, es geht alles rein was man möchte. Bißchen Hähnchen dazu oder unterschiedlichstes Gemüse aus dem Garten. Wir packen meist alles kohlige rein – Blumenkohl, Rosenkohl, Kohlrabi ...</cite>

## Anleitung

### Zitronengrascurry Paste

1. Für die Zitronengras Paste das Zitronengras waschen und die grünen Teile bis zum Übergang zum Bauchigen abschneiden und in Ringe schneiden. Diese können super getrocknet und fein gemahlen als Pulver weiterverwendet oder in größeren Stücken als Tee genutzt werden.
2. Ingwer, Knoblauch und Zwiebeln schälen und in Stücke schneiden.
3. Chilli waschen, Strunk entfernen und in Ringe schnibbeln.
4. Die Limette von der Schale befreien und vierteln.
5. Alle Zutaten und das Speiseöl in einen Blender geben und gib ihm! Pulsiert mit dem Blender, bis eine feine Masse entsteht. Falls es mal nicht so will, ein wenig mehr Öl dazu und mit dem Stößel die Masse einmal vermengen.
6. Wenn die gewünschte Konsistenz erreicht ist abfüllen oder beiseite stellen.

### Zitronengras Thai Curry

1. Für das Thai Curry das Gemüse schälen und in mundgerechte Stücke schneiden.
2. Die Kichererbsen abtropfen lassen und beiseite stellen.
3. Knoblauch schälen und fein hacken.
4. Die Kokosmilch in eine Rührschüssel geben und 1-2 Eßlöffel Zitronengrascurry Paste dazugeben.
5. Rührt nun mit einem Flachrührer oder Schneebesen 2-3 Minuten auf geringer Stufe, bis das Curry sich aufgelöst hat und die Masse eine schaumigen Konsistenz besitzt.
6. In der Zwischenzeit könnt ihr den Herd vorheizen und 1-2 El Kokosfett erhitzen. Bratet das Gemüse darin kurz und scharf an.
7. Ihr könnt hier jegliches Gemüse verwenden, das ihr mögt. Gebt aber nicht alles auf einmal in die Pfanne. Nehmt das Gemüse mit der längsten Garzeit und bratet dieses als ersten an. Ist es scharf angebraten, schiebt es an den Rand zum weitergaren.
8. Bei dem oben aufgeführten Gemüse würde ich in dieser Reihenfolge vorgehen:
9. Karotten, Zuckerschoten, Paprika, Zwiebel und zum Schluss der Knoblauch.
10. Reduziert die Hitze und gebt die Kokosmilchmasse dazu.
11. Kichererbsen und Cashewkerne noch rein und bei geringer Hitze alles heiß werden lassen. Der Inhalt darf nicht mehr aufkochen, ansonsten ist das süße Kokosmilch Aroma weg.
12. Während des Aufwärmens noch mit Salz & Pfeffer abschmecken.

>Mehr geht einfacher
<cite>Die Zitronengras Curry Paste kann am besten in einer etwas größeren Menge hergestellt werden. Wir machen meist die 5 fache Menge, damit rentiert sich das versauen des Blenders :) Eingefroren ist die Paste locker 2 Jahre haltbar.</cite>

## Rezept Fotos

![Rezept Foto Zitronengras Thai Curry. Auf einem Schneidebrett sind die Zutaten für die Thai Curry Paste vorbereitet.](Zitronengras-Thaicurry-Paste-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Zitronengras Thai Curry. Auf einem Schneidebrett sind die Zutaten für die Thai Curry Paste vorbereitet.](Zitronengras-Thaicurry-Paste-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Zitronengras Thai Curry. Die roten Zwibeln und das Zitronengras wurden geschält und bereitgelegt.](Zitronengras-Thaicurry-Paste-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Zitronengras Thai Curry. Die Limetten aufgeschnitten und gleich der Saft ausgepresst.](Zitronengras-Thaicurry-Paste-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Zitronengras Thai Curry. Alle Zutaten für die Paste sind im Blender und warten darauf gemixt zu werden.](Zitronengras-Thaicurry-Paste-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Detailaufnahme Zitronengras Thai Curry. Alle Zutaten für die Paste sind im Blender und warten darauf gemixt zu werden.](Zitronengras-Thaicurry-Paste-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Detailaufnahme von oben Zitronengras Thai Curry. Alle Zutaten für die Paste sind im Blender und warten darauf gemixt zu werden.](Zitronengras-Thaicurry-Paste-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
![Rezept Foto die Kokosmilch wird mit der Küchenmaschine und der Thai Curry Paste cremig gerührt.](Zitronengras-Thaicurry-Paste-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Das Thai Curry wird zusammen mit dem Gemüse in der großen Pfanne bzw. Bräter scharf angebraten und mit der Curry Kokosmilch Masse abgelöscht.](Zitronengras-Thaicurry-Paste-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Zitronengras Thai Curry" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
