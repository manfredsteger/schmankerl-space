---
title: Kürbisquiche
published: true
date: '17:34 10/29/2018'
metadata:
    og:title: 'Rezept: Kürbisquiche'
    'og:image': https://schmankerl.space/de/blog/kuerbisquiche/000-teaser-kuerbis-quiche-speck-rezept-gericht.jpg
    'og:image:alt': 'Rezept Foto der Kürbisquiche. Das Foto zeigt die noch dampfende Kürbisquiche. Angerichtet auf einem runden Teller, garniert mit frischem Pfeffer aus der Mühle.'
    keywords: 'Kürbis, Kürbisquiche, Hokkaidokürbis, Teig, Speck, Kürbiszeit, Halloween, Rezept'
    description: 'Kürbisquiche: Nach Halloween Kürbis über? Im Winter ist die Kürbiszeit die Schönste. Probiert unser leichtes Kürbisgericht einfach mal aus.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Schmankerl
    tag:
        - Kürbis
        - Herbst
        - Schmankerl
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: '1 1/2 Stunden'
    -
        option: 'Zubereitung:'
        value: '1/2 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Quicheform, große Schüssel, Topf, Reibe/Küchenmaschine, Backofen, Nudelholz'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Teig:'
        list:
            - '250g Weizenmehl (Type 405)'
            - '100g Frischkäse'
            - '100g Butter'
            - '1 Eigelb'
            - 'Salz'
            - 'Olivenöl'
    -
        title: 'Füllung:'
        list:
            - '1 Hokkaidokürbis (klein-mittel)'
            - '1 Zwiebel'
            - '1 Knoblauch'
            - '125g Speck'
            - '250g Creme fraiche'
            - '3 Eier'
            - '120g geriebener Käse (z.B. Gouda, Emmentaler, o.ä.)'
            - 'Muskatnuss'
            - 'Salz & Pfeffer'

---

>Kürbis ist nicht nur zum Gruseln da:
<cite>Halloween ist vorbei, aber die Kürbiszeit noch lange nicht, sie hat gerade erst angefangen.</cite>

## Anleitung

1. Für den Teig die Butter in kleine Flocken oder Stücke schneiden und das Ei trennen. Nun kann das Mehl mit Frischkäse, Butter, Eigelb und einer Prise Salz zu einem glatten Teig verknetet werden. Diesen in Frischhaltefolie eine Stunde im Kühlschrank ruhen lassen.
2. Die Quicheform mit einem Schuss Olivenöl einfetten.
3. Zwiebel und Knoblauch klein schneiden. Käse reiben.
4. Den Ofen auf 200°C Ober-/Unterhitze vorheizen.
5. Den Kürbis vierteln, mit einem Löffel die Kerne entfernen und schälen. Anschließend in kleinere Stücke schneiden und alles fein reiben bzw. raspeln.
6. Den Speck in einem Topf kross anbraten und anschließend Zwiebel und Knoblauch mit anschwitzen.
7. Die Creme fraiche mit den Eiern in einer großen Schüssel verquirlen und mit Salz, Pfeffer und Muskatnuss würzen. Anschließend den geriebenen Kürbis, Käse, angebratenen Speck und Zwiebeln dazugeben und alles gut miteinander vermengen.
8. Den Teig mit einem Nudelholz etwa auf die Größe der Form ausrollen, in die eingefettete Quicheform legen und anpassen. Mit einer Gabel ein paar mal den Teig einstechen.
9. Nun kann die Füllung hineingegeben werden und alles für etwa eine Stunde im vorgeheizten Backofen backen lassen.

>Auf Wunsch auch vegetarisch:
<cite>Auch wenn er ein hervoragender Geschmacksträger ist, kann der Speck jederzeit weggelassen werden.</cite>

## Rezept Fotos

![Rezept Foto Kürbis Quiche selber machen. Garten Hokkaido Kürbis liegt auf einem Schneidebrett und wartet darauf in Stücke geschnitten zu werden.](rezept-kuerbis-quiche-butter-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Der Hokkaido Kürbis wurde in Stücke geschnitten und die Kerne entfernt.](rezept-kuerbis-quiche-butter-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Seitenansicht vom Hokkaido Kürbis der in Stücke geschnitten die Kerne entfernt wurden.](rezept-kuerbis-quiche-butter-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Stücke des Kürbisses auf dem Schneidebrett und daneben liegt ein Brotmesser.](rezept-kuerbis-quiche-butter-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Man sieht genau in die drehende Raspel der Kitchen Aid Küchemaschine. Es kommen die geraspelten Kürbisstückchen heraus und fallen in die Schüssel.](rezept-kuerbis-quiche-butter-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Auf einem Schneidebrett wurden Zwiebelchen und Schinken Speck für den Belag der Quiche gehackt.](rezept-kuerbis-quiche-butter-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Ein Blick in den Topf, in dem der Speck knusprig braun gebraten wird.](rezept-kuerbis-quiche-butter-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Quicheform in der der Teig bereits ausgelegt wurde.](rezept-kuerbis-quiche-butter-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Katharina stürzt gerade die Kürbisraspel in eine Schüssel und vermengt es mit den Zutaten.](rezept-kuerbis-quiche-butter-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Katharina verrührt die Kürbisstückchen mit den weiteren Zutaten für das Rezept.](rezept-kuerbis-quiche-butter-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Katharina drückt die Kürbis Speck Masse vorsichtig in die Quicheform.](rezept-kuerbis-quiche-butter-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Einblick in den Ofen, in dem die noch rohe Quiche liegt.](rezept-kuerbis-quiche-butter-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Die Quiche kommt frisch aus dem Ofen, die knackige Oberfläche lockt und der krosse Speck fällt sofort ins Auge.](rezept-kuerbis-quiche-butter-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Katharina hat die frische Quiche gerade eben angeschnitten und hebt vorsichtig das erste Stück heraus.](rezept-kuerbis-quiche-butter-014.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Quiche selber machen. Hokkaido Kürbis Quiche angerichtet auf einem weißen china bone Teller mit Salz und Pfeffer garniert.](rezept-kuerbis-quiche-butter-015.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Kürbisquiche" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
