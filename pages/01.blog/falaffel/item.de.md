---
title: Falaffel
published: true
date: '12:20 11/06/2020'
metadata:
    og:title: 'Rezept: Falaffel selbst gemacht'
    'og:image': falaffel/000-titelbild.jpg
    'og:image:alt': 'Rezept Foto von den selbstgemachten Falaffeln. Frittierte Falaffelbällchen angerichtet auf einem Teller.'
    keywords: 'Rezept, orientalisch, Falaffel, Kichererbsen, Hummus, vegetarisch'
    description: 'Falaffel einfach selbst gemacht: Wer gerne frisch, gesund und sättigend isst, der kommt an der Falaffel nicht vorbei! Der Falaffelwrap ist ein geniales orientalisches Essen.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Vegetarisch
    tag:
        - Falaffel
        - Hummus
        - vegan
        - vegetarisch
description:
    -
        option: 'Schwierigkeitsgrad: mittel'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: '1 Tag'
    -
        option: 'Zubereitung:'
        value: '2 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Reibe, Kochtopf, Fritöse, Fritiertopf'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Falaffel:'
        list:
            - '250g getrocknete Kichererbsen'
            - '1 große Zwiebel'
            - '1 Zehe Knoblauch'
            - '1/4 Bund Petersilie'
            - '1/2 Tl Kreuzkümmel im Ganzen'
            - '1/4 Tl Kardamom gemahlen'
            - '1/2 Tl Cayenne Pfeffer'
            - '1 Tl Salz'
            - '1/2 Tl Backpulver'
            - '2 El Mehl'
            - '2-3 El Aquafaba oder Wasser'
---

>Vorsicht: Fleischfresser mutieren!
<cite>Fleischliebhaber können ganz schnell zur vegetarischen/veganen Haltung wechseln, wenn sie dieses Rezept probieren. Bei mir war es zumindest so! Das neue Lieblingsessen von Manfred.</cite>

> Tipp: Die Menge macht's
<cite>Am besten gleich doppelt oder dreifache Menge machen, die rohen Falaffeln lassen sich prima einfrieren.</cite>

## Anleitung

1. Die getrockneten Kichererbsen über Nacht in kaltes Wasser einlegen. Wer es ganz eilig hat, kann auch eingelete Kichererbsen aus der Dose nehmen, jedoch wird die Konsistenz nicht so toll, eher matschig, und schmeckt lange nicht so nussig.
2. Befreit anschließend die Kichererbsen vom dickflüssigen Wasser (Aquafaba), fangt dabei eine Tasse voll auf. Spült die Kichererbsen danach einmal mit klarem Wasser ab. Ähnlich wie beim Nudelkochen ist das Stärkewasser gut für das Binden von Saucen o.ä. geeignet.
3. Kocht die Kichererbsen maximal 10 Minuten, damit Sie ein wenig weicher und bekömmlicher werden. Wir haben festgestellt, dass die rein eingeweichten Kichererbsen etwas aufstoßen und beim Frittieren etwas hart werden können. Aber nicht zu lange kochen, ansonsten zerfallen die Falaffeln später beim Frittieren.
4. Schüttet das Kochwasser ab und lasst die Kichererbes etwa 10 Minuten abkühlen.
5. Jetzt wird's interessant, wir müssen die kleinen Erbsen reiben. Das ist gar nicht so leicht. Wir haben sehr viel herumprobiert und das beste Ergebnis mit einem Kartoffelreiben Einsatz für die KitchenAid erreicht. Eine Nussreibe tut auch einen guten Job, passt aber auf, dass die Stücke nicht zu fein werden. Ihr verliert sonst den Biss bzw. den leckeren Crunchy-Teil dieses Gerichts.
6. Zwiebel und Knoblauch schälen und fein hacken.
7. Die Petersilie fein hacken.
8. Gebt nun die Zwiebeln, Knoblauch und Petersilie zur Kichererbsenmasse hinzu und vermengt alles miteinander.
9. Nun kommen die Gewürze und der Rest dran. Gebt den Kreuzkümmel, Kardamom, Cayenne Pfeffer, Salz, Backpulver und Mehl dazu. Hebt alles vorsichtig unter.
10. Gebt noch 3 El Aquafaba dazu und vermengt alles erneut miteinander.
11. Macht nun mit den Händen den Formtest. Nehmt eine viertel Hand voll und drückt die Masse fest zusammen, es sollte ein kompakter Teig entstehen. Ist die Masse zu krümelig bzw. fällt der Teig auseinander, fügt weiteres Wasser hinzu.
12. Der Teig muss nun ca. eine halbe bis Stunde abgedeckt im Kühlschrank ziehen.
13. Formt nun mit feuchten Händen und einem kräftigen Händedruck kleine Bällchen aus dem Teig.
14. Erhitzt in einem Topf das Frittieröl und macht den Zahnstocher Test, ob das Fett heiß genug ist. Haltet dafür den Zahnstocher oder Schaschlikspiel in das heiße Fett. Wenn kleine Bläschen aufsteigen, ist das Fett heiß genug. Ist das Fett zu heiß wirbeln die Falaffeln herum und können leicht zerfallen.
15. Frittiert die kleinen Wunderbällchen ca. 3-5 Minuten, bis sie braun sind.
16. Nachdem Frittieren die Bällchen auf einem Stück Küchenrolle abtropfen lassen und warm halten.

> Tipp: Beim Frittieren zerfallen?
<cite>Keine Sorge, das kriegen wir schon hin. Meistens zerfallen die Falaffeln beim Frittieren wenn zu wenig Bindung vorhanden ist. Ihr könnt beim Rollen schon eine Probefalaffel frittieren. Wenn Sie zerfällt, fügt noch mehr Aquafaba dazu und presst die Bällchen noch stärker zusammen. Wenn das auch nichts hilft, könnt ihr die gerollten Bällchen noch im Ofen leicht vorbacken und dann erst bei reduzierter Hitze frittieren. Es kann manchmal auch an den Kichererbsen liegen, beim nächsten Mal eine andere Marke probieren.</cite>

## Rezept Fotos

![Rezept Foto Falaffel. Die Kichererbsen werden gerade durch die Kartoffelreibe gedreht.](falaffel-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto der Falaffeln. Die Kichererbsen werden gerade durch die Kartoffelreibe gedreht und in einer Rührschüssel aufgefangen. ](falaffel-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Falaffelbällchen. Kichererbsenmasse mit allen Gewürzen.](falaffel-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Falaffel. Gerollte Falaffelteiglinge auf dem Küchenbrett.](falaffel-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Falaffel. Katharina rollt gerade die Teiglinge mit den Händen.](falaffel-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Falaffel. Katharina rollt gerade die Teiglinge mit den Händen. Schwarz Weiss Foto.](falaffel-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Falaffeln beim Fittieren.](falaffel-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Falaffel selbst gemacht" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
