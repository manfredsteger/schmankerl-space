---
title: MINI Power Smoothie
published: true
date: '14:23 02/22/2020'
metadata:
    og:title: 'Rezept: Grüner Power Smoothie'
    'og:image': https://schmankerl.space/de/blog/smoothie-kiwi/000-teaser-smoothie-green-gesund-kiwi-banane-rezept.jpg
    'og:image:alt': 'Rezept Foto vom Grünen Power Smoothie. Das Foto zeigt den fertigen, weichen Smoothie in einem Glas. Dahinter steht der übrige Smoothie in einem Weckglas.'
    keywords: 'Rezept, Smoothie, Kiwi, Banane, Mango, gesund, Zucchini'
    description: 'Kiwi Smoothie: Ein Smoothie Shot am Tag ist der perfekte Vitamin Kick und schmeckt dazu noch verdammt lecker. Das tolle am Früchte Smoothie ist die einfache Abwandlung, wenn man ein gutes Grundrezept hat.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Smoothie
    tag:
        - Früchte
        - Obst
        - Gemüse
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '5 Personen'
    -
        option: 'Vorbereitung:'
        value: 'keine'
    -
        option: 'Zubereitung:'
        value: '30 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Standmixer oder Blender'
ingredients_title: Zutaten
ingredients:
    -
        title: 'MINI Power Smoothie:'
        list:
            - '2 Äpfel'
            - '3 reife Kiwis'
            - '1 reife Mango'
            - '2 Bananen'
            - '15 grüne Weintrauben'
            - '1 Limette oder halbe Zitrone'
            - '75g Salatgurke'
            - '75g Zucchini'
            - '1El Chia- oder Leinsamen oder 1El Speiseöl'
            - '1Tl Spirulina- oder Weizengraspulver'
            - '400-750ml Leitungswasser'
---

>Schlapp? Kränklich? Lust auf was Süßes?
<cite>Ein Smoothie Shot am Tag ist der perfekte Vitamin Kick und schmeckt dazu noch verdammt lecker. Besser als jedes Nahrungsergänzungsmittel, hält der Smoothie das ganze Jahr fit.</cite>

## Anleitung

1. Kauft am besten Bio Ware für den Smoothie, denn alles was gegessen werden kann, wird verarbeitet.
2. Wascht das Obst und das Gemüse gründlich und entfernt alle Stiele.
3. Schneidet die Äpfel in Spalten und entfernt das Kernhaus nicht.
4. Schält die Mango und löst das Fruchtfleisch vom Kern.
5. Schneidet die Gurke und Zucchini in dicke Scheiben.
6. Schält die Bananen und schneidet diese in grobe Stücke.
7. Halbiert die Limette oder Zitrone.
8. Werft nun alle Zutaten in Euren Standmixer oder Blender und presst den Limetten- oder Zitronensaft in den Behälter.
9. Je nach Wunsch kommen am Schluss 400-750ml Wasser dazu. Umso mehr Wasser, umso süffiger wird der Smoothie. Fangt am besten mit den 400ml an und bei Bedarf könnt ihr noch Wasser nachkippen, falls das Endergebnis zu fest wird.
10. Schaltet nun den Standmixer oder Blender auf kleiner Stufe ein und wartet bis die Zutaten langsam zu einem Brei werden.
11. Sind die Zutaten breiartig, fahrt den Standmixer oder Blender langsam auf Höchstleistung. Bei maximaler Stufe etwa 30 Sekunden bis 1 Minute laufen lassen.
12. Je nach Power und Hochwertigkeit des Standmixers oder Blenders erhaltet ihr ein weiches Endergebnis oder nur ein Mousse. Wir nutzen einen Blender mit 24.000 Umdrehungen pro Minute und 3,5 PS Leistung, das Ergebnis wird wolkenweich :)
13. Ihr könnt den Smoothie in Einweckgläsern 2-3 Tage im Kühlschrank lagern.

>Grundrezept abändern
<cite>Ihr könnt die Früchte beliebig austauschen. Banane und Mango machen den Smoothie dick, Kiwi gibt den säuerlichen Geschmack ab und gibt die satte Farbe. Tauscht z.B. alle grünen Früchte gegen Waldbeeren und Erdbeeren aus und anstatt der Gurke wie Zucchini einfach rote Beete dazu. Vorsicht bei der Orange! Schmeckt super, wird aber bei längerer Lagerung sehr bitter, Orange würde ich nur direkt verköstigen.</cite>

## Rezept Fotos

![Rezept Foto Grüner Power Früchte Smoothie. Alle Früchte in einer Tonschale angerichtet, daneben ein Schneidebrett.](rezept-power-smoothie-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grüner Power Früchte Smoothie. Die Zutaten liegen nun auf dem Tisch bereit für die Verarbeitung.](rezept-power-smoothie-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grüner Power Früchte Smoothie. Die geschälten Früchte liegen zum Zerkleinern für den Smoothie bereit.](rezept-power-smoothie-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grüner Power Früchte Smoothie. Detailaufnahme der geschälten Früchte, die zum Zerkleinern für den Smoothie bereit liegen.](rezept-power-smoothie-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grüner Power Früchte Smoothie. Zweite Detailaufnahme der geschälten Früchte, die zum Zerkleinern für den Smoothie bereit liegen.](rezept-power-smoothie-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grüner Power Früchte Smoothie. Die Zutaten liegen alle im Blender bereit und warten darauf gemixed zu werden.](rezept-power-smoothie-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grüner Power Früchte Smoothie. Schwarz Weiss Aufnahme als der Blender eingeschaltet wurde und die Zutaten zerkleinert werden.](rezept-power-smoothie-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grüner Power Früchte Smoothie. Darstellung des großen Blenders, der für den Smoothie bereitsteht.](rezept-power-smoothie-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grüner Power Früchte Smoothie. Endergebnis Aufnahme des super weichen Smoothies.](rezept-power-smoothie-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Grüner Power Früchte Smoothie. Rezeptfoto vom fertig angerichteten Smoothie in einem Tumbler. Im Hintergrund steht ein Weckglas voll mit Smoothie.](rezept-power-smoothie-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "MINI Power Smoothie" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
