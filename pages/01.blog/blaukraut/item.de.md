---
title: Traditionelles Blaukraut
published: true
date: '08:40 01/10/2021'
metadata:
    og:title: 'Rezept: Traditionelles Blaukraut'
    'og:image': https://schmankerl.space/de/blog/blaukraut/000-titelbild.jpg
    'og:image:alt': 'Zutaten für das Blaukraut auf einem Schneidebrett angerichtet für das Rezept.'
    keywords: 'Rezept, Blaukraut, traditionell, Rotkohl, Beilage'
    description: 'Blaukraut: Ein richtiges Schmankerl aus der Heimat. Der traditionelle Rothol aka Blautkraut wird zu vielen Speisen gereicht wie z.B. Entenbrust, Fleischpflanzerl und Wildgerichten.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Beilage
    tag:
        - Blaukraut
        - Rotkohl
        - vegetarisch
        - vegan
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: 'keine'
    -
        option: 'Zubereitung:'
        value: '2 Stunden'
    -
        option: 'Ausstattung:'
        value: 'großer Topf, Hobel oder Küchenmaschine'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Blaukraut alias Rotkohl:'
        list:
            - '1 mittelgroßer Kopf Blaukraut'
            - '3 kleine Äpfel'
            - '1 Zwiebel'
            - '120ml Gemüsebrühe'
            - '4 Nelken'
            - '3 El Branntweinessig'
            - '1 El Zucker'
            - '4 El Sonnenblumenöl'
            - '1 1/2 Tl Salz'
            - '1-2 Tl Mehl'
---

>Eine kräftige Farbe macht das Kraut fürs Auge besonders schmackhart.
<cite>Gut zu wissen... Essig darf bei der Zubereitung nicht fehlen, da er die leuchtende Farbe des Blaukrauts erhält und das Kraut mürbe macht.</cite>

## Anleitung

1. Das Blaukraut von den äußeren Blättern befreien, halbieren oder vierteln und den Strunk entfernen.
2. Zwiebel und Äpfel schälen und in feine Spalten schneiden.
3. Das Kraut mit der Küchenmaschine, oder einem Hobel fein reiben.
4. In einem großen Topf das Öl bei mittlerer Hitze erwärmen und die Zwiebel und Äpfel anschwitzen. Anschließend den Zucker einrieseln und alles karamellisieren lassen.
5. Das Kraut mit unterheben und kurz anschwitzen.
6. Hitze weiter reduzieren und Salz, Essig, Brühe und Nelken zugeben. Der Topfboden sollte stets gut mit Flüssigkeit bedeckt sein. Im Zweifel weitere Brühe nachgießen.
7. Mit geschlossenem Deckel alles 20-30 Minuten köcheln lassen, je nachdem wie bissfest, oder weich man das Kraut möchte.
8. Abschließend abschmecken. Die Mengenangaben variieren je nach Größe des Krauts. Lieber mit der beschriebenen Menge beginnen, nachwürzen kann man immer noch.
9. Zum Schluss wird das Kraut noch angedickt, um die restliche Flüssigkeit zu binden. Hierfür werden 1-2 El Mehl in 3-4 El lauwarmen Wasser gelöst. Das Mehlteigerl sollte eine zähflüssige Konsistenz haben und nicht klumpen. Nun vorsichtig unter ständigem Rühren zum Kraut geben, damit es sich gut verteilt. Nach kurzer Zeit sollte das Blaukraut schon merklich angedickt sein.

## Rezept Fotos

![Rezept Foto Blaukraut. Der Rotkohl wird gerade von Katharina in Viertel gschnitten und der Strunk mit einem Küchenmesser entfernt.](Blaukraut-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Blaukraut. Der Rotkohl wird gerade von Katharina in Viertel gschnitten und der Strunk mit einem Küchenmesser entfernt.](Blaukraut-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Detail Bild von den Blaukraut Streifen die aus der Küchenmaschine gekommen sind.](Blaukraut-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Ansicht des Topfs mit dem köchelnden Blaukraut darin.](Blaukraut-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Traditionelles Blaukraut" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
