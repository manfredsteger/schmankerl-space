---
title: Italienischer Nudelsalat
published: true
date: '17:34 03/04/2018'
metadata:
    og:title: 'Rezept: Italienischer Nudelsalat'
    'og:image': https://schmankerl.space/de/blog/italienischer-nudelsalat/000-teaser-italienischer-nudelsalat-mediterran-ruccola-rezept.jpg
    'og:image:alt': 'Rezept Foto vom italienischen Nudelsalat. Das Foto zeigt eine Nahaufnahme von einem gewölbtem Teller mit reichlich Nudelsalat darauf. Der Nudelsalat ist mit frischem Ruccola und Balsamico Creme garniert worden.'
    keywords: 'Nudeln, Pasta, italienisch, Nudelsalat, italienischer Nudelsalat, Penne, Mozzarella, rotes Pesto, Balsamico, Parmesan, Rezept'
    description: 'Italienischer Nudelsalat: Do it yourself! Rotes Pesto und getrocknete Tomaten für das Rezept gibt es hier auch auf dem Blog :) Geniales Gericht als Mitbringsel für Partys und Geburtstage.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Salat
    tag:
        - Salat
        - Grillen
        - Party
        - Pasta
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: '3 Stunden'
    -
        option: 'Zubereitung:'
        value: '30 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Große Schüssel'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Nudelsalat:'
        list:
            - '120g eingelegte, getrocknete Tomaten'
            - '500g Penne'
            - '250g Mozzarella'
            - '160g Parmaschinken'
            - '50g Pinienkerne'
            - '50g Cashewkerne'
            - '1 Bund Rucola'
            - '1 Knoblauchzehe'
            - '60g Parmesan'
            - '8 EL Balsamico'
            - 'Balsamico Creme'
            - '10 EL Olivenöl'
            - '1 TL scharfer Senf'
            - '1 TL Honig'
            - '1 TL rotes Pesto (optional)'
            - 'Salz & Pfeffer'
---

>Do it yourself:
<cite>Die [getrockneten Tomaten](../eingelegte-getrocknete-tomaten/item.de.md) legen wir selbst ein, da sie gekauft zu teuer sind und fürchterlich schmecken. Außerdem lohnt sich ein Vorrat für mehrere Gerichte, wie das [rote Pesto](../rotes-pesto/item.de.md), immer.</cite>

## Anleitung

1. Die Penne im Salzwasser aldente kochen, abgießen und mit der Hälfte des Olivenöls im Topf abkühlen lassen.
2. Getrocknete Tomaten, Mozzarella und Parmaschinken klein schneiden. Die Knoblauchzehe fein hacken.
3. Zu den kalten Nudeln nun das restliche Öl, Balsamico, Senf, Honig, Pesto (optional), Knoblauch, Salz und Pfeffer geben. Alles gut vermengen, anschließend die Tomaten, Mozzarella, Schinken, Pinien- und Cashewkerne unterheben. Der Salat sollte nun gute 2 Stunden ziehen. Vor dem Servieren nach Belieben abschmecken.
4. Zum Anrichten kommt auf den Nudelsalat der gewaschene Rucola, geriebener Parmesan und zum Abschluss Balsamico Creme.

## Rezept Fotos

![Rezept Foto Italienischer Nudelsalat. Der Antipasti Salat ist auf einem geschwungenem Teller angerichtet und mit Ruccola garniert.](rezept-italienischer-nudelsalat-ruccola-balsamico-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Italienischer Nudelsalat. Der Antipasti Salat ist auf einem geschwungenem Teller angerichtet und mit Balsamico Essig beträufelt.](rezept-italienischer-nudelsalat-ruccola-balsamico-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Italienischer Nudelsalat. Nahaufnahme des Nudelsalats, es sind sehr schön die verschiedenen Zuataten wie Parmaschinken, Pinienkerne und die Balsamico Creme zu sehen.](rezept-italienischer-nudelsalat-ruccola-balsamico-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Italienischer Nudelsalat" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
