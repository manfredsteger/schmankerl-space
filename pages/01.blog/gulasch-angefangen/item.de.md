---
title: Gulasch
published: true
date: '17:34 04/04/2023'
metadata:
    og:title: 'Rezept: Gulasch'
    'og:image': https://schmankerl.space/de/blog/gulasch/000-titelbild.jpg
    'og:image:alt': 'Gulasch mit Kartoffelbrei'
    keywords: 'Rezept, Gulasch, deftig, Rindfleisch, Sauce, Braten, Kinderessen'
    description: 'Gulasch: Lange schmoren und am Ende super weich und fluffig. Das Rindergulasch Ammerthaler Art ist ein absoluter Reinschlemmer.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Braten
    tag:
        - Gulasch
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '12 Portionen'
    -
        option: 'Vorbereitung:'
        value: '60 Minuten'
    -
        option: 'Zubereitung:'
        value: '4-12 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Topf, Reindl oder Bräter mit Deckel, Stabmixer oder Blender'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Rindergulasch:'
        list:
            - '2kg Rindergulasch'
            - '150g Bauchspeck (Würfel oder ganz)'
            - '2kg Zwiebeln'
            - '1 Tube Tomatenmark 3-fach'
            - '60g Paprikapulver'
            - '15g Paprikapulver rosenscharf'
            - '1Tl Chipotli Chilipulver'
            - '1 Loorbeerblatt'
            - '5 Wachholderbeeren'
            - '4El Helle Sojasauce'
            - '50-70ml Branntweinessig'
            - '1El Worcester Sauce'
            - '1,2l Wasser (700 wenn mit)'
            - '500ml Rotwein (entweder)'
            - '100g Creme Fraiche (oder)'
            - 'Butterschmalz'
            - 'Salz und Pfeffer'
---

## Anleitung

1. Das Rindergulasch in ca. 3cm große Würfel schneiden (lassen). Den Bauchspeck in kleine feine Würfel teilen.
2. Die Zwiebeln schälen, vierteln und dann quer halbieren. Ihr braucht die halbierten Spalten nicht auseinanderpulen, das übernimmt gleich der Herd.
3. Bräter auf ca. halber Hitze / mittlerer Stufe mit gut 3El-4El Butterschmalz aufheizen und schon bei lauwarmer Hitze die Zwiebelspalten hinzufügen.
4. Die Zwiebeln etwa 25 Minuten anschmoren und dabei immer wieder leicht wenden und mit dem Schaber die Spalten auseinanderdrücken. Das spart viele Tränen ein. Bei Bedarf noch mehr Butterschmal zugeben.
5. Bereitet in der Zwischenzeit die Sauce vor. Bringt entweder 700ml (wenn ihr mit Wein kochen wollt) oder 1,2 Liter Wasser, wenn ihr die Creme Fraiche Variante für euch entschieden habt, zum Kochen.
6. Vermengt in einer Schüssel mit mindestens 3l Volumen das Tomatenmark, Paprikapulver und Rosenscharf, Chipotli Chili, das Loorbeerblatt, die Wachholderbeeren, die Sojasauce, den Branntweinessig und die Worcester Sauce. Gebt das heiße Wasser oder ggf. zusätzlich noch den Rotwein dazu. Gut verrühren und Beiseite stellen.
7. Nach ca. 25 Minuten sollten die Fleckerl gar und schon langsam gelb-braun werden. Jetzt wird es Zeit den Speck hinzuzugeben. Ihr könnt schon mal langsam den Ofen auf ca. 150 Grad Ober-Unterhitze vorheizen oder den Holzofen anschüren.
8. Lasst den Speck und die Zwiebeln etwa weitere 15-20 Minuten anbraten, bis am Boden der Pfanne oder des Bräters Bratansatz bildet. Dieser sollte schön kross und dunkelbraun sein, aber nicht schwarz werden. Sobald der Speck gut Farbe angenommen hat, ist die Grundmasse fertig.
9. Löscht nun den Bratansatz mit der Sauce ab. Passt dabei auf, beim Eingießen kann es schon mal ordentlich spritzen und blubbern.
10. Fügt nun das Rindergulasch im rohen Zustand in den Bräter oder die große Pfanne, verrüht die Fleischstückchen so, das sie alle mit Sauce bedeckt sind und packt einen Deckel auf den Bräter.
11. Wenn ihr das Gulasch schon am selben Tag essen wollt, also in frühestens 3 Stunden ab jetzt, könnt ihr den Herd noch kurz auf 1/3 Leistung anlassen und ca. 5 Minuten abwarten. Dann wird das Fleisch schon mal durchgewärmt und ihr spart euch eine halbe Stunde. Wollt ihr es wissen und ein wenig slow cooking betreiben, dann schaltet den Herd ab.
12. Schiebt den Bräter oder die Pfanne mit aufgesetztem Deckel in den Ofen. Bei 150 Grad ca. braucht ihr mindestens 2,5 Stunden Gar- bzw. Schmorzeit. Beim slow cooking könnt ihr jetzt auf 110 Grad zurückdrehen und ganz entspannt 3-4 Stunden chillen.
13. Je nach Machart nehmt ihr beim ersten Vorgang den Bräter aus dem Ofen und pult die Fleischstücke und Loorbettblatt mit einem Löffel aus der Sauce. Ich nehme immer die Saucenschüssel dafür her.
14. Wenn ihr das ganze Fleisch herausgepult habt, könnt ihr die Sauce mit einem Stabmixer oder Blender pürieren. Danach fügt ihr das Fleisch zurück und wieder ab in den Ofen (slow cooking) oder auf den Herd wenn es gleich Essen geben soll.
15. Schmeckt nach und nach die Sauce mit den Grundzutaten aus der Saucenbeschreibung ab. Die Wein Variante wäre jetzt fertig, bei der Creme Fraiche Variante müsst ihr nachdem Abschmecken die Hitze des Herds auf ganz niedrig rezuzieren. Nehmt einen kleinen Schöpfer Sauce und gebt diese in eine kleine Schüssel. Lasst sie 1 Minuten abkühlen, holt die Creme Fraiche und verrüht sie mit der kleinen Menge Sauce. Danach ab damit in den großen Bräter. Gut und vorsichtig verrühren, damit das Fleisch nicht brüchig wird. Das Gulasch ist fertig und darf nicht mehr aufkochen, sonst flockt die Creme Fraiche aus.
16. Die Slow Cooking Variante kann nach 4 Stunden auf entweder 6 Stunden 90 Grad zurückgeschaltet werden oder wenn es schon Abend/Nacht ist einfach abgeschalten werden. Bedenkt, wenn ihr eine offene Küche habt, es wird am nächsten Tag das ganze Haus/Bude wohl nach Gulasch duften. Bei geschlossenem Deckel, zieht das Fleisch herrlich durch und die Sauce bekommt einen wunderbaren Fleischgeschmack, ganz ohne Hilfsmittel wie Rinderbrühe oder Fond. Erwärmt gute 2 Stunden vor dem Essen das Gargut auf 130 Grad langsam im Herd oder auch etwas schneller bei ein Drittel der Herd Power. Es kommt nicht mehr auf die Dauer an, nur, dass die Sauce nicht köchelt. Wenn ihr essen wollt, muss die Sauce mindestens 70 Grad erreicht haben.
17. Mischt nun auch je nach Variante Creme Fraiche unter oder schmeckt das Gulasch final ab. Fertig.

## Tipp

> Gulasch satt? Gulasch Suppe Ahoi! Das beste am Gulasch ist die Gulasch Suppe wenn man es nicht mehr sehen kann. Ich esse zwei Mal Gulasch und dann mache ich mit 1l Wasser und Kartoffeln, Zwiebelspalten und Paprikastückchen eine wohlschmeckende Suppe daraus.

---

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Gulasch" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
