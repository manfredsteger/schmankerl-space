---
title: Rauchige Fajitas mit Limetten-Dip
published: true
date: '17:34 05/29/2019'
metadata:
    og:title: 'Rezept: Rauchige Fajitas mit Limetten-Dip'
    'og:image': https://schmankerl.space/de/blog/fajitas/000-teaser-fajitas-rezept-bohnen-mexikanisch.jpg
    'og:image:alt': 'Rezept Foto von den Fajitas. Das Foto zeigt die Zutaten und die bereitstehenden Wraps für die Füllung.'
    keywords: 'Fajita Mexikanisch, Mexico, Liquid Smoke, Grillen, Feurig, Scharf, Limetten, Hähnchen, Rezept'
    description: 'Fajitas mit Limetten-Dip: Grillklassiker aus Mexiko, der auf keinem Sommerfest fehlen sollte. Das Rezept unbedingt ausprobieren und teilen.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Mexikanisch
        - Fleisch
        - Grillen
        - Marinade
    tag:
        - Scharf
        - Feurig
        - Hähnchen
        - Tortillas
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: '12 Stunden'
    -
        option: 'Zubereitung:'
        value: '1 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Form, Plastikbeutel, Schüssel, Grill, Grillpfanne, Pfanne'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Fajitas:'
        list:
            - '12 Maismehltortillas (klein)'
            - '1kg Hähnchenschenkel oder Hähnbrustfilets'
            - '2 Paprikas'
            - '2 große rote Zwiebel'
            - '150g schwarze Bohnen'
            - '100g geriebener Cheddar oder alter Gauda'
    -
        title: 'Marinade:'
        list:
            - '3 El brauner Zucker'
            - '1 El Dijon Senf'
            - '1 Tl Chillipulver'
            - '2 Tl Kreuzkümmel'
            - '1 Tl Zwiebelpulver'
            - '1 Tl Knoblauchpulver'
            - '1 Tl geräuchertes Paprikapulver'
            - '1 Tl Chipotle Chillipulver'
            - '1 Tl Liquid Smoke'
            - '1 Tl Salz'
            - '1/2 Tl frisch gemahlener Pfeffer'
            - '3 El Orange ausgepresst'
            - '2 El Limette ausgepresst'
            - '2 El Helle Sojasauce'
            - '2 El Olivenöl'
    -
        title: 'Limetten-Dip:'
        list:
          - '1 Packung Sour Creme'
          - '1/2 Limette ausgepresst'
          - '1 Tl Salz'
          - '1 Tl Pfeffer'
          - '1/2 Tl Wilder Sumach'
          - '1/2 Chipotle Chillipulver'
          - '1/2 Knoblauchzehe fein gehackt'
          - '2 El gehackter Schnittlauch'
---

>Lust auf Grillen?
<cite>Ein Grillklassiker aus Mexiko, der auf keinem Sommerfest fehlen sollte.</cite>

## Anleitung

1. Die Hähnchenschenkel (Hähnchenbrustfilets) gründlich waschen und trockentupfen. Das Fleisch mit einem scharfen langen Messer vom Knochen lösen. Versucht dabei möglichst große zusammenhängende Stücke herauszuschneiden und legt diese beiseite.
2. Vermischt die Zutaten der Marinade in eine kleinen Schüssel und verrüht diese ein bis zwei Minuten. Es soll eine homogene Masse ohne Klümpchen entstehen.
3. Legt die Hähnchenteile in einen ausreichend großen Gefrierbeutel und gebt die Marinade dazu. Verschließt den Beutel gut und legt ihn über Nacht zum Marinieren in den Kühlschrank.
4. Bereitet 45 Minuten vor dem Essen die gewünschten Beilagen vor. Rührt den Dip in einer kleinen Schale an. Schneidet die Paprika und Zwiebeln in mundgerechte Stücke (nicht zu groß) und füllt die Bohnen und den geriebenen Käse in kleine Schälchen.
5. Heizt den Grill oder Herd ordentlich auf. Je nach Ausstattung legt das Hähnchen auf den Grillrost oder die (Grill)-Pfanne und grillt/bratet das Fleisch scharf an.
6. Nehmt das Fleisch heraus und schneidet es in feine Streifen und legt es zusammen mit den geschnittenen Paprika- und Zwiebelstückchen wieder in die (Grill)-Pfanne. Bratet beides mit ein wenig Olivenöl und zwei Esslöffel Fajita-Marinade aus dem Gefrierbeute 2-3 Minuten lang an.
7. Pfanne an den Tisch bringen und die Fajitas mit dem Dip bestreichen und mit den gewünschten Beilagen befüllen.

>Tipps
<cite>Befüllt die kleinen Tortillas nur mit sehr wenig Zutaten, sonst lassen sie sich sehr schlecht mit den Händen essen. Ja mit den Händen :)</cite>

<cite>Die Marinade lässt sich in 10-facher Menge sehr schön anrühren und einwecken. Füllt die Marinade in 200ml Gläschen ab und ihr könnt jederzeit 2-3 Hähnchenbrüste mit einem Gläschen schnell marinieren.</cite>

## Rezept Fotos

![Rezept Foto Mexikansiche Grill Fajitas. Auf dem Schneidebrett werden die Paprikas für die Fajita Füllung vorbereitet bzw. kleingeschnitten.](rezept-mexikanische-fajitas-grillen-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Nahaufnahme der geschnittenen Paprikas und rote Zwiebeln.](rezept-mexikanische-fajitas-grillen-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Einblick in die Rührschüssel mit den frischen Gewürzen für die Fajita Marinade.](rezept-mexikanische-fajitas-grillen-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Die Kitchen Aid Küchenmaschine rührt die Marinade fertig.](rezept-mexikanische-fajitas-grillen-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Der Dip für die Fajitas ist in einem Schälchen angerichtet, garniert mit frischem Schnittlauch aus dem Garten.](rezept-mexikanische-fajitas-grillen-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Ein Schneidebrett mit alles Zutaten die für die Füllung der Fajitas benötigt werden.](rezept-mexikanische-fajitas-grillen-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Dip und Zutaten nebeneinander angerichtet.](rezept-mexikanische-fajitas-grillen-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Kross gegrillte Hähnchenbrust auf dem Gas Grill.](rezept-mexikanische-fajitas-grillen-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Ein gedeckter Tisch im Garten, beim Grillen ist es das schönste, gleich im Freien zu sein und dort Essen zu dürfen.](rezept-mexikanische-fajitas-grillen-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Der Gasgrill von oben mit den fertig scharf angegegrillten Hähnchen und dem knackigen Gemüse, dass noch scharf angebraten wird.](rezept-mexikanische-fajitas-grillen-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Nahaufnahme des angegrillten Gemüse, das schon richtig Farbe bekommen hat.](rezept-mexikanische-fajitas-grillen-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Nahaufname der angegrillten Hähnchenbrust, das Gemüse ist auch gleich fertig.](rezept-mexikanische-fajitas-grillen-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Die kross angegrillte Hähnchenbrust liegt auf einem Schneidebrett bereit und wird gleich in dünne Streifen für die Fajitas geschnitten.](rezept-mexikanische-fajitas-grillen-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Die Hähnchenbrust in dünne Scheiben geschnitten.](rezept-mexikanische-fajitas-grillen-014.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Die dünnen Hähnchenbrust Streifen werden noch in kleinere Würfel geschnitten, damit sie gut in die Fajitas passen.](rezept-mexikanische-fajitas-grillen-015.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Mexikansiche Grill Fajitas. Die Fajitas werden gerade auf einem kleinen Teller gelegt und mit den Zutaten befüllt. Gleich wird gegessen.](rezept-mexikanische-fajitas-grillen-016.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Rauchige Fajitas mit Limetten-Dip" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
