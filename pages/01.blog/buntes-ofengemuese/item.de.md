---
title: Buntes Ofengemüse
published: true
date: '21:55 03/29/2018'
metadata:
    og:title: 'Rezept: Buntes Ofengemüse'
    'og:image': https://schmankerl.space/de/blog/buntes-ofengemuese/000-teaser-gemuesekiste-gemuesekorb-ofengemuese.jpg
    'og:image:alt': 'Rezept Foto vom bunten Ofengemüse. Das Foto zeigt eine handgetöpferte Schale aus der Lüneburger Heide mit frischem Gemüse, das gleich geschnibbelt und gebraten wird.'
    keywords: 'Gemüse, Ofengemüse, Brokkoli, Süßkartoffel, Kürbis, Kartoffel, Grillen, Rezept'
    description: 'Buntes Ofengemüse: Nach Lust und Laune austoben ist das Motto. Lieber Brokkoli, statt Süßkartoffel ... Schnappt euch euer Lieblingsgemüse und macht´s zu eurem eigenem Rezept.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Gemüse
    tag:
        - Vegetarisch
        - Beilage
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: '1 Stunde'
    -
        option: 'Zubereitung:'
        value: '1 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Backblech, Backpapier, große Schüssel'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Buntes Ofengemüse:'
        list:
            - '1 Hokkaido-Kürbis'
            - '2 Süßkartoffeln'
            - '200g Kartoffeln'
            - '2 Paprika'
            - '150g Zuckerschoten'
            - '3 Zwiebeln'
            - '2 Karotten'
            - '50ml Olivenöl'
            - 'Salz & Pfeffer'

    -
        title: 'Dip:'
        list:
            - '150g Frischkäse'
            - '200g Creme fraiche'
            - '150g Magerquark (alternativ Naturjoghurt)'
            - 'Limette (optional)'
            - 'Petersilie'
            - 'Schnittlauch'
            - 'Wilder Sumach Gewürzmischung'
            - 'Salz & Pfeffer'
---

>Variabilität und Kreativität gewünscht!
<cite>Hier kann man sich nach Lust und Laune austoben. Lieber Brokkoli, statt Süßkartoffel ... und bloß keine Karotten?! Schnappt euch euer Lieblingsgemüse und macht´s zu eurem eigenem Rezept.</cite>

## Anleitung

1. Kürbis, Kartoffeln, Karotten schälen, restliches Gemüse waschen. Alles in annähernd gleich große Würfel schneiden.
2. Backofen auf 200 Grad Ober-, Unterhitze vorheizen.
3. Für den Dip Frischkäse, Creme fraiche und Magerquark zusammenrühren. Salzen, pfeffern und gerne einen Spritzer Limetten-, oder Zitronensaft dazu. Die Kräuter waschen, klein hacken und mit zum Dip geben. Bis zum Servieren in den Kühlschrank stellen, kurz vorher nochmal abschmecken und mit wildem Sumach verfeinern.
4. Salzwasser in einem kleinen Topf auf dem Ofen erhitzen. Das Wasser sollte nicht kochen, nur sehr heiß sein. Die Zuckerschoten für 5 Minuten in den Topf geben und anschließend in ein Sieb abschütten.
5. Die gewürfelten Süßkartoffeln, Kartoffeln, Paprika und Karotten in eine große Schüssel geben. Hierzu kommt das Olivenöl, Salz und Pfeffer. Alles gut vermischen.
6. Backpapier auf das Backblech legen, das gewürzte Gemüse gleichmäßig darauf verteilen und auf die mittlere Schiene in den vorgeheizten Backofen schieben.
7. In der Schüssel sollten noch Reste vom Öl, Salz und Pfeffer sein, mit denen man jetzt die Zwiebeln und Zuckerschoten marinieren kann.
8. Nach 15 Minuten Backzeit kommen die Kürbiswürfel mit auf das Blech. Hier nutzt man die Gelegenheit am besten gleich, um das ganze Gemüse einmal mit dem Löffel durchzumischen.
9. Jetzt wirds etwas knifflig. Wer sein Gemüse knackig und bissfest mag, sollte ab Zugabe der Kürbiswürfel etwa 20 Minuten vergehen lassen und dann regelmäßig mit einer Gabel den Gargrad des Gemüses testen. Die Zwiebeln und Zuckerschoten brauchen im Ofen nur noch 5 Minuten. Somit muss jeder für sich selbst entscheiden, wie stark das Gemüse durchgegart werden soll und wann die letzten beiden Zutaten mit in den Ofen gegeben werden.
10. Zum Servieren den abgeschmeckten Dip mit anreichen. Wer möchte, kann noch frische Petersilie über das Ofengemüse streuen.

>"Multifunktionsgericht".
<cite>Ob zur Resteverwertung, als bunter Farbklecks auf dem Teller, oder als Beilage zum Grillen. Dieses Rezept ist wirklich vielseitig einsetzbar und wer die Schneidearbeit nicht scheut, wird große Freude daran haben.</cite>

## Rezept Fotos

![Rezept Foto Buntes Garten Ofengemüse. Das frische Gartengemüse liegt in einer schönen handgetöpferten Schale bereit geschält und geschnibbelt zu werden.](rezept-buntes-garten-ofen-gemuese-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Buntes Garten Ofengemüse. Seitenansicht des frischen Gartengemüse.](rezept-buntes-garten-ofen-gemuese-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Buntes Garten Ofengemüse. Draufsicht des frischen Gartengemüse.](rezept-buntes-garten-ofen-gemuese-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Buntes Ofengemüse" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
