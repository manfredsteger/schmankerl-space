---
title: Schweinebraten in der Kümmelbiersoße
published: ture
date: '12:34 03/21/2018'
metadata:
    og:title: 'Rezept: Schweinebraten in der Kümmelbiersoße'
    'og:image': https://schmankerl.space/de/blog/schweinebraten/000-teaser-schweinebraten-braten-krustenbraten-knoedel-rezept.jpg
    'og:image:alt': 'Ansicht des angerichteteten Rezepts: Schweinebraten mit Kümmelbiersöße auf einem gewölbten Teller. Im Hintergrund ist eine Flasche Bier zu sehen, das auch für die Sauce verwendet wurde.'
    keywords: 'Schweinebraten, Kümmelbiersoße, Soße, Festtagsbraten, Schmankerl, Braten, Knödel, Bayerisch, Rezept'
    description: 'Schweinebraten in der Kümmelbiersoße: Ein richtiges Stück Schweinerei! Seit drei Generationen weitergegeben und immer wieder verfeinert.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Braten
    tag:
        - Schwein
        - Fleisch
        - Sonntagsessen
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: anspruchsvoll
    -
        option: 'Reicht für:'
        value: '6 Personen'
    -
        option: 'Vorbereitung:'
        value: '1 Tag'
    -
        option: 'Zubereitung:'
        value: '7 Stunden'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Bräter, Pürierstab, Sieb, Backblech, Backpapier, Alufolie'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Schweinebraten:'
        list:
            - '500g Schweinerollbraten'
            - '2 Scheiben Schweinenacken (ca. 300g)'
            - '800g – 1kg Schälrippchen ohne Knorpel'
            - '1 Bund Suppengemüse (Karotten, Sellerie, Lauch und Petersilie)'
            - '1kg Zwiebel'
            - '500ml Dunkelbier oder bayrisch Helles'
            - 'Weizenmehl (wenn möglich doppelgriffig)'
            - 'Speisestärke'
            - 'Kümmel im Ganzen'
            - 'Kümmel gemahlen'
            - 'Paprikapulver edelsüß'
            - 'Chillipulver'
            - 'Sojasauce'
            - 'Salz & Pfeffer'
---

>Wer den Braten nicht ehrt, ist des Sonntags nicht wert!
<cite>Dieses Rezept wurde über drei Generationen weitergegeben und immer wieder verfeinert. Langsam kann ich ihn so gut wie meine Mutter und bald ein wenig besser ;)</cite>

![Schälrippchen auf einem Backblech ausgelegt, kross gebacken mit einer tollen Kruste.](schweinebraten-teaser.jpg)

## Anleitung

Das richtige Fleisch spielt bei diesem Gericht die tragende Rolle. Welche Stücke vom Schwein genommen werden ist offen – ohne Rippchen jedoch geht es nicht! Die Schälrippen bestehen vorwiegend aus Knochen, die für den Geschmack der Soße unerlässlich sind.

1. Am Vortag alle Fleischsorten mit den Gewürzen Salz & Pfeffer (viel), Paprikapulver (mittel), Chillipulver (sehr wenig) und dem Kümmel im Ganzen (viel) rundum einreiben.
2. Den Boden eines großen Bräters mit reinem Pflanzenöl einstreichen und die Fleischstücke hineinlegen. Mit weiterem Speiseöl das Fleisch benetzen und bei Bedarf das Fleisch Schichten. Anschließend für mindestens eine Nacht mit geschlossenem Deckel im Kühlschrank ruhen lassen.
3. 5 Stunden vor dem Essen muss die Vorbereitung beginnen.
4. Das Suppengemüße waschen und in mittelgroße Stücke schneiden. Die Möhren in Scheiben, die Zwiebeln je nach Größe vierteln oder sechsteln, den Lauch in Ringe und den Sellerie in Stückchen. Die Petersilie kommt erst ganz am Schluß zur Soße.
5. Das Fleisch aus dem Kühlschrank holen und auf ein Küchenbrett legen. Im Bräter sollte noch relativ viel Fett sein. Diesen auf dem Ofen erhitzen und bei mittlerer Temperatur das Gemüse in einem Schwung 4-5 Minuten anbraten. Währenddessen den Ofen auf 180 Grad Ober- Unterhitze vorheizen.
6. In der Zwischenzeit das Backblech mit zwei Schichten Backpapier und Alufolie ummanteln und die Rippchen mit der Fleischseite nach oben darauf ausbreiten. Sie dürfen auf keinen Fall übereinander liegen.
7. Das Backblech und den Bräter (ohne Deckel) auf oberer und unterer Schiene in den Ofen stellen. Ca. 30 – 45 Minuten lang das Gemüse dunkel bräunen lassen. Wenn das Gemüse sehr dunkel ist, das restliche Fleisch in den Bräter geben und zurück in den Ofen stellen.
8. Nach gut 1 Stunde sollte das Fleisch im Bräter Farbe angenommen haben. Den Bräter aus dem Ofen holen, das Fleisch und das Gemüse mit Mehl fein einstäuben. Weitere 30 Minuten lang anbräunen lassen.
9. Das Bier in den Bräter geben und mit dem Gemüse unter leichtem Rühren vermengen.
10. Nun werden die Schienen gewechselt. Der Bräter kommt nach unten und die Rippchen nach kurzem Mehl bestäuben nach oben. Alles Weitere 1 Stunde bei 170 Grad schmoren und anrösten lassen.
11. Ab hier wird etwas Fingerspitzengefühl und viel Erfahrung der Schlüssel zum perfekten Schweinebraten sein. Ihr müsst nun den Bräunungsgrad begutachten. Normalerweise sage ich immer, lieber etwas heller als zu dunkel – hier ist es aber genau anders herum. Der Schweinebraten braucht eine tiefdunkle Farbe, da sonst die Soße kein Aroma bekommt. Nehmt nun beide Behälter aus dem Ofen und legt das Fleisch auf ein Küchenbrett beiseite.
12. Gießt die Flüssigkeit und die Bratreste vom Küchenblech in den Bräter. Es hilft wenn ihr etwa 100ml Wasser auf das Küchenblech gießt und mit einem Löffel die Reste löst.
13. Anschließend das ganze Fleisch (Rippchen müssen in der Flüssigkeit liegen) in den Bräter geben und bei 160 Grad auf mittlerer Schiene für weitere 30 Minuten zurück in den Ofen.
14. Den Bräter aus dem Ofen holen und das Fleisch auf das Küchenbrett legen. Einen mittelgroßen Topf bereitstellen und das Sieb aufsetzen. Den restlichen Inhalt des Bräters durch das Sieb in den Topf fließen lassen.
15. Das Fleisch wieder in den Bräter geben und auf mittlerer Schiene zurück in den Ofen.
16. Das Sieb auf dem Topf belassen und mit dem Pürierstab das eingekochte Gemüse lange feinpürieren. Mit einem Löffel die Unterseite des Siebs abstreichen und in den Topf geben. Das Püree beiseite stellen und ggf. zum Abschmecken der Soße aufheben.
16. Den Topf mit der Rohsoße auf dem Herd bei mittlerer Hitze einmal aufkochen lassen. Währenddessen einen großen Esslöffel Speisestärke in ein Schälchen geben und mit 10-15ml Wasser verrühren. Nach dem Aufkochen die Hitze reduzieren, bis die Soße nicht mehr kocht.
17. Nun kommt der zweite schwierige Teil – das Abschmecken der Soße. Ähnlich wie beim Bräunungsgrad des Fleisches ist hier viel Erfahrung nötig. Die Soße sollte nun leicht salzig und malzig schmecken - aber nicht zu kräftig sein. Vorsichtig mit Salz und Sojasauce abschmecken. Nachträglich einen halben Teelöffel gemahlenen Kümmel und eine Brise frischen Kümmel hinzugeben.
18. Behaltet das Fleisch immer im Auge und wartet geduldig den richtigen Röstgrad ab. Bemerkt ihr, dass ein Stück Fleisch zu dunkel wird, tauscht es mit einem etwas hellerem Fleisch aus. Ist das gesamte Fleisch zu hell, übergrillt bei 200-220 Grad den Braten kurzzeitig – oder umgekehrt, dreht die Hitze im Ofen auf etwa 100 Grad zurück.
19. Ist das Fleisch fertig, kann zum finalen Soßen-Hieb angesetzt werden. Die Soße erneut aufkochen lassen, währenddessen 1/4 - 1/2 Bund Petersilie feinhacken. Die aufgelöste Speisestärke erneut kurz aufrühren und unter Rühren in die köchelnde Soße mengen.
20. Eine Minute unter leichtem Köcheln die Soße andicken lassen. Die Hitze reduzieren und mit Pfeffer und Salz final abschmecken.
21. Das Fleisch aus dem Ofen nehmen und Restflüssigkeit in die Sauce schütten.
22. Die gehackte Petersilie in die Sauce geben, kurz verrühren und zum Servieren bereitstellen.
23. Guten Appetit! Ihr habt es euch verdient.

>Fleischalternative
<cite>Wer schon ein Bratenexperte ist, kann sich an das beste Stück Fleisch für den Schweinebraten wagen – das Schäuferl. Das Stück Schweineschulter ist außerhalb Bayerns gar nicht so leicht zu bekommen – sogar bei den hießigen Metzgern nur auf Bestellung möglich. Das Schäuferl muss im Ganzen und mit Knochen geschmort werden. Wer in Franken lebt, hat keine Problem, dort ist es eine Spezialität.</cite>

>Beilage
<cite>Klassisch gibt es hierzu Knödel, Münchner Weißkraut und mindestens eine Flasche Augustiner pro Person. Prost, Mahlzeit!</cite>

## Rezept Fotos

![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Das frische Schweinefleisch liegt bereit um mit den Gewürzen bestreut zu werden. Schälrippchen, Schweinebauch, Spanferkl Rollbraten werden gleich mit Gewürzen einbalsamiert.](rezept-bayerischer-schweinebraten-biersauce-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Detailaufnahme von den Schweinerippchen ohne Knorpel.](rezept-bayerischer-schweinebraten-biersauce-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Ansicht unseres selbstgebauten Gewürzschränkchens mit frischen Gewürzen und Kräutern für den Schweinebraten wie z.B. Kümmel.](rezept-bayerischer-schweinebraten-biersauce-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Detailaufnahme von den Schweinerippchen ohne Knorpel mit viel Gewürzen wie Paprikapulver und Kümmel bestreut.](rezept-bayerischer-schweinebraten-biersauce-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Detailaufnahme von den Schweinerippchen ohne Knorpel die im Bräter bereitliegen und darauf warten im Ofen gebraten zu werden.](rezept-bayerischer-schweinebraten-biersauce-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Detailaufnamhe des Bräters mit dem geschnittenen Gemüse wie Zwiebel, Karotten, Lauch und Sellerie.](rezept-bayerischer-schweinebraten-biersauce-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Die Schweinerippchen wurden auf einem separaten Backblech knusprig braut gebraten.](rezept-bayerischer-schweinebraten-biersauce-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Die Schweinerippchen wurden auf einem separaten Backblech knusprig braut gebraten - schwarz weiß Aufnahme des vorherigen Bildes.](rezept-bayerischer-schweinebraten-biersauce-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Das fertige Gericht angerichtet auf einem geschwungenen Teller. Zwei Stücke Schweinebraten und einen Kartoffelknödel mit Kräutern bestreut.](rezept-bayerischer-schweinebraten-biersauce-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Das fertige Gericht angerichtet auf einem geschwungenen Teller. Zwei Stücke Schweinebraten und einen Kartoffelknödel mit Kräutern bestreut aber in anderem Blickwinkel.](rezept-bayerischer-schweinebraten-biersauce-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Der fertige Schweinebraten angerichtet auf einem geschwungenen Teller. Zwei Stücke Schweinebraten und einen Kartoffelknödel mit Kräutern bestreut. Lecker!](rezept-bayerischer-schweinebraten-biersauce-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Bayerischer Schweinebraten in der Kümmelbiersoße. Der fertige Schweinebraten angerichtet auf einem Teller. Zwei Stücke Braten und einen Knödel mit Kräutern bestreut.](rezept-bayerischer-schweinebraten-biersauce-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Schweinebraten in der Kümmelbiersoße" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
