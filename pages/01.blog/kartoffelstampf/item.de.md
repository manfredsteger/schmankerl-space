---
title: Oberpfälzer Kartoffelstampf
published: true
date: '13:30 12/12/2020'
metadata:
    og:title: 'Rezept: Oberpfälzer Kartoffelstampf'
    'og:image': https://schmankerl.space/de/blog/kartoffelstampf/000-titelbild.jpg
    'og:image:alt': 'Oberpfälzer Kartoffelbrei ist eine Delikatesse, wenn die richtigen Kartoffeln im Spiel sind. Nicht jede Kartoffel hat es verdient zum Stampf zu werden.'
    keywords: 'Rezept, Oberpfälzerisch, Kartoffelbrei, Stampf, Vegetarisch, Kinderessen'
    description: 'Rezept Kartoffel Stampf: Oberpfälzer Kartoffelbrei ist eine Delikatesse, wenn die richtigen Kartoffeln im Spiel sind. Nicht jede Kartoffel hat es verdient zum Stampf zu werden'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Vegetarisch
        - Beilage
    tag:
        - Kartoffeln
        - Hits4Kids
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: '45 Minuten'
    -
        option: 'Zubereitung:'
        value: '30 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Kochfeld, Kartoffelpresse, Kasserolle, Reibe'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Kartoffelstampf:'
        list:
            - '1,5kg mehlige Kartoffeln'
            - '125g Butter'
            - '1 mittelgroße Zwiebel'
            - '500ml Milch'
            - 'Muskatnuss'
            - 'Salz'
---

## Anleitung

1. Die mehligen Kartoffeln schälen, große Kartoffeln vierteln und entweder im Dämpftopf, oder in einem Topf mit Wasser für etwa 20 Minuten gar kochen.
2. In der Zwischenzeit die Zwiebel schälen, klein würfeln und in einem Topf mit etwa 1 El Butter bräunen.
3. Sind die Kartoffeln fertig, diese am besten noch heiß durch die Kartoffelpresse drücken, oder mit dem Stampfer zerdrücken. Mit kalten Kartoffeln geht das deutlich schwerer.
4. Nun 125g Butter bei geringer Hitze zum Kartoffelmus geben und gut rühren bis sich die Butter aufgelöst hat. Anschließend die Milch zugeben und den Herd etwas mehr erhitzen. Hier sollte man stets ein Auge auf den Topf haben und immer wieder gut umrühren, sonst brennt der Kartoffelstampf schnell an. Die Menge an Milch und Butter muss je nach Kartoffelsorte individuell leicht angepasst werden. Letzten Endes soll der Stampf sämig und fluffig zugleich sein.
5. Zum Schluss die gebräunten Zwiebeln unterrühren und mit geriebener Muskatnuss und Salz abschmecken.

>Nicht jede Kartoffel passt
<cite>Bei der Kartoffelsorte hat man oft die Qual der Wahl. Aus eigener Erfahrung und eigenem Anbau können wir frühe Kartoffeln, wie "Frieslander", "Sieglinde", oder "Belle de Fontenay" empfehlen.</cite>

## Rezept Fotos

![Rezept Foto vom Kartoffelbrei. Die rohen Kartoffeln liegen in einer Obstkiste bereit geschält uu werden. Es sind unterschiedliche Kartoffelsorten.](kartoffelstampf-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Die Kartoffeln für den Brei wurden geschält und in einem Kopftoch gerade weich gegart.](kartoffelstampf-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Butter und Zwiebeln bereitgestellt und die Zwiebel in kleine Würfel geschnitten.](kartoffelstampf-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Die angebräunten Zwiebeln für den Kartoffelstampf in einer Kasserolle.](kartoffelstampf-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Die weich gekochten Kartoffeln zerfallen schon leicht, daran erkennt man, dass sie perfekt für das Stampfen sind.](kartoffelstampf-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Katharina drück gerade mit einer Kartoffelpresse die Kartoffeln in eine Rührschüssel.](kartoffelstampf-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Detailaufnahme Katharina drück gerade mit einer Kartoffelpresse die Kartoffeln in eine Rührschüssel.](kartoffelstampf-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Mama Siglinde hat gerade mit einem Schneebesen die durchgedrückten Kartoffeln mit etwas Milch verrührt. Es entsteht ein sämiger Kartroffelbrei.](kartoffelstampf-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Mehr Milch und Butter zum Brei und kräftig rühren.](kartoffelstampf-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Foto vom Essenstisch mit Bratlinge, Kartoffelbrei und vegetarischer Bratensauce.](kartoffelstampf-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Oberpfälzer Kartoffelstampf" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
