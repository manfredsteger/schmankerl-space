---
title: Russischer Zupfkuchen
published: true
date: '17:34 04/13/2018'
metadata:
    og:title: 'Rezept: Russischer Zupfkuchen'
    'og:image': https://schmankerl.space/de/blog/russischer-zupfkuchen/000-teaser-russischer-zupfkuchen-blech-kuchen.jpg
    'og:image:alt': 'Rezept Foto vom Russischen Zupfkuchen. Das Foto zeigt den angeschnittenen außen braun krümeligen, innen die gold gelb, wie cremige Füllung.'
    keywords: 'Russischer Zupfkuchen, Kuchen, Kakao, Vanille, Backen, Backrezept, schneller Kuchen, Magerquark, Rezept'
    description: 'Russischer Zupfkuchen: Kakao trifft auf Vanille. Außen knackig und innen weich aber auch cremig. Ein verführerischer Kuchen der von Tag zu Tag leckerer wird!'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Backen
    tag:
        - Kuchen
        - Lieblingskuchen
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Zubereitung:'
        value: '1 Stunde 30 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Ofen, Kuchenform (26cm Durchmesser), Rührgerät, Rührschüssel, Knet- und Rührhaken, kleiner Topf'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Boden und Streusel:'
        list:
            - '350g Weizenmehl (Typ 1050)'
            - '150g Zucker'
            - '200g Butter'
            - '50g Kakao'
            - '1 Ei'
            - '1 Backpulver'
            - 'Semmelbrösel zum Einfetten (optional)'
    -
        title: 'Füllung:'
        list:
            - '500g Magerquark'
            - '200g Zucker'
            - '200g Butter'
            - '3 Eier'
            - '1 Vanillezucker'
            - '1 Vanillepuddingpulver'
---

>Wenn Kakao auf Vanille trifft:....
<cite>Außen knackig innen weich und cremig, ein verführerischer Kuchen, der von Tag zu Tag leckerer wird.</cite>

---

## Anleitung

1. Wer möchte, kann die Kuchenform vorab mit Butter einstreichen und Semmelbrösel darüber streuen, damit nach dem Backen nichts kleben bleibt. Die meisten neuen Formen sind mittlerweile ganz gut beschichtet, dass man sich diesen Schritt auch sparen kann.
2. Mehl und Zucker abwiegen und in die Rührschüssel geben. Die Butter in einem kleinen Topf auf dem Ofen zerlassen und mit in die Schüssel geben. Kakao, Ei und Backpulver dazu und alles gut mit dem Knethaken oder per Hand vermengen, bis ein einheitlicher Teig entsteht.
3. Den Backofen auf 185 Grad Ober-, Unterhitze vorheizen.
4. Den Boden der Kuchenform locker mit dem Teig auslegen und leicht am Rand hochziehen. Der bröckelige Teig muss gar nicht fest angedrückt werden, er wird beim Backen automatisch fest. Hierfür sollte c.a. die Hälfte, oder etwas mehr verwendet werden. Der restliche Teig kann erst mal in einer kleineren Schüssel bei Seite gestellt werden, daraus werden am Ende die Streusel geformt.
5. Für die Füllung die restliche Butter in einem kleinen Topf auf dem Ofen zerlassen. Sie sollte nicht anbräunen!
6. In der Zwischenzeit den Quark, Zucker und Vanillezucker verrühren. Die Eier einzeln dazugeben. Flüssige Butter unterrühren und am Schluss das Päckchen Vanillepuddingpulver dazu. Alles gut verrühren, bis eine homogene Masse entsteht.
7. Die Quarkfüllung nun in die ausgekleidete Kuchenform schütten. Den restlichen Teig zu Streuseln formen und auf die Kuchenmasse verteilen. Da der Teig von sich aus sehr bröckelig ist, reicht es meist, ihn locker durch die Hände zu reiben.
8. Für 1 Stunde bei 185 Grad in den Backofen und am Ende der Backzeit mit einem Holz-, oder Schaschlickstäbchen testen, ob der Kuchen fertig ist. Kleibt beim Herausziehen des Stäbchens kein Teig am Stäbchen, ist der Kuchen durchgebacken und somit fertig.

>Geduld zahlt sich aus.
<cite>Obwohl es jedes mal eine Herausforderung ist, den Kuchen nicht gleich am ersten Tag anzuschneiden, zahlt sich das Warten tatsächlich aus. Den vollen Geschmack entwickelt der Russische Zupfkuchen erst nach 2-3 Tagen. Sollte er solange überleben, kann er ohne Probleme im Kühlschrank oder an einem kalten Platz aufbewahrt werden.</cite>

## Rezept Fotos

![Rezept Foto Russischer Zupfkuchen. Die Butter in einer Stiel Kasserolle langsam am Herd schmelzen.](rezept-backen-kuchen-russischer-zupfkuchen-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Russischer Zupfkuchen. Die Kakao Masse und die Butter wurden zusammen mit dem Zucker in einer Schüssel vermengt. Es sind größere Klumpen entstanden, die als Streusel dienen.](rezept-backen-kuchen-russischer-zupfkuchen-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Russischer Zupfkuchen. Die Kakao Streusel werden zur Hälfte zerdrückt und als Boden für den Kuchen benutzt und die anderen als Streusel für den Kuchen.](rezept-backen-kuchen-russischer-zupfkuchen-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Russischer Zupfkuchen. Katharina rührt gerade die Creme Füllung mit einem Schaumlöffel durch. Vorher war die Creme aber in der Küchenmaschine.](rezept-backen-kuchen-russischer-zupfkuchen-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Russischer Zupfkuchen. Nahaufnahme Katharina rührt gerade die Creme Füllung mit einem Schaumlöffel durch.](rezept-backen-kuchen-russischer-zupfkuchen-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Russischer Zupfkuchen. Die Vanillepudding Füllung wird gerade aus der Schüssel auf den Boden gekippt.](rezept-backen-kuchen-russischer-zupfkuchen-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Russischer Zupfkuchen. Schwarz Weiß Aufnahme: Die Vanillepudding Füllung wird gerade aus der Schüssel auf den Boden gekippt.](rezept-backen-kuchen-russischer-zupfkuchen-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Russischer Zupfkuchen. Nahaufnahme: Die Vanillepudding Füllung wird gerade aus der Schüssel auf den Boden gekippt.](rezept-backen-kuchen-russischer-zupfkuchen-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Russischer Zupfkuchen. Die Kakao Streusel werden leicht zerkleinert und über die Füllung gestreut.](rezept-backen-kuchen-russischer-zupfkuchen-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Russischer Zupfkuchen. Nach und nach füllt sich der Streusel Deckel des Russischen Zupfkuchens.](rezept-backen-kuchen-russischer-zupfkuchen-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Russischer Zupfkuchen. Der Kuchen kommt frisch aus dem Backofen und sieht kross aus.](rezept-backen-kuchen-russischer-zupfkuchen-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Russischer Zupfkuchen. Außen kross und innen fluffig und cremig. Der angeschnittene Kuchen riecht herrlich und muss leider noch einen ganzen Tag ruhen, bis er wirklich gut schmeckt.](rezept-backen-kuchen-russischer-zupfkuchen-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Russischer Zupfkuchen" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
