---
title: Kürbiscreme Curry Suppe
published: true
date: '11:20 10/20/2020'
metadata:
    og:title: 'Rezept: Kürbiscreme Curry Suppe'
    'og:image': https://schmankerl.space/de/blog/kuerbiscremesuppe/000-titelbild-kuerbissuppe-rezept.jpg
    'og:image:alt': 'Kürbiscreme Curry Suppe angerichtet auf einem Suppenteller mit Kürbiskernöl und einer Scheibe Holzofen Kürbiskernbrot'
    keywords: 'Rezept, Curry, Kürbis, Kürbiscreme, Suppe, Sahne'
    description: 'Rezept: Kürbiscreme Curry Suppe mit frischem Hokkaido Kürbis und einer scharfen Curry Note. Perfekt für den Herbst.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Suppe
    tag:
        - Kürbis
        - Vegetarisch
description:
    -
        option: 'Schwierigkeitsgrad: mittel'
    -
        option: 'Reicht für:'
        value: '5 Personen'
    -
        option: 'Vorbereitung:'
        value: '1 Stunde'
    -
        option: 'Zubereitung:'
        value: '1 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Blender, Herd, Topf'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Kürbiscreme Suppe:'
        list:
            - '1kg Hokkaido Kürbis'
            - '1 mittelgroße Kartoffel'
            - '1 mittelgroße Zwiebel'
            - '1 kleine Zehe Knoblauch'
            - '1 mittelgroße Karotte'
            - '1 Scheibe Ingwer'
            - '60g Butter'
            - '200ml Sahne'
            - '1,2l Gemüsebrühe'
            - '1 kleiner Zimtsplitter'
            - '2-3 Tl Madras Curry Pulver'
            - '5 El Kürbiskernöl'
            - '1 Tl Speiseöl'
            - 'Pfeffer&Salz'
---

## Anleitung

1. Den Hokkaido Kürbis gut sauber machen und in Viertel schneiden. Anschließend den Strunk und die Kerne loswerden. Schneidet den ungeschälten Kürbis in etwa 2-3cm große Würfel.
2. Schält und schneidet die Kartoffeln, Zwiebel und Karotte 1-2cm große Stücke. Knoblauch wird gepellt und in Scheiben geschnitten.
3.  Nehmt einen ausreichend großen Topf, ich würde für diese Menge einen 3-4l Topf empfehlen, damit es später beim Pürieren (falls kein Blender vorhanden) keine Sauerei gibt. Erhitzt das Speiseöl auf mittlerer Stufe und bratet Kartoffeln, Zwiebel, Karotte, Zimtsplitter,Ingwerscheibe und Knoblauch an, bis alles ein wenig Farbe bekommt.
4. In der Zwischenzeit könnt ihr die Gemüsebrühe anrühren und beiseite stellen.
5. Ist das Gemüse angebräunt könnt ihr die Kürbisstücke dazugeben und 2-3 Minuten mit anbraten.
6. Gießt nun die Gemüsebrühe an und reduziert die Hitze, damit die Flüssigkeit unter dem Siedepunkt bleibt. Der Kürbis muss nun 20 Minuten garen.
7. Gebt nun die Sahne dazu und lasst alles einmal kurz Aufkochen.
8. Die Zutaten kommen nun in den Blender oder bleiben im Topf und werden mit dem Pürierstab zerkleinert.
9. Kippt die Suppe wieder in den Topf und kocht alles erneut kurz auf. Beim Erhitzen könnt ihr schon mal vorsichtig das Curry Pulver hinzugeben, umrühren und abschmecken.
10. Hat die Suppe einen angenehmen Curry Geschmack noch das Stück Butter hinzugeben, ein wenig rühren und mit Pfeffer&Salz finalisieren.
11. Die Suppe in einen Teller geben und mit einem 1El Kürbiskernöl garnieren.

>Tipp:
<cite>Wer gerne Suppen macht, wird einen Blender lieben! Egal ob Suppen oder Saucen, alles wird cremiger und spart viel Zeit, Sauerei und Nerven :-)</cite>

## Rezept Fotos

![Rezept Foto Kürbis Creme Suppe Hokkaido Kürbis auf dem Schneidebrett](kuerbissuppe001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Creme Suppe Oma Siglinde schneidet die Zwiebeln](kuerbissuppe002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Creme Suppe zerteilter Hokkaido Kürbis auf dem Schneidebrett](kuerbissuppe003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Creme Suppe alle Zutaten geschnitten und bereit um angebraten zu werden](kuerbissuppe004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Creme Suppe Kürbis gewürfelt auf dem Schneidebrett](kuerbissuppe005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Creme Suppe Kürbis alles kommt in den Kochtopf](kuerbissuppe006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Creme Suppe alle Zutaten schmoren im Topf mit der Gemüsebrühe](kuerbissuppe007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Creme Suppe alle Zutaten stehen im Blender bereit](kuerbissuppe008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Detailaufnahme Kürbis Creme Suppe alle Zutaten stehen im Blender bereit](kuerbissuppe009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Detailaufnahme Kürbis Creme Suppe nach dem Blender Vorgang](kuerbissuppe010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Detailaufnahme Kürbis Creme Suppe mit einem Stück Butter im Kochtopf](kuerbissuppe011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Detailaufnahme Kürbis Creme Suppe das frische Holzofenbrot wird geschnitten und zur Suppe gereicht](kuerbissuppe012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Kürbis Creme Suppe serviert auf einem Suppenteller mit Kürbiskernöl und frischem Kürbiskernbrot](kuerbissuppe013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Kürbiscreme Curry Suppe" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
