---
title: 'Himbeer Balsamico Dressing'
published: true
date: '06:10 07/07/2020'
metadata:
    author: 'Chefköche Katharina und Manfred Steger'
    keywords: 'Himbeere, Balsamico, Himbeeressig, Essig, Dressing, Salat, Öl, Kürbiskernöl, Rezept'
    description: 'Himbeer Balsamico Dressing mit Kürbiskernöl: Qualitativ hochwertigen Zutaten und viel Probiererei machen dieses Rezept zum Begleiter in der Küche. Nicht nur für den Salat genial, auch als Basis für ausgefallende Saucen der Hit.'
    'og:description': 'Himbeer Balsamico Dressing mit Kürbiskernöl: Qualitativ hochwertigen Zutaten und viel Probiererei machen dieses Rezept zum Begleiter in der Küche. Nicht nur für den Salat genial, auch als Basis für ausgefallende Saucen der Hit.'
    og:type: food
    og:locale: de_DE
    og:title: 'Rezept: Himbeer-Balsamico Dressing'
    'og:image': https://schmankerl.space/de/blog/balsamicodressing/000-teaser-himbeer-balsamico-dressing.jpg
    'og:image:alt': 'Kochtopf mit den köchelnden Zutaten der Balsamico Reduktion. Der Balsamico Himbeer Schaum setzt sich ähnlich wie beim Marmeladeeinwecken langsam ab.'
taxonomy:
    category:
        - Salat
        - Dressing
        - Vegetarisch
    tag:
        - Sauce
        - Gartensalat
        - Garten
        - schnell
        - Kräuter
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: keine
    -
        option: 'Zubereitung:'
        value: '25 Minuten'
    -
        option: 'Ausstattung:'
        value: Topf
ingredients_title: Zutaten
ingredients:
    -
        title: 'Himbeer Balsamico Dressing:'
        list:
            - '4 Tl Zucker / 80g'
            - '2 Tl Gelierzucker / 40g'
            - '1 Tl Salz / 20g'
            - '8 El Balsamicoessig  / 120 ml'
            - '1 El Apfelessig / 15ml'
            - '1 Tl Himbeeressig / 10ml'
            - '2 Tl Helle Sojasauce 20ml'
    -
        title: 'Gartensalat:'
        list:
            - Eichblattsalat
            - Zuckerhutsalat
            - 'Rote Zwiebel'
            - Spitzpaprika
            - Salatgurke
            - Sonnenblumenkerne
            - Walnusskerne
            - 'Steierisches Kürbiskernöl'
---

>Aufgepasst:
<cite>Dieses Rezept lebt von qualitativ hochwertigen Zutaten und viel Probiererei. Kauft nicht den billigsten aber auch nicht den teuersten Balsamicoessig. Gartensalat und Gartengemüse übertrumpfen jede Supermarkt- und auch Bioware!</cite>

## Anleitung

1. Beide Zuckerarten und das Salz in einem Topf leicht erwärmen, bis der Zucker anfängt zu schmelzen.
2. Mit dem Balsamicoessig und Apfelessig ablöschen und unter ständigem Rühren mit einem Löffel, den angesetzten Zucker lösen.
3. Anschließend den Himbeeressig und die helle Sojasauce hinzufügen.
4. 10-15 Minute köcheln lassen, Herd abschalten und beiseite stellen. Macht ihr eine vielfache Menge auf einmal, erhöht sich die Kochdauer. Bei 10 facher Menga ca. 1 Stunde.
5. Für den Direktverzehr ca. 15 Minuten abkühlen lassen. Wer das Ganze einwecken möchte, die Einweckgläser mit kochendem Wasser ausspülen, das Dressing kräftig verrühren und in die Gläser einfüllen. Die Gläser verschließen und 2 Minuten auf den Kopf stellen.
6. Salat und Gemüse waschen bzw. schälen.
7. Den Salat in mundgerechte Stücke zupfen und in ein Sieb zum trocknen legen.
8. Die Zwiebel und Spitzpaprika in dünne Ringe schneiden
9. Die Gurke fein hobeln.
10. Die Sonnenblumenkerne bei Bedarf kurz in einem Topf anrösten.
11. Den Salat 5 Minuten vor dem Essen mit ca. 5 El Dressing anmachen.
12. Das Steierische Kürbiskernöl darübergießen und den Salat gut vermengen.
13. Anschließend die restlichen Zutaten darübergeben und servieren.

>Tipp: Zu sauer?
<cite>Ist das Dressing zu sauer, einfach kurz vor dem Verwenden noch einmal aufkochen lassen und wieder abkühlen lassen. Wer eine Mikrowelle hat, kann das Dressing in einer Tasse kurz aufwärmen.</cite>

## Mengenangabe zum Einwecken

*Reicht für 2-3 Gläser á 400ml*

- 150g Zucker
- 70g Gelierzucker
- 25g Salz
- 500ml Balsamicoessig
- 40ml Apfelessig
- 40ml Himbeeressig
- 40ml Helle Sojasauce

Je größer die Menge, desto länger muss das Dressing köcheln. Einfache Menge ca. 15 Minuten und die 10-fache Menge etwa 70 Minuten. Wenn's ölig wird das Dressing und nicht mehr zu sauer ist, passt es.

>Viel mehr als ein Dressing!
<cite>Die Balsamicoreduktion kann auch als Marinade für Grill-Gemüsespieße verwendet werden oder als Basis für eine Sauce. Passt perfekt zur kurzgebratenen Entenbrust, einfach das Dressing 20 Minuten einköcheln lassen und mit Entenfond abschmecken.</cite>

## Rezept Fotos

![Rezept Foto Himbeer Balsamico Dressing. Schneidebrett mit den Zutaten aus der Rezeptbeschreibung für das Dressing.](himbeer-balsamico-dressing-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Himbeer Balsamico Dressing. Zuckerschoten in einem Topf mit Salzwasser. Sie werden kurz blanchiert.](himbeer-balsamico-dressing-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Himbeer Balsamico Dressing. Salatschüssel mit Zuckerschoten und Karottenscheiben. Gerade frisch mit dem Balsamico Dressing angemacht.](himbeer-balsamico-dressing-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Himbeer Balsamico Dressing. Foto in Schwarz Weiss. Salatschüssel mit Zuckerschoten und Karottenscheiben. Gerade frisch mit dem Balsamico Dressing angemacht.](himbeer-balsamico-dressing-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Himbeer Balsamico Dressing. Der Himbeer Balsamico Dressing und Kürbiskernöl angemacht Salat angerichtet auf einem großen Teller mit ein paar Brotscheiben an den Rädnern.](himbeer-balsamico-dressing-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Himbeer Balsamico Dressing. Foto in Schwarz Weiss. Der Himbeer Balsamico Dressing und Kürbiskernöl angemacht Salat angerichtet auf einem großen Teller mit ein paar Brotscheiben an den Rädnern.](himbeer-balsamico-dressing-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)


## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Himbeer Balsamico Dressing mit Kürbiskernöl am Gartensalat" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
