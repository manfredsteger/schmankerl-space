---
title: 'Eierlikör Tiramisu'
published: true
date: '08:34 08/26/2018'
metadata:
    og:title: 'Rezept: Eierlikör Tiramisu'
    'og:image': https://schmankerl.space/de/blog/tiramisu/000-teaser-tiramisu-nachspeise-gericht-rezept-mascarpone.jpg
    'og:image:alt': 'Eine Detailaufnahme der angerichteten Tiramisu auf einem kleinen runden Teller mit einer Kuchengabel. Man sieht die feine Schicht Kakao Pulver und die fluffige Eierlikör Creme.'
    keywords: 'Eierlikör, Tiramisu, Nachtisch, Süßspeise, Biskuitboden, Espresso, italienisch, Nachspeise, Rezept'
    description: 'Eierlikör Tiramisu: Der fluffig-cremige Muntermachen gelingt immer. Wer ein wenig Zeit hat, macht den Biskuitboden selbst und wird dafür geschmacklich belohnt.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Süßspeise
        - Nachtisch
        - Nachspeise
        - Italienisch
    tag:
        - Mascarpone
        - Italienisch
        - luftig
        - lecker
description:
    -
        option: 'Schwierigkeitsgrad: leicht'
    -
        option: 'Reicht für:'
        value: '6-8 Personen'
    -
        option: 'Zubereitung:'
        value: '1 Stunden 30 Minuten'
    -
        option: 'Ausstattung:'
        value: 'Auflaufform oder Ofenpfanne (Reindl), Backpapier, Rührschüssel'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Biskuitboden:'
        list:
            - '75g Weizenmehl'
            - '50g Speisestärke'
            - '4 Eier mittlerer Größe'
            - '125g Zucker'
            - '1 Päckchen Vanillezucker'
            - '1 Messerspitze Backpulver'
            - '3-4 El warmes Wasser'
            - '2-4 doppelte Espresso'
    -
        title: 'Tiramisu Füllung:'
        list:
            - '500g Mascarpone'
            - '5 Eier'
            - '125g Zucker'
            - '150ml Eierlikör'
            - 'Kakao Pulver'
---

>Der fluffig-cremige Muntermachen
<cite>Keine Zeit und eine Nachspeise benötigt? Hier gehts richtig schnell und gelingt immer. Wer ein wenig Zeit hat, macht den Biskuitboden selbst und wird dafür geschmacklich belohnt.</cite>

## Anleitung

1. Für den Biskuitboden 4 Eier trennen. Das Eiweiß mit einem Mixer anschlagen, bis es leicht aufschäumt. Dann 1 1/2 El Zucker hinzugeben und weiter schlagen, bis ein fester Eischnee ensteht. Der Eischnee sollte nicht zu fest und lange geschlagen werden, ansonsten wird er nicht so voluminös und fluffig.
2. Den Eischnee in eine andere Schüssel zwischenlagern. In die bereits benutzte Schüssel die Eigelbe mit 4 El lauwarmen Wasser geben.
3. Das Eigelb-Wasser-Gemisch im Mixer schaumig schlagen und den übrigen Zucker der anfänglichen 125g nach und nach beim Mixen unterrieseln lassen. Ist der komplette Zucker in der Masse, noch eine Minute weiter schlagen.
4. Das Mehl mit Speisestärke und Backpulver vermengen.
5. Den Einschnee auf die Eigelbmasse geben und darüber Mehl-Speisestärke-Gemisch sieben.
6. Mit einem Holzkochlöffel die Massen vorsichtig unterheben bis eine homogene Masse entsteht. Bitte auf keinen Fall rühren, da sonst die Luftigkeit des Biskuits verloren geht.
7. Sollte die vorhergesehene Tiramisu Form ofenfest sein, diese mit Backpapier auslegen und die Biskuitmasse einfüllen.
8. Den Backofen auf 180 Grad Ober- und Unterhitze vorheizen. Die Form auf mittlerer Schiene in den Ofen geben und je nach dicke des Bodens 10-25 Minuten goldbraun backen. Um zu testen, ob der Teig durchgebacken ist, ein Schachlikstäbchen in den Teig stecken. Haftet daran Teig ist der Boden noch nicht durchgebacken. Wenn ihr wenig Erfahrung mit Biskuit habt, macht die Stäbchenprobe lieber zu oft als zu wenig.
9. Den fertigen Biskuitboden aus dem Ofen nehmen und sofort mit dem Backpapier herausheben. Anschließend kopfüber in die Form stürzen und das Backpapier vorsichtig abziehen. Bei heißem bzw. noch warmen Biskuit funktioniert das Abziehen des Backpapiers besser als im kalten Zustand.
10. Den Biskuitboden in der Form auskühlen lassen und derweilen je nach Geschmack 2-4 frische Espresso darüber treufeln.
11. Für die Tiramisu-Masse die Eier trennen und das Eiweiß steif schlagen. Beiseite stellen und das Eigelb mit den 125g Zucker cremig schlagen. Nun wird unter die Eigelbmasse die Mascarpone Creme zusammen mit dem Eierlikör untergerührt.
12. Als nächstes wird der Eischnee vorsichtig mit einem Holzlöffel untergehoben, bis eine homogene Masse entsteht.
13. Abschließend die Masse auf den Biskuitboden verteilen.
14. Ab in den Kühlschrank und nach etwa 1-2 Stunden schmeckt die Tiramisu schon sehr gut. Am besten aber 12 Stunden kühl ruhen lassen, damit der Biskuitboden leicht durchtränkt wird.
15. Vor dem Servieren mit einem Sieb Kakao Pulver gleichmäßig darüberstreuen.

> Ein Traum, auch an heißen Tagen.
<cite>Passt aber bitte mit den rohen Eiern auf, die Tiramisu immer schön kühl halten oder noch viel einfacher – flott weglöffeln.</cite>

## Rezept Fotos

![Rezept Foto Eierlikör Tiramisu. Die Eier wurden getrennt und in eine Schüssel gegeben.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-001.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Der Eischnee wurde richtig steif geschlagen und beiseite gestellt.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-002.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Der Eischnee wird langsam und vorsichtig unter die Masse gehoben, nicht gerührt.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-003.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Das Mehr wird in einem Sieb langsam über die Masse gestreut, damit keine Klumpen entstehen.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-004.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Wieder muss langsam das Mehl unter die Masse gehoben werden.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-005.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Der Biskuitboden ist seit mehreren Minuten im Ofen und die Oberfläche langsam braun. Mit einem Schaschlickstäbchen wird die Masse getestet. Sticht man hinein und es bleibt kein Teig hängen, ist der Biskuitboden fertig.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-006.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Der fertige Biskuitboden ist goldbraun und homogen.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-007.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Foto vom Siebträger in dem bereits das Kaffeemehl verteilt wurde und darauf wartet in die Siebträgermaschine zu kommen.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-008.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Der Siebträger ist nun in die Siebträgermaschine eingebunden und wartet darauf als Espresso wieder herauszukommen.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-009.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Seitenansicht des eingespannten Siebträgers in die Esproesso Maschine.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-010.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Der bodenlose Siebträger bereitet den Espresso zu, dicke Crema läuft aus und die kleine Espresso Tasse füllt sich mit dem Kaffee.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-011.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Die Espresso Tasse ist halb voll mit cremig schaumigen Espresso.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-012.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Eine Detailaufnahme von der Siebträgermaschine und der Espressozubereitung. Zwei doppelte Espresso reichen für den großen Biskuitboden aus.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-013.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>
![Rezept Foto Eierlikör Tiramisu. Mama Siglinde beträufelt den Biskuitboden mit dem firschen Espresso.](eierlikoer-tiramisu-original-rezept-anleitung-bilder-014.jpg?lightbox=1920,1280&sizes=%28max-width%3A26em%29+100vw%2C+50vw&resize=900,600&quality=60)
<hr>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Eierlikör Tiramisu" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
