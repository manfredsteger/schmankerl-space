---
title: Gebutterter Kartoffelgratin
published: true
date: '17:34 03/06/2018'
metadata:
    og:title: 'Rezept: Gebutterter Kartoffelgratin'
    'og:image': https://schmankerl.space/de/blog/gebutterter-kartoffelgratin/000-teaser-kartoffel-gratin-selbst-gemacht-rezept.jpg
    'og:image:alt': 'Rezept Foto vom gebutterten Kartoffelgratin. Das Foto zeigt eine Bratröhre, indem der Kartoffelgratin langsam goldbraun wird. Eine schöne Käse Kruste bildet sich langsam.'
    keywords: 'Kartoffelgratin, Kartoffeln, Gratin, Beilage, Wiener Schnitzel, Muskat, Käse, Rezept'
    description: 'Gebutterter Kartoffelgratin: Gratin ist eine tolle Beilage für viele Gerichte und Rezepte. Wir essen den Gratin gerne zusammen mit einem frischen Gartensalat.'
    author: 'Chefköche Katharina und Manfred Steger'
taxonomy:
    category:
        - Beilage
        - Kartoffeln
        - Vegetarisch
    tag:
        - Gratin
        - einfach
description:
    -
        option: 'Schwierigkeitsgrad:'
        value: leicht
    -
        option: 'Reicht für:'
        value: '4 Personen'
    -
        option: 'Vorbereitung:'
        value: 'keine'
    -
        option: 'Zubereitung:'
        value: '1 Stunde'
    -
        option: 'Ausstattung:'
        value: 'Ofen, hitzebeständige Schale, kleiner Topf, Reibe, großes Küchenmesser'
ingredients_title: Zutaten
ingredients:
    -
        title: 'Kartoffelgratin:'
        list:
            - '1,2kg festkochende Kartoffeln'
            - '1 Knoblauchzehe'
            - 'Großes Stück Butter'
            - '200ml Sahne'
            - '200ml Milch'
            - '50g alter Gauda'
            - '50g Parmesan'
            - 'Muskat'
            - 'Salz & Pfeffer'
---

>Schnell und auch alleine lecker!
<cite>Das Gratin ist eine tolle Beilage für z.B. ein [Wiener Schnitzel](../wiener-schnitzel/item.de.md), kann aber auch als eigenständiges Gericht satt machen und natürlich auch gut schmecken.</cite>

## Anleitung

1. Die Kartoffeln schälen und in ein Wasserbad geben.
2. Die ungeschälte Knoblauchzehe mit der flachen Seite des Küchenmessers mit mittlerer Kraft zusammendrücken. Danach die Enden abschneiden, den Rest schälen und in kleine Würfelchen hacken.
3. Die Butter in einem kleinen Topf unter geringer Hitze zerlassen. Währenddessen die Kartoffeln aus dem Wasserbad holen und in dünne Scheiben schneiden.
4. Sobald die Butter zerlaufen ist und anfängt zu brutzeln, 1 Minute warten und den gehackten Knoblauch hinzugeben und anrösten bis er goldbraun ist.
5. Den Boden der feuerfesten Schale mit der Knochblauch-Butter bestreichen und zur Seite stellen.
6. Den Ofen auf 200 Grad Ober-Unterhitze vorheizen.
7. Die Milch und Sahne in den Topf geben, Hitze erhöhen und aufkochen lassen. Danach wieder auf geringe Temperatur herunterstellen und ca. 10 Minuten einreduzieren lassen. Währenddessen die Kartoffelscheiben in der Form mit leichter Neigung aufstellen.
8. Anschließend die Sauce mit frisch geriebenem Muskat, Salz und Pfeffer abschmecken und über den Gratin geben.
9. Den Käse über den Gratin reiben und verteilen. Ab in den Ofen, auf die mittlere Schiene und nach ca. 30 Minuten  servieren.

>Die richtige Käsemischung macht's!
<cite>Eine Käsesorte ist meist fade oder zu raß – eine ausgewogene Mischung aus mehreren Hart- und Weichkäse macht einen guten Gratinkäse aus. Wir nehmen meist Mozzarella, alter Gouda, alter Cheddar und Parmesan. </cite>

## Rezept Fotos


## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">![Creative Commons Lizenzvertrag CC BY. Sie können dieses Rezept bei Nennen und des Namens für alle Zwecke verwenden.](/user/assets/80x15.png)</a> "Gebutterter Kartoffelgratin" von <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
