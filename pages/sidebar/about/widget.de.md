---
title: 'Über uns'
published: true
metadata:
    keywords: 'Manfred Steger, Katharina Wertschek, Köche, Manfred, Katharina, Schmankerl, schmankerl.space'
    description: 'Katharina und Manfred kochen einfach gerne und warum nicht etwas Kultur und Lebensgefühl mit dem Universum teilen?  Manfred bastelt auch noch gerne an Website und an Bildungsfragen herum – mehr hierzu auf manfred-steger.de'
    robots: 'index, follow'
    author: 'Chefköche Katharina Wertschek und Manfred Steger'
taxonomy:
    category:
        - sidebar
css_suffix: widget_text
---

Katharina und Manfred kochen einfach gerne und warum nicht etwas Kultur und Lebensgefühl mit dem Universum teilen?

Manfred bastelt auch noch gerne an Website und an Bildungsfragen herum – mehr hierzu auf https://www.manfred-steger.de

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="/user/assets/88x31.png" /></a><br />Die Werke (Rezepte und Bilder) dieses Blogs sind lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>.

Zitiervorschlag: "Rezepttitel" <a rel="author" href="https://schmankerl.space">schmankerl.space</a>, <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>
